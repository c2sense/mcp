
package org.c2sense.mcp.rulengine;



import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Transformer;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.c2sense.mcp.utils.C2SDIConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.SAXParseException;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.publicalerts.cap.CapException;
import com.google.publicalerts.cap.CapXmlParser;
import com.google.publicalerts.cap.NotCapException;





@RunWith( SpringRunner.class )
@SpringBootTest
public class MessagesTest
{
	private String payload;

	@Before
	public void setUp( )
	{
		Resource resource = new ClassPathResource( "cap/edxl.de.xml" );
		InputStream resourceInputStream;
		try
		{
			resourceInputStream = resource.getInputStream( );
			payload = IOUtils.toString( resourceInputStream, Charsets.UTF_8 );

			System.out.println( payload );
		}
		catch ( IOException e )
		{
			Throwables.propagate( e );
		}
	}

	@Test
	public void messageNotNullTest( )
	{
		Assert.assertNotNull( "Message should be not null", payload );
	}

	@Test
	public void isBase64SringTest( )
	{
		final String regex = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( payload );

		Assert.assertTrue( matcher.find( ) );
	}

	@Test
	public void messageConversionToStringTest( )
	{
		byte [ ] decoded = Base64.getDecoder( ).decode( payload );

		payload = StringUtils.toEncodedString( decoded, Charsets.UTF_8 );
		Assert.assertNotNull( "Message should be not null", payload );

		System.out.println( payload );

		Transformer transformer = C2SDIConverter.instance.getTransformer( "edxl.de.xml_cap.xml" );

		StringWriter capConverted = C2SDIConverter.instance.convert( IOUtils.toInputStream( payload, Charsets.UTF_8 ), transformer );
		System.out.println( capConverted.toString( ) );

		CapXmlParser capXmlParser = new CapXmlParser( true );

		try
		{
			capXmlParser.parseFrom( capConverted.toString( ) );
		}
		catch ( NotCapException | SAXParseException | CapException e )
		{
			System.out.println( e.getMessage( ) );
		}
	}
}

/*

*/
