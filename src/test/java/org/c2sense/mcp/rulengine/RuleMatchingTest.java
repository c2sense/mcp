
package org.c2sense.mcp.rulengine;



import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.service.RuleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.SAXParseException;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.publicalerts.cap.Alert;
import com.google.publicalerts.cap.CapException;
import com.google.publicalerts.cap.CapXmlParser;
import com.google.publicalerts.cap.NotCapException;





@RunWith( SpringRunner.class )
@SpringBootTest
public class RuleMatchingTest
{

	@Autowired
	private RuleService			ruleService;
	private Rule				rule;
	private String				payload;
	private CapXmlParser		capXmlParser;
	private Alert				parsedAlert;
	private CommonCapMessage	message;

	@Before
	public void setUp( )
	{

		Resource resource = new ClassPathResource( "cap/cap_test.xml" );
		InputStream resourceInputStream;
		try
		{
			resourceInputStream = resource.getInputStream( );
			payload = IOUtils.toString( resourceInputStream, Charsets.UTF_8 );
			System.out.println( payload );
		}
		catch ( IOException e )
		{
			Throwables.propagate( e );
		}
		capXmlParser = new CapXmlParser( true );
		try
		{
			parsedAlert = capXmlParser.parseFrom( payload );
		}
		catch ( NotCapException | SAXParseException | CapException e )
		{
			Throwables.propagate( e );
		}
		message = new CommonCapMessage( parsedAlert );
	}

	@Test
	public void matchAreaFunctionTest( )
	{
		rule = ruleService.findByName( "AreaMatch" );
		Assert.assertTrue( rule.getRuleExecutable( ).apply( message ) );
	}

	@Test
	public void mismatchAreaFunctionTest( )
	{
		rule = ruleService.findByName( "AreaMismatch" );
		Assert.assertFalse( rule.getRuleExecutable( ).apply( message ) );
	}

	@Test
	public void matchregularExpressionTest( )
	{
		rule = ruleService.findByName( "Match regex" );
		Assert.assertTrue( rule.getRuleExecutable( ).apply( message ) );
	}

	@Test
	public void misMatchregularExpressionTest( )
	{
		rule = ruleService.findByName( "Regex mismatch" );
		Assert.assertFalse( rule.getRuleExecutable( ).apply( message ) );
	}
}

/*

*/
