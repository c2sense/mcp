
package org.c2sense.mcp.service;



import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.c2sense.mcp.form.UserJson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;





@RunWith( SpringRunner.class )
@SpringBootTest
public class TeamServiceTest
{
	@Autowired
	private TeamService	teamService;

	Long [ ]			userIds	= { 1L, 2L, 3L };

	@Before
	public void setUp( )
	{

	}

	@Test
	public void addUsersToTeamTest( )
	{
		try
		{

			teamService.addUsersToTeam( userIds, 2L );
		}
		catch ( DataIntegrityViolationException ex )
		{
			System.out.println( "Already inserted" );
		}

		List < UserJson > findAllUsersByTeam = teamService.findAllUsersByTeam( 2L );
		ArrayList < Long > userIdsList = Lists.newArrayList( userIds );
		while ( findAllUsersByTeam.iterator( ).hasNext( ) )
		{
			assertThat( userIdsList.contains( findAllUsersByTeam.iterator( ).next( ).id ) );
		}
	}

}

/*

 */
