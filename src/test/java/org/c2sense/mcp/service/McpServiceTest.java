
package org.c2sense.mcp.service;



import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.c2sense.mcp.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;





@RunWith( SpringRunner.class )
@SpringBootTest
public class McpServiceTest
{
	@Autowired
	private McpService teamService;

	@Before
	public void setUp( )
	{
	}

	@Test
	public void testTeamList( ) throws Exception
	{
		List < Team > list1 = teamService.fetchMcpGraph( );

		McpService.printMcpGraph( list1 );

		assertThat( list1 ).isNotNull( ).isNotEmpty( );
	}
}

/*



















*/
