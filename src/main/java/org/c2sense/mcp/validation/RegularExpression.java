
package org.c2sense.mcp.validation;



import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.c2sense.mcp.validation.impl.RegularExpressionValidator;





@Target( { FIELD, METHOD, PARAMETER, ANNOTATION_TYPE } )
@Retention( RUNTIME )
@Constraint( validatedBy = RegularExpressionValidator.class )
@Documented
public @interface RegularExpression
{
	String message( ) default "{validation.invalid-regex}";

	Class < ? > [ ] groups( ) default { };

	Class < ? extends Payload > [ ] payload( ) default { };
}
