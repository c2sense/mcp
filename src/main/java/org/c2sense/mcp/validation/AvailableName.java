
package org.c2sense.mcp.validation;



import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.c2sense.mcp.validation.impl.AvailableNameValidator;





@Target( { FIELD, METHOD, PARAMETER, ANNOTATION_TYPE } )
@Retention( RUNTIME )
@Constraint( validatedBy = AvailableNameValidator.class )
@Documented
public @interface AvailableName
{
	String message( )

	default "{validation.name-not-available}";

	Class < ? > [ ] groups( ) default { };

	Class < ? > onClass( );

	Class < ? extends Payload > [ ] payload( ) default { };
}
