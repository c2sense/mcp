
package org.c2sense.mcp.validation.impl;



import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.c2sense.mcp.validation.NoForbiddenWord;





public class NoForbiddenWordValidator implements ConstraintValidator < NoForbiddenWord, String >
{
	private String taboo;

	@Override
	public void initialize( NoForbiddenWord constraintAnnotation )
	{
		taboo = constraintAnnotation.taboo( );
	}

	@Override
	public boolean isValid( String value, ConstraintValidatorContext context )
	{
		if ( value == null )
		{
			return true;
		}

		return ! value.contains( taboo );
	}
}
