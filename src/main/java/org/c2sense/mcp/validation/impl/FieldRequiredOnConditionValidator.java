
package org.c2sense.mcp.validation.impl;



import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.c2sense.mcp.model.RequiredFieldCondition;
import org.c2sense.mcp.validation.FieldRequiredOnCondition;

import com.google.common.base.Strings;





public class FieldRequiredOnConditionValidator implements ConstraintValidator < FieldRequiredOnCondition, RequiredFieldCondition < String > >
{

	@Override
	public void initialize( FieldRequiredOnCondition constraintAnnotation )
	{

	}

	@Override
	public boolean isValid( RequiredFieldCondition < String > value, ConstraintValidatorContext context )
	{
		if ( value == null )
		{
			return true;
		}

		if ( ! value.isFieldRequired( ) )
		{
			return true;
		}
		else
		{
			String messageTemplate = context.getDefaultConstraintMessageTemplate( );

			context.disableDefaultConstraintViolation( );
			context.buildConstraintViolationWithTemplate( messageTemplate )
				.addPropertyNode( "mailingList" ).addConstraintViolation( );

			return ! Strings.isNullOrEmpty( value.fieldValue( ) );
		}
	}
}
