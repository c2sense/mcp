
package org.c2sense.mcp.validation.impl;



import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.c2sense.mcp.validation.RegularExpression;





public class RegularExpressionValidator implements ConstraintValidator < RegularExpression, String >
{

	@Override
	public void initialize( RegularExpression constraintAnnotation )
	{

	}

	@Override
	public boolean isValid( String value, ConstraintValidatorContext context )
	{
		if ( value == null )
		{
			return true;
		}

		try
		{
			Pattern.compile( value );
			return true;
		}
		catch ( PatternSyntaxException ex )
		{
			return false;
		}
	}
}
