
package org.c2sense.mcp.validation.impl;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.c2sense.mcp.model.NamedEntity;
import org.c2sense.mcp.validation.AvailableOrOwnName;





public class AvailableOrOwnNameValidator implements ConstraintValidator < AvailableOrOwnName, NamedEntity < Long > >
{
	@PersistenceContext
	private EntityManager	em;

	private Class < ? >		entityClass;

	@Override
	public void initialize( AvailableOrOwnName constraintAnnotation )
	{
		entityClass = constraintAnnotation.entityClass( );
	}

	@Override
	@SuppressWarnings( { "unchecked" } )
	public boolean isValid( NamedEntity < Long > value, ConstraintValidatorContext context )
	{
		if ( value == null )
		{
			return true;
		}

		List < NamedEntity < Long > > list = ( List < NamedEntity < Long > > ) em.createQuery( "from " + entityClass.getSimpleName( ) + " e where e.name = :name", entityClass )
			.setParameter( "name", value.getName( ) )
			.getResultList( );

		NamedEntity < Long > entity = ( NamedEntity < Long > ) em.find( entityClass, value.getId( ) );

		if ( list.isEmpty( ) || ( entity != null && entity.getName( ).equals( value.getName( ) ) ) )
		{
			return true;
		}
		else
		{
			String messageTemplate = context.getDefaultConstraintMessageTemplate( );

			context.disableDefaultConstraintViolation( );
			context.buildConstraintViolationWithTemplate( messageTemplate )
				.addPropertyNode( "name" ).addConstraintViolation( );

			return false;
		}
	}
}
