
package org.c2sense.mcp.validation.impl;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.c2sense.mcp.validation.AvailableName;





public class AvailableNameValidator implements ConstraintValidator < AvailableName, String >
{
	@PersistenceContext
	private EntityManager	em;
	private Class < ? >		clazz;

	@Override
	public void initialize( AvailableName constraintAnnotation )
	{
		clazz = constraintAnnotation.onClass( );
	}

	@Override
	public boolean isValid( String value, ConstraintValidatorContext context )
	{
		if ( value == null )
		{
			return true;
		}

		return em.createQuery( "from " + clazz.getSimpleName( ) + " c where c.name = :name", clazz ).setParameter( "name", value ).getResultList( ).size( ) == 0;
	}
}
