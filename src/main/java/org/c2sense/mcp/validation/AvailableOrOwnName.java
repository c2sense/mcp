
package org.c2sense.mcp.validation;



import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.c2sense.mcp.validation.impl.AvailableOrOwnNameValidator;





@Target( { TYPE, PARAMETER, ANNOTATION_TYPE } )
@Retention( RUNTIME )
@Constraint( validatedBy = AvailableOrOwnNameValidator.class )
@Documented
public @interface AvailableOrOwnName
{
	String message( ) default "{validation.name-not-available}";

	Class < ? > entityClass( );

	Class < ? > [ ] groups( ) default { };

	Class < ? extends Payload > [ ] payload( ) default { };
}
