
package org.c2sense.mcp.filter;



import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;





@Component
public class PrincipalDetailsFilter extends GenericFilterBean
{
	public static final String	REQUEST_ATTRIBUTE_PRINCIPAL_DETAILS	= "org.c2sense.mcp.PRINCIPAL_DETAILS";
	public static final String	REQUEST_ATTRIBUTE_USER_ID			= "org.c2sense.mcp.USER_ID";
	public static final String	REQUEST_ATTRIBUTE_USER_TEAMS		= "org.c2sense.mcp.USER_TEAMS";

	@Autowired
	private JdbcTemplate		jdbcTemplate;

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException
	{
		System.out.println( "[REQUEST] " + ( ( HttpServletRequest ) request ).getRequestURL( ) );

		Authentication authentication = SecurityContextHolder.getContext( ).getAuthentication( );

		// Non trovo la costante che definisce il valore anonymousUser
		if ( authentication != null && ! authentication.getName( ).equals( "anonymousUser" ) )
		{
//		 @-
/*
			String sql = "select u.id as id, group_concat( a.team_id ) as teams, u.name as name from user u left join team_admin a on (a.user_id = u.id) where u.email = ?";

			List <  PrincipalDetails > principalDetails = jdbcTemplate.query( sql, new RowMapper < PrincipalDetails >( )
			{
				@Override
				public PrincipalDetails mapRow( ResultSet rs, int rowNum ) throws SQLException
				{
					return new PrincipalDetails( rs.getLong( "id" ), rs.getString( "teams" ), rs.getString( "name" ) );
				}
			}, authentication.getName( ) );

			request.setAttribute( REQUEST_ATTRIBUTE_USER_ID, principalDetails.get( 0 ).userId );
			request.setAttribute( REQUEST_ATTRIBUTE_USER_TEAMS, principalDetails.get( 0 ).teamIds );
			request.setAttribute( REQUEST_ATTRIBUTE_PRINCIPAL_DETAILS, new PrincipalDetails( principalDetails.get( 0 ).userId, principalDetails.get( 0 ).teamIds, authentication.getName( ) ) );
*/
//			 @+

		}
		request.setAttribute( REQUEST_ATTRIBUTE_PRINCIPAL_DETAILS, new PrincipalDetails( 1L, "1,2",
			authentication.getName( ) ) );

		System.out.println( request.getAttribute( REQUEST_ATTRIBUTE_USER_ID ) );
		System.out.println( request.getAttribute( REQUEST_ATTRIBUTE_USER_TEAMS ) );

		chain.doFilter( request, response );
	}

	public static PrincipalDetails fromRequest( HttpServletRequest request )
	{
		return ( PrincipalDetails ) request.getAttribute( REQUEST_ATTRIBUTE_PRINCIPAL_DETAILS );
	}
}
