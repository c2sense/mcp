
package org.c2sense.mcp.filter;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.Assert;





public class PrincipalDetails
{
	public static final String		ROLE_ADMINISTRATOR		= "ADMINISTRATOR";
	public static final String		ROLE_TEAM_ADMINISTRATOR	= "TEAM_ADMINISTRATOR";

	public final List < String >	roles					= Arrays.asList( "ADMINISTRATOR" );
	public final String				name;
	public final Long				userId;
	public final List < Long >		teamIds;

	public PrincipalDetails( Long userId, String teamIds, String name )
	{
		this.userId = userId;
		this.teamIds = new ArrayList <>( );
		this.name = name;

		if ( teamIds != null )
		{
			String [ ] splittedIds = teamIds.split( "," );

			for ( int i = 0; i < splittedIds.length; i ++ )
			{
				this.teamIds.add( Long.parseLong( splittedIds[ i ] ) );
			}
		}
	}

	public PrincipalDetails( Long userId, List < Long > teamIds, String name )
	{
		Assert.notNull( teamIds );

		this.userId = userId;
		this.teamIds = teamIds;
		this.name = name;
	}

	public boolean hasRole( String role )
	{
		return roles.contains( role );
	}

	public boolean hasTeam( Long teamId )
	{
		return teamIds.contains( teamId );
	}

	public boolean hasGroup( Long groupId )
	{
		// TODO Auto-generated method stub
		return false;
	}
}
