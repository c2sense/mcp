
package org.c2sense.mcp.model;



import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;





/**
 * Created by Andrea.Sbarra on 24/08/2016.
 */
public class ConstraintViolationErrorInfo extends ErrorInfo
{
	public final ImmutableMap < String, String > errors;

	public ConstraintViolationErrorInfo( String url, ConstraintViolationException ex )
	{
		super( url, ex );
		Builder < String, String > errorsMapBuilder = ImmutableMap.builder( );
		for ( ConstraintViolation < ? > violation : ex.getConstraintViolations( ) )
		{
			errorsMapBuilder.put( violation.getPropertyPath( ).toString( ), violation.getMessage( ) );
		}
		errors = errorsMapBuilder.build( );
	}
}
