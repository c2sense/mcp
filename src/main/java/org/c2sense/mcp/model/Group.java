
package org.c2sense.mcp.model;



import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.c2sense.mcp.form.GroupForm;





@Entity
@Table( name = "group_" )
public class Group implements NamedEntity < Long >
{
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long		id;

	public String	name;

	public String	description;

	@Column( name = "team_id" )
	public Long		teamId;

	@Column( name = "sms_notification" )
	public Boolean	smsNotification;

	@Column( name = "user_email_notification" )
	public Boolean	userEmailNotification;

	@Column( name = "mailing_list" )
	public String	mailingList;

	@Column( name = "facebook_email" )
	public String	facebookEmail;

	@Column( name = "twitter_email" )
	public String	twitterEmail;

	public Group( )
	{
	}

	public Group( GroupForm form )
	{
		this.name = form.name;
		this.description = form.description;
		this.teamId = form.teamId_;
		this.smsNotification = form.isSmsNotificationActive;
		this.userEmailNotification = form.isUserEmailNotificationActive;
		this.mailingList = form.mailingList;
		this.facebookEmail = form.facebookEmail;
		this.twitterEmail = form.twitterEmail;
	}

	@Override
	public Long getId( )
	{
		return id;
	}

	@Override
	public String getName( )
	{
		return name;
	}

	public Boolean isSmsNotificationActive( )
	{
		return smsNotification;
	}

	public Boolean isUserEmailNotificationActive( )
	{
		return userEmailNotification;
	}

	public Boolean isMailingListNotificationActive( )
	{
		return mailingList != null;
	}

	public Boolean isFacebookEmailNotificationActive( )
	{
		return facebookEmail != null;
	}

	public Boolean isTwitterEmailNotificationActive( )
	{
		return twitterEmail != null;
	}

	@OneToMany( fetch = FetchType.LAZY )
	@JoinTable( name = "group_user", joinColumns = @JoinColumn( name = "group_id" ), inverseJoinColumns = @JoinColumn( name = "user_id" ) )
	public Set < User >		users	= new HashSet <>( );

	@OneToMany( fetch = FetchType.LAZY )
	@JoinTable( name = "group_topic", joinColumns = @JoinColumn( name = "group_id" ), inverseJoinColumns = @JoinColumn( name = "topic_id" ) )
	public Set < Topic >	topics	= new HashSet <>( );

	@OneToMany( mappedBy = "groupId", fetch = FetchType.LAZY )
	public Set < Rule >		rules	= new HashSet <>( );

}
