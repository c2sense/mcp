
package org.c2sense.mcp.model;



/**
 * Created by Andrea.Sbarra on 24/08/2016.
 */
public abstract class ErrorInfo
{
	public final String	url;
	public final String	ex;

	public ErrorInfo( String url, Exception ex )
	{
		this.url = url;
		this.ex = ex.getMessage( );
	}
}
