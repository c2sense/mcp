
package org.c2sense.mcp.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.c2sense.mcp.form.TopicForm;





@Entity
@Table( name = "topic" )
public class Topic implements NamedEntity < Long >
{
	public enum ContentType
	{
		CAP_XML, CMF_JSON
	}

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long			id;

	public String		name;

	public String		description;

	@Enumerated( EnumType.STRING )
	@Column( name = "content_type" )
	public ContentType	contentType;

	public Topic( )
	{
	}

	public Topic( TopicForm form )
	{
		this.name = form.name;
		this.description = form.description;
		this.contentType = ContentType.valueOf( form.contentType );
	}

	@Override
	public Long getId( )
	{
		return id;
	}

	@Override
	public String getName( )
	{
		return name;
	}
}
