
package org.c2sense.mcp.model;



import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;





@Entity
@Table( name = "user" )
public class User
{
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long		id;

	@JsonIgnore
	@Column( name = "spt_id" )
	public String	sptId;

	public String	name;

	public String	email;

	@Column( name = "phone_number" )
	public String	phoneNumber;

	public static class RowMapper implements org.springframework.jdbc.core.RowMapper < User >
	{
		@Override
		public User mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			User user = new User( );

			user.id = rs.getLong( "id" );
			user.name = rs.getString( "name" );
			user.email = rs.getString( "email" );
			user.phoneNumber = rs.getString( "phone_number" );

			return user;
		}
	}
}

/*

















*/
