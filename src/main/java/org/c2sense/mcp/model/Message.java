
package org.c2sense.mcp.model;



import java.io.IOException;
import java.io.StringReader;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;





@Entity
@Table( name = "message" )
public class Message
{
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long		id;

	@Lob
	public String	alert;

	public Message( )
	{
	}

	public Message( String alert )
	{
		this.alert = alert;
	}

	public String getIndentedAlert( ) throws TransformerException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, ClassCastException
	{
		String unescapeXml = StringEscapeUtils.unescapeXml( alert );
		final InputSource src = new InputSource( new StringReader( unescapeXml ) );
		final Node document = DocumentBuilderFactory.newInstance( ).newDocumentBuilder( ).parse( src ).getDocumentElement( );

		final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance( );
		final DOMImplementationLS impl = ( DOMImplementationLS ) registry.getDOMImplementation( "LS" );
		final LSSerializer writer = impl.createLSSerializer( );

		writer.getDomConfig( ).setParameter( "format-pretty-print", Boolean.TRUE );
		writer.getDomConfig( ).setParameter( "xml-declaration", true );

		return new String( writer.writeToString( document ) );
	}
}
