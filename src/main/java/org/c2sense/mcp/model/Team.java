
package org.c2sense.mcp.model;



import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.c2sense.mcp.form.TeamForm;





@Entity
@Table( name = "team" )
public class Team implements NamedEntity < Long >
{
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long		id;

	public String	name;

	public String	description;

	public Team( )
	{
	}

	public Team( TeamForm form )
	{
		name = form.name;
		description = form.description;
	}

	@Override
	public Long getId( )
	{
		return id;
	}

	@Override
	public String getName( )
	{
		return name;
	}

	@OneToMany( fetch = FetchType.LAZY )
	@JoinTable( name = "team_user", joinColumns = @JoinColumn( name = "team_id" ), inverseJoinColumns = @JoinColumn( name = "user_id" ) )
	public Set < User >		users	= new HashSet <>( );

	@OneToMany( fetch = FetchType.LAZY )
	@JoinTable( name = "team_topic", joinColumns = @JoinColumn( name = "team_id" ), inverseJoinColumns = @JoinColumn( name = "topic_id" ) )
	public Set < Topic >	topics	= new HashSet <>( );

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "teamId" )
	public Set < Group >	groups	= new HashSet <>( );

	// ------------------------------------------------------------------------

	public String getDescription( )
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public Set < User > getUsers( )
	{
		return users;
	}

	public void setUsers( Set < User > users )
	{
		this.users = users;
	}

	public Set < Topic > getTopics( )
	{
		return topics;
	}

	public void setTopics( Set < Topic > topics )
	{
		this.topics = topics;
	}

	public Set < Group > getGroups( )
	{
		return groups;
	}

	public void setGroups( Set < Group > groups )
	{
		this.groups = groups;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public void setName( String name )
	{
		this.name = name;
	}
}
