
package org.c2sense.mcp.model;



public interface NamedEntity < ID >
{
	ID getId( );

	String getName( );
}
