
package org.c2sense.mcp.model;



import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.c2sense.mcp.form.RuleForm;
import org.c2sense.mcp.instances.JSON;
import org.c2sense.mcp.rulengine.spi.RuleEngine;
import org.c2sense.mcp.rulengine.spi.ExecutableRule;





@Entity
@Table( name = "rule" )
public class Rule implements NamedEntity < Long >
{
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long				id;

	@Column( name = "group_id" )
	public Long				groupId;

	public String			name;

	public String			description;

	@Lob
	@Column( name = "rule_definition" )
	private String			ruleDefinition;

	@Transient
	private final Object	ruleLock	= new Object( );

	@Transient
	private ExecutableRule		ruleExecutable;

	@OneToMany( fetch = FetchType.LAZY )
	@JoinTable( name = "rule_topic", joinColumns = @JoinColumn( name = "rule_id" ), inverseJoinColumns = @JoinColumn( name = "topic_id" ) )
	@Column( insertable = false, updatable = false )
	public Set < Topic >	topics		= new HashSet <>( );

	public Rule( )
	{
	}

	public Rule( Long id, String name, String description )
	{
		// @-
		this.id          = id;
		this.name        = name;
		this.description = description;
		// @+
	}

	public Rule( Rule rule )
	{
		// @-
		this.id             = rule.id;
		this.groupId        = rule.groupId;
		this.name           = rule.name;
		this.description    = rule.description;
		this.ruleDefinition = rule.ruleDefinition;
		// @+
	}

	public Rule( RuleForm form )
	{
		// @-
		this.groupId        = form.groupId_;
		this.name           = form.name;
		this.description    = form.description;
		this.ruleDefinition = JSON.stringify( form.ruleExec );
		// @+
	}

	@Override
	public Long getId( )
	{
		return id;
	}

	@Override
	public String getName( )
	{
		return name;
	}

	public String getRuleDefinition( )
	{
		return ruleDefinition;
	}

	public void setRuleDefinition( String definition )
	{
		synchronized ( ruleLock )
		{
			this.ruleDefinition = definition;
			this.ruleExecutable = null;
		}
	}

	public ExecutableRule getRuleExecutable( )
	{
		if ( ruleExecutable == null && ruleDefinition != null )
		{
			synchronized ( ruleLock )
			{
				if ( ruleExecutable == null && ruleDefinition != null )
				{
					ruleExecutable = RuleEngine.getInstance( ).readValue( this.ruleDefinition, ExecutableRule.class );
				}
			}
		}

		return ruleExecutable;
	}
}
