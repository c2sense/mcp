
package org.c2sense.mcp.model;



public interface RequiredFieldCondition < T >
{

	public Boolean isFieldRequired( );

	public T fieldValue( );
}
