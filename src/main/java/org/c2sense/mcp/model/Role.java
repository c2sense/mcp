
package org.c2sense.mcp.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;





/**
 * Created by massimo on 09/08/16.
 */
@Entity
@Table( name = "role" )
public class Role
{

	@Transient
	public static String	ADMINISTRATOR		= "ADMINISTRATOR";

	@Transient
	public static String	TEAM_ADMINISTRATOR	= "TEAM_ADMINISTRATOR";

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	public Long				id;

	public String			name;

	public String			description;

}
