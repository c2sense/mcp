
package org.c2sense.mcp.annotation;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;





@Component
public class PrincipalDetailInterceptor extends HandlerInterceptorAdapter
{
	@Override
	public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception
	{
		HandlerMethod handlerMethod = ( HandlerMethod ) handler;
		PrincipalDetailAnnotation principalDetailAnnotation = handlerMethod.getMethodAnnotation( PrincipalDetailAnnotation.class );

		System.out.println( principalDetailAnnotation.teamIdPathParamFinder( ) );

		System.out.println( request.getPathInfo( ) );

		return false;
	}
}
