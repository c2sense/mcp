
package org.c2sense.mcp;



import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.c2sense.mcp.core.McpManagerInstance;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;





//Test

@SpringBootApplication
public class McpApplication extends SpringBootServletInitializer
{

	public static ApplicationContext applicationContext;

	@Override
	protected SpringApplicationBuilder configure( SpringApplicationBuilder application )
	{
		application.sources( McpApplication.class );

		applicationContext = application.run( new String [ ] { } );

		McpManagerInstance.instance.init( );

		System.out.println( "System ready" );

		return application;
	}

	@Override
	public void onStartup( ServletContext servletContext )
		throws ServletException
	{
		// this.servletContext = servletContext;
		System.out.println( "ON Startup" );
		super.onStartup( servletContext );
	}

	public static void main( String [ ] args )
	{
		applicationContext = SpringApplication.run( McpApplication.class, args );

		McpManagerInstance.instance.init( );

		System.out.println( "System ready" );
	}

	public static ApplicationContext getApplicationContext( )
	{
		return applicationContext;
	}
}

/*

*/
