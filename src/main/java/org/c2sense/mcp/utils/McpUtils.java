
package org.c2sense.mcp.utils;



import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;





public class McpUtils
{
	public static < T > String joinList( List < T > listToJoin, CharSequence delimiter )
	{
		StringJoiner joiner = new StringJoiner( delimiter );
		for ( T joinThis : listToJoin )
			joiner.add( joinThis.toString( ) );
		return joiner.toString( );
	}

	public static String joinList( Long [ ] arrayToJoin, String delimiter )
	{
		return joinList( Arrays.asList( arrayToJoin ), delimiter );

	}
}
