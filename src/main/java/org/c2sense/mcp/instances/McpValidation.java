
package org.c2sense.mcp.instances;



import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;





public enum McpValidation
{
	instance;

	private final LocalValidatorFactoryBean validator;

	private McpValidation( )
	{
		this.validator = new LocalValidatorFactoryBean( );
	}

	public javax.validation.Validator getValidator( )
	{
		return instance.validator;
	}
}
