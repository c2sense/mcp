
package org.c2sense.mcp.instances;



import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.google.common.base.Throwables;





public enum JSON
{
	instance;

	private final ObjectMapper objectMapper;

	JSON( )
	{
		this.objectMapper = new ObjectMapper( );

		objectMapper
			.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false )
			.configure( SerializationFeature.INDENT_OUTPUT, false )
			.configure( MapperFeature.AUTO_DETECT_GETTERS, false )
			.configure( MapperFeature.AUTO_DETECT_IS_GETTERS, false )
			.configure( MapperFeature.AUTO_DETECT_SETTERS, false )
			.setSerializationInclusion( Include.NON_NULL )
			.registerModule( new Hibernate5Module( ) );
	}

	public static ObjectMapper getObjectMapper( )
	{
		return instance.objectMapper;
	}

	public static String stringify( Object value )
	{
		try
		{
			return instance.objectMapper.writeValueAsString( value );
		}
		catch ( JsonProcessingException e )
		{
			throw Throwables.propagate( e );
		}
	}

	public static < T > T parse( String content, Class < T > valueType )
	{
		try
		{
			return instance.objectMapper.readValue( content, valueType );
		}
		catch ( IOException e )
		{
			throw Throwables.propagate( e );
		}
	}
}
