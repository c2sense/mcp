
package org.c2sense.mcp.service;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.form.TopicForm;
import org.c2sense.mcp.form.TopicJson;
import org.c2sense.mcp.model.Topic;
import org.c2sense.mcp.model.Topic.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;





@Service
@Transactional
public class TopicService
{

	@Autowired
	private JdbcTemplate	jdbcTemplate;

	@PersistenceContext
	private EntityManager	em;

	public void create( TopicForm form )
	{
		Topic topic = new Topic( form );
		em.persist( topic );
	}

	public void update( Long topicId, TopicForm form )
	{
		Topic topic = em.find( Topic.class, topicId );
		topic.name = form.name;
		topic.description = form.description;
		topic.contentType = ContentType.valueOf( form.contentType );
	}

	public void deleteTopic( Long topicId )
	{
		Topic topic = em.find( Topic.class, topicId );
		em.remove( topic );
	}

	public List < TopicJson > findAllTopics( )
	{
		String sql = "SELECT t.*, GROUP_CONCAT(team_id) AS teams, null AS groups, null AS rules from topic t left join team_topic tt on t.id = tt.topic_id GROUP BY t.name";

		return jdbcTemplate.query( sql, new TopicJson.RowMapper( ) );
	}

	public List < TopicJson > findAllTopicsByTeam( Long teamId )
	{
		String sql = "SELECT t.*, null AS teams, GROUP_CONCAT(group_id) AS groups, null AS rules "
			+ "FROM topic t JOIN team_topic tt ON t.id = tt.topic_id "
			+ "LEFT JOIN group_topic gt ON tt.topic_id = gt.topic_id "
			+ "WHERE tt.team_id = ? "
			+ "GROUP BY t.name";

		return jdbcTemplate.query( sql, new TopicJson.RowMapper( ), teamId );
	}

	public Object findAllTopicsByGroupId( Object teamId, Long groupId )
	{
		String sql = " SELECT t.*, null AS teams, null AS groups, GROUP_CONCAT(rule_id) AS rules "
			+ " FROM topic t JOIN team_topic tt ON t.id = tt.topic_id "
			+ " JOIN group_topic gt ON tt.topic_id = gt.topic_id "
			+ " LEFT JOIN rule_topic rt ON gt.topic_id = rt.topic_id "
			+ " WHERE tt.team_id = ? and gt.group_id = ? "
			+ " GROUP BY t.name ";

		return jdbcTemplate.query( sql, new TopicJson.RowMapper( ), teamId, groupId );
	}

	public List < Long > findAllRuleTopicsByRuleId( Long ruleId )
	{
		String sql = " SELECT tr.topic_id as topic_id"
			+ " FROM rule_topic tr "
			+ " WHERE tr.rule_id = ? ";

		return jdbcTemplate.query( sql, new RowMapper < Long >( )
		{
			@Override
			public Long mapRow( ResultSet rs, int rowNum ) throws SQLException
			{
				return rs.getLong( "topic_id" );
			}
		}, ruleId );
	}

	public void removeRuleTopics( Long ruleId, Long groupId, Collection < Long > topicIds )
	{
		for ( Long topicId : topicIds )
		{
			removeRuleTopic( ruleId, groupId, topicId );
		}
	}

	public void removeRuleTopic( Long ruleId, Long groupId, Long topicId )
	{
		String sql = "DELETE from rule_topic where group_id = ? and rule_id = ? and topic_id = ?";
		jdbcTemplate.update( sql, groupId, ruleId, topicId );
	}

	public List < Topic > findMcpTopicsGraph( )
	{
		return em.createQuery( "from Topic", Topic.class ).getResultList( );
	}
}
