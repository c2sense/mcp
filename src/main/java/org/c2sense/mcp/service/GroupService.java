
package org.c2sense.mcp.service;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.form.GroupForm;
import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.utils.McpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;





@Service
@Transactional
public class GroupService
{
	@Autowired
	private JdbcTemplate	jdbcTemplate;

	@PersistenceContext
	private EntityManager	em;

	public void create( GroupForm form )
	{
		Group createThisGroup = new Group( form );
		em.persist( createThisGroup );
	}

	public void update( GroupForm form )
	{
		Group updateThisGroup = em.find( Group.class, form.groupId_ );
		updateThisGroup.name = form.name;
		updateThisGroup.description = form.description;
		updateThisGroup.mailingList = form.mailingList;
		updateThisGroup.smsNotification = form.isSmsNotificationActive;
		updateThisGroup.userEmailNotification = form.isUserEmailNotificationActive;
		updateThisGroup.facebookEmail = form.facebookEmail;
		updateThisGroup.twitterEmail = form.twitterEmail;
	}

	public void addUsersToGroup( Long [ ] userIds, Long teamId, Long groupId )
	{
		String userIdsToString = McpUtils.joinList( userIds, "," );

		StringBuilder sql = new StringBuilder( " insert into group_user ( team_id, group_id, user_id ) " );
		sql.append( "select ? as team_id, ? as group_id, u.id as user_id " )
			.append( " from " )
			.append( " user u " )
			.append( " left join group_user g on ( g.group_id = ? and u.id = g.user_id ) " )
			.append( " where " )
			.append( " u.id in " )
			.append( " ( " ).append( userIdsToString ).append( " ) " )
			.append( " and " )
			.append( " g.group_id is null " );

		jdbcTemplate.update( sql.toString( ), teamId, groupId, groupId );
	}

	public void removeUsersFromGroup( Long [ ] userIds, Long groupId )
	{
		String userIdsToString = McpUtils.joinList( userIds, "," );
		StringBuilder sql = new StringBuilder( "delete from group_user where group_id = ? and EXISTS (select * from user  where  user_id  =  id and  id in (" )
			.append( userIdsToString )
			.append( " )) " );
		jdbcTemplate.update( sql.toString( ), groupId );
	}

	public void addTopicsToGroup( Long [ ] topicIds, Long teamId, Long groupId )
	{
		String topicIdsToString = McpUtils.joinList( topicIds, "," );

		StringBuilder sql = new StringBuilder( " insert into group_topic ( team_id, group_id, topic_id ) " );
		sql.append( "select ? as team_id, ? as group_id, t.id as topic_id " )
			.append( " from " )
			.append( " topic t " )
			.append( " left join group_topic g on ( g.group_id = ? and t.id = g.topic_id ) " )
			.append( " where " )
			.append( " t.id in " )
			.append( " ( " ).append( topicIdsToString ).append( " ) " )
			.append( " and " )
			.append( " g.group_id is null " );

		jdbcTemplate.update( sql.toString( ), teamId, groupId, groupId );

	}

	public void removeTopicsFromGroup( Long [ ] topicIds, Long groupId )
	{
		String topicIdsToString = McpUtils.joinList( topicIds, "," );
		StringBuilder sql = new StringBuilder( "delete from group_topic where group_id = ? and EXISTS (select * from topic where topic_id  =  id and  id in (" )
			.append( topicIdsToString )
			.append( " )) " );
		jdbcTemplate.update( sql.toString( ), groupId );
	}

	public void deleteGroup( Long teamId, Long groupId )
	{
		Group group = em.createQuery( "from Group g where g.teamId = :teamId and g.id = :groupId", Group.class )
			.setParameter( "teamId", teamId )
			.setParameter( "groupId", groupId )
			.getSingleResult( );
		em.remove( group );

	}
}

// @-
/*
	public List < User > findAllUsersWithTheMightySql( )
	{
		Session hibernateSession = em.unwrap( Session.class );

		hibernateSession.doWork( new org.hibernate.jdbc.Work( )
		{
			@Override
			public void execute( Connection connection ) throws SQLException
			{
			}
		} );

		throw new RuntimeException( "Not implemented" );
	}
*/
// @+
