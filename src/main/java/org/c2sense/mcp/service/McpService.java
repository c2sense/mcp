
package org.c2sense.mcp.service;



import static java.lang.String.format;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.model.Team;
import org.c2sense.mcp.model.Topic;
import org.c2sense.mcp.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;





@Service
@Transactional
public class McpService
{

	protected static final Logger	logger	= LoggerFactory.getLogger( McpService.class );

	@PersistenceContext
	private EntityManager			em;

	public List < Team > fetchMcpGraph( )
	{

		return em.createQuery(
			"select distinct(t) from Team t "
				+ "left join fetch t.users tu "
				+ "left join fetch t.topics tt "
				+ "left join fetch t.groups g "
				+ "left join fetch g.rules r "
				+ "left join fetch g.topics gt "
				+ "left join fetch g.users gu "
				+ "left join fetch r.topics rt "
				+ "order by t.name asc, tu.name asc, tt.name asc, g.name asc, r.name asc, gt.name asc, gu.name asc, rt.name asc",
			Team.class ).getResultList( );
	}

	public static void printMcpGraph( List < Team > teams )
	{
		for ( Team team : teams )
		{
			p( "visiting team [%s] %s", team.id, team.name );

			if ( team.users != null )
			{
				for ( User user : team.users )
				{
					p( "    " + " - user [%s] %s", user.id, user.name );
				}
			}
			if ( team.topics != null )
			{
				for ( Topic topic : team.topics )
				{
					p( "    " + " * topic [%s] %s", topic.id, topic.name );
				}
			}
			if ( team.groups != null )
			{

				for ( Group group : team.groups )
				{
					p( "    " + "visiting group [%s] %s", group.id, group.name );

					if ( group.users != null )
					{
						for ( User user : group.users )
						{
							p( "    " + " -- user [%s] %s", user.id, user.name );
						}
					}
					if ( group.topics != null )
					{
						for ( Topic topic : group.topics )
						{
							p( "    " + " ** topic [%s] %s", topic.id, topic.name );
						}
					}

					if ( group.rules != null )
					{
						for ( Rule rule : group.rules )
						{
							p( "    " + "    " + "visiting rule [%s] %s", rule.id, rule.name );

							if ( rule.topics != null )
							{
								for ( Topic topic : rule.topics )
								{
									p( "    " + "    " + " *** topic [%s] %s", topic.id, topic.name );
								}
							}
						}
					}

				}
			}
		}
	}

	public static void p( String format, Object... args )
	{
		logger.debug( format( format, args ) );
		System.out.println( format( format, args ) );
	}

	public static void printMcpTopics( List < Topic > topics )
	{
		for ( Topic topic : topics )
		{
			logger.debug( "Topic found: " + topic.name );
		}
	}
}
