
package org.c2sense.mcp.service;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.form.UserJson;
import org.c2sense.mcp.model.User;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;





@Service
@Transactional
public class UserService
{
	@Autowired
	private JdbcTemplate	jdbcTemplate;

	@PersistenceContext
	private EntityManager	em;

	public List < UserJson > findAllUsers( )
	{
		String sql = "SELECT u.*, GROUP_CONCAT(tu.team_id) as teams, GROUP_CONCAT(ta.team_id) as team_admins, null as groups from user u left join team_user tu on u.id = tu.user_id left join team_admin ta on u.id = ta.user_id GROUP BY u.name;";

		return jdbcTemplate.query( sql, new UserJson.RowMapper( ) );
	}

	public List < UserJson > findAllUsersByTeam( Long teamId )
	{
		String sql = "SELECT u.*, null AS teams, null as team_admins, GROUP_CONCAT(group_id) AS groups "
			+ "FROM user u JOIN team_user tu ON u.id = tu.user_id "
			+ "LEFT JOIN group_user gu ON tu.user_id = gu.user_id "
			+ "WHERE tu.team_id = ? "
			+ "GROUP BY u.name";

		return jdbcTemplate.query( sql, new UserJson.RowMapper( ), teamId );
	}

	public List < User > findAllUsersWithTheMightySql( )
	{
		Session hibernateSession = em.unwrap( Session.class );

		hibernateSession.doWork( new org.hibernate.jdbc.Work( )
		{
			@Override
			public void execute( Connection connection ) throws SQLException
			{
			}
		} );

		throw new RuntimeException( "Not implemented" );
	}

	public List < User > findAllUsersByGroupId( Long groupId )
	{
		String sql = " SELECT u.* "
			+ "FROM user u JOIN group_user gu ON u.id = gu.user_id "
			+ "WHERE gu.group_id = ? "
			+ "order by u.name";

		return jdbcTemplate.query( sql, new User.RowMapper( ), groupId );
	}
}

/*



















*/
