
package org.c2sense.mcp.service;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.form.RuleForm;
import org.c2sense.mcp.instances.JSON;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.utils.McpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;





@Transactional
@Service
public class RuleService
{
	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private JdbcTemplate	jdbcTemplate;

	@Autowired
	private TopicService	topicService;

	public void create( RuleForm form )
	{
		Rule createThisRule = new Rule( form );
		em.persist( createThisRule );
		em.flush( );

		addTopicsToRule( createThisRule.id, form.groupId_, form.topicIds );
	}

	public void deleteRule( Long ruleId )
	{
		Rule rule = em.find( Rule.class, ruleId );
		em.remove( rule );
	}

	public void update( RuleForm form )
	{
		Rule rule = em.find( Rule.class, form.ruleId_ );

		// @-
		rule.id          = form.ruleId_;
		rule.groupId     = form.groupId_;
		rule.name        = form.name;
		rule.description = form.description;
		rule.setRuleDefinition( JSON.stringify( form.ruleExec ) );
		// @+

		this.manageTopics( form );
	}

	public Rule findById( Long id )
	{
		return em.find( Rule.class, id );
	}

	public Rule findByName( String name )
	{
		return em.createQuery( "from Rule r where r.name = :name ", Rule.class ).setParameter( "name", name ).getSingleResult( );
	}

	private void manageTopics( RuleForm form )
	{
		List < Long > oldTopicIds = topicService.findAllRuleTopicsByRuleId( form.ruleId_ );
		List < Long > newTopicIds = Arrays.asList( form.topicIds );

		Collection < Long > topicIdsToRemove = Collections2.filter(
			oldTopicIds, Predicates.not( Predicates.in( newTopicIds ) ) );

		Collection < Long > topicIdsToAdd = Collections2.filter(
			newTopicIds, Predicates.not( Predicates.in( oldTopicIds ) ) );

		List < Long > addThis = new ArrayList < Long >( topicIdsToAdd );

		topicService.removeRuleTopics( form.ruleId_, form.groupId_, topicIdsToRemove );

		addTopicsToRule( form.ruleId_, form.groupId_, addThis );
	}

	private void addTopicsToRule( Long ruleId, Long groupId, List < Long > topicIds )
	{
		if ( topicIds.isEmpty( ) )
		{
			return;
		}
		String topicIdsToString = McpUtils.joinList( topicIds, "," );
		addTopicsToRuleExecuteQuery( ruleId, groupId, topicIdsToString );
	}

	private void addTopicsToRuleExecuteQuery( Long ruleId, Long groupId, String topicIdsToString )
	{
		StringBuilder sql = new StringBuilder( " insert into rule_topic ( group_id, rule_id, topic_id ) " );
		sql.append( "select ? as group_id, ? as rule_id, g.topic_id as topic_id " )
			.append( " from " )
			.append( " group_topic g " )
			.append( " where " )
			.append( " g.group_id = ? " )
			.append( " and g.topic_id in " )
			.append( " ( " ).append( topicIdsToString ).append( " ) " );

		jdbcTemplate.update( sql.toString( ), groupId, ruleId, groupId );
	}

	private void addTopicsToRule( Long ruleId, Long groupId, Long [ ] topicIds )
	{
		if ( topicIds.length == 0 )
		{
			return;
		}

		String topicIdsToString = McpUtils.joinList( topicIds, "," );
		addTopicsToRuleExecuteQuery( ruleId, groupId, topicIdsToString );
	}
}
