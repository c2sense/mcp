
package org.c2sense.mcp.service;



import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;





@Service
public class MailService
{
	private static final Logger	logger	= LoggerFactory.getLogger( MailService.class );

	@Autowired
	private JavaMailSender		javaMailSender;

	public void send( String from, String [ ] to, String [ ] cc, String [ ] bcc, String replyTo, String subject, String plainTextBody, Boolean isBodyHtml ) throws MailException, MessagingException
	{
		if ( from == null || to == null || subject == null || plainTextBody == null )
		{
			logger.error( "MailService.send(): missing parameters." );
			throw new MailPreparationException( "Missing required parameters" );
		}

		MimeMessage message = javaMailSender.createMimeMessage( );
		MimeMessageHelper helper = new MimeMessageHelper( message, true );
		helper.setTo( to );

		if ( cc != null )
		{
			helper.setCc( cc );
		}
		if ( bcc != null )
		{
			helper.setBcc( bcc );
		}
		helper.setReplyTo( replyTo );
		helper.setFrom( from );
		helper.setSubject( subject );
		helper.setText( plainTextBody, isBodyHtml );

		// FileSystemResource file = new FileSystemResource( new File( "c:/Sample.jpg" ) );
		// helper.addAttachment( "CoolImage.jpg", file );

		message.setSentDate( new Date( ) );

		logger.info( "################################" );
		logger.info( "##   Message is ready to be sent.." );

		javaMailSender.send( message );

		logger.info( "##   email Sent Successfully!!" );
		logger.info( "################################" );
	}
}
