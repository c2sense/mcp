
package org.c2sense.mcp.service;



import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.model.Message;
import org.springframework.stereotype.Service;





@Service
@Transactional
public class MessageService
{
	@PersistenceContext
	private EntityManager em;

	public Long save( Message message )
	{
		em.persist( message );
		return message.id;
	}

	public Message findMessageById( Long messageId )
	{
		return em.find( Message.class, messageId );
	}
}
