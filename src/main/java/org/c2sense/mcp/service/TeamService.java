
package org.c2sense.mcp.service;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.c2sense.mcp.form.TeamForm;
import org.c2sense.mcp.form.UserJson;
import org.c2sense.mcp.model.Team;
import org.c2sense.mcp.model.User;
import org.c2sense.mcp.utils.McpUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;





@Service
@Transactional
public class TeamService
{
	@Autowired
	private JdbcTemplate	jdbcTemplate;

	@PersistenceContext
	private EntityManager	em;

	public List < Team > fetchMcpTeamGraph( )
	{

		return em.createQuery(
			"select distinct(t) from Team t "
				+ "left join fetch t.users tu "
				+ "left join fetch t.topics tt "
				+ "left join fetch t.groups g "
				+ "left join fetch g.rules r "
				+ "left join fetch g.topics gt "
				+ "left join fetch g.users gu "
				+ "left join fetch r.topics rt "
				+ "order by t.name asc, tu.name asc, tt.name asc, g.name asc, r.name asc, gt.name asc, gu.name asc, rt.name asc",
			Team.class ).getResultList( );
	}

	public void addUsersToTeam( Long [ ] userIds, Long teamId )
	{
		StringBuilder sql = new StringBuilder( " insert into team_user ( team_id, user_id ) " );
		sql.append( "select ? as team_id, u.id as user_id " )
			.append( " from " )
			.append( " user u " )
			.append( " left join team_user t on ( t.team_id = ? and u.id = t.user_id ) " )
			.append( " where " )
			.append( " u.id in " )
			.append( " ( " ).append( McpUtils.joinList( userIds, "," ) ).append( " ) " )
			.append( " and " )
			.append( " t.team_id is null " );

		jdbcTemplate.update( sql.toString( ), teamId, teamId );
	}

	public void removeUsersFromTeam( Long [ ] userIds, Long teamId )
	{

		StringBuilder sql = new StringBuilder( "delete from team_user where  team_id = ? and EXISTS (select * from user  where  user_id  =  id and  id in (" )
			.append( McpUtils.joinList( userIds, "," ) )
			.append( " )) " );
		jdbcTemplate.update( sql.toString( ), teamId );
	}

	public List < UserJson > findAllUsersByTeam( Long teamId )
	{
		String sql = "SELECT u.*, null AS teams, GROUP_CONCAT(group_id) AS groups "
			+ "FROM user u JOIN team_user tu ON u.id = tu.user_id "
			+ "LEFT JOIN group_user gu ON tu.user_id = gu.user_id "
			+ "WHERE tu.team_id = ? "
			+ "GROUP BY u.name";

		return jdbcTemplate.query( sql, new UserJson.RowMapper( ), teamId );
	}

	public List < User > findAllUsersWithTheMightySql( )
	{
		Session hibernateSession = em.unwrap( Session.class );

		hibernateSession.doWork( new org.hibernate.jdbc.Work( )
		{
			@Override
			public void execute( Connection connection ) throws SQLException
			{
			}
		} );

		throw new RuntimeException( "Not implemented" );
	}

	public List < Team > findAllTeams( )
	{
		List < Team > list = em.createQuery( "from Team t order by t.name asc", Team.class ).getResultList( );

		return list;
	}

	public void create( TeamForm form )
	{
		Team team = new Team( form );
		em.persist( team );
	}

	public void update( Long teamId, TeamForm form )
	{
		Team team = em.find( Team.class, teamId );
		team.name = form.name;
		team.description = form.description;
	}

	public void deleteTeam( Long teamId )
	{
		Team team = em.find( Team.class, teamId );
		em.remove( team );
	}

	public void addTopicsToTeam( Long [ ] topicIds, Long teamId )
	{
		StringBuilder sql = new StringBuilder( " insert into team_topic ( team_id, topic_id ) " );
		sql.append( "select ? as team_id, t.id as topic_id " )
			.append( " from " )
			.append( " topic t " )
			.append( " left join team_topic tt on ( tt.team_id = ? and t.id = tt.topic_id ) " )
			.append( " where " )
			.append( " t.id in " )
			.append( " ( " ).append( McpUtils.joinList( topicIds, "," ) ).append( " ) " )
			.append( " and " )
			.append( " tt.team_id is null " );

		jdbcTemplate.update( sql.toString( ), teamId, teamId );
	}

	public void removeTopicsFromTeam( Long [ ] topicIds, Long teamId )
	{

		StringBuilder sql = new StringBuilder( "delete from team_topic where  team_id = ? and EXISTS (select * from topic  where  topic_id  =  id and  id in (" )
			.append( McpUtils.joinList( topicIds, "," ) )
			.append( " )) " );
		jdbcTemplate.update( sql.toString( ), teamId );
	}

	public void addUsersToAdminTeam( Long [ ] userIds, Long teamId )
	{
		StringBuilder sql = new StringBuilder( " insert into team_admin ( team_id, user_id ) " );
		sql.append( "select ? as team_id, u.id as user_id " )
			.append( " from " )
			.append( " user u " )
			.append( " left join team_user t on ( t.team_id = ? and u.id = t.user_id ) " )
			.append( " where " )
			.append( " u.id in " )
			.append( " ( " ).append( McpUtils.joinList( userIds, "," ) ).append( " ) " )
			.append( " and " )
			.append( " t.team_id = ? " );

		jdbcTemplate.update( sql.toString( ), teamId, teamId, teamId );

	}

	public void removeUsersFromTeamAdmin( Long [ ] userIds, Long teamId )
	{
		StringBuilder sql = new StringBuilder( "delete from team_admin where  team_id = ? and EXISTS (select * from user  where  user_id  =  id and  id in (" )
			.append( McpUtils.joinList( userIds, "," ) )
			.append( " )) " );
		jdbcTemplate.update( sql.toString( ), teamId );

	}

}

/*



















*/
