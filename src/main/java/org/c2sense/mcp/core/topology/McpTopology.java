
package org.c2sense.mcp.core.topology;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.model.Team;
import org.c2sense.mcp.model.Topic;





/**
 * Class {@link McpTopology}
 *
 */
public class McpTopology
{
	public final Map < String, Topic >			topics;
	public final Map < String, TopicGroups >	rulesTree;

	/**
	 *
	 * @param topics
	 * @param teams
	 */
	public McpTopology( List < Topic > topics, List < Team > teams )
	{
		// @-
		this.topics    = topics.stream( ).collect( Collectors.toMap( topic -> topic.getName( ), topic -> topic ) );
		this.rulesTree = buildRulesTree( teams );
		// @+
	}

	public static class TopicGroups
	{
		public final Topic						topic;
		public final Map < Long, GroupRules >	groups	= new LinkedHashMap <>( );

		public TopicGroups( Topic topic )
		{
			this.topic = topic;
		}
	}

	public static class GroupRules
	{
		public final Group			group;
		public final List < Rule >	rules	= new ArrayList <>( );

		public GroupRules( Group group )
		{
			this.group = group;
		}
	}

	/**
	 *
	 * @param teams
	 * @return the updated Topics / Groups / Rules tree
	 */
	private Map < String, TopicGroups > buildRulesTree( final List < Team > teams )
	{
		final Map < String, TopicGroups > rulesTree = new HashMap <>( );

		for ( Team team : teams )
		{
			for ( Group group : team.groups )
			{
				for ( Rule rule : group.rules )
				{
					for ( Topic topic : rule.topics )
					{
						if ( ! rulesTree.containsKey( topic.name ) )
						{
							rulesTree.put( topic.name, new TopicGroups( topic ) );
						}

						final TopicGroups topicGroups = rulesTree.get( topic.name );

						if ( ! topicGroups.groups.containsKey( group.id ) )
						{
							topicGroups.groups.put( group.id, new GroupRules( group ) );
						}

						topicGroups.groups.get( group.id ).rules.add( rule );
					}
				}
			}
		}

		return rulesTree;
	}
}

/*

*/
