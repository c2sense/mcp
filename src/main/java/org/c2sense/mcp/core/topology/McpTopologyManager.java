
package org.c2sense.mcp.core.topology;



import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

import org.c2sense.mcp.McpApplication;
import org.c2sense.mcp.model.Team;
import org.c2sense.mcp.model.Topic;
import org.c2sense.mcp.service.McpService;
import org.c2sense.mcp.service.TeamService;
import org.c2sense.mcp.service.TopicService;

import com.google.common.eventbus.EventBus;





public class McpTopologyManager
{

	private final AtomicBoolean	mustUpdate		= new AtomicBoolean( false );

	private final AtomicLong	idGenerator		= new AtomicLong( );

	private McpTopology			currentTopology	= null;

	public final EventBus		eventBus		= new EventBus( );

	/**
	 *
	 * @return the current Topology
	 */
	public McpTopology getCurrentTopology( )
	{
		return currentTopology;
	}

	/**
	 * Call this asynchronous method when you know that the underlying topology has been somehow updated
	 *
	 */
	public CompletableFuture < McpTopology > reloadTopologyAsync( )
	{
		return reloadTopology( );
	}

	private CompletableFuture < McpTopology > reloadTopology( )
	{
		if ( mustUpdate.getAndSet( true ) == true )
		{
			// other update events are in the queue, there's no need to spawn multiple processes
			return CompletableFuture.completedFuture( null );
		}

		return CompletableFuture.supplyAsync( new Supplier < McpTopology >( )
		{
			@Override
			public McpTopology get( )
			{
				Long id;

				synchronized ( idGenerator )
				{
					id = idGenerator.incrementAndGet( );
				}

				// from this point onward, an update event will raise the mustUpdate flag up again
				mustUpdate.set( false );

				// @- load the persistent data, build the new topology
				final List < Topic > topics = McpApplication.getApplicationContext( ).getBean( TopicService.class ).findMcpTopicsGraph( );
				final List < Team >  teams  = McpApplication.getApplicationContext( ).getBean( TeamService.class ).fetchMcpTeamGraph( );
				// @+

				McpTopology topology = new McpTopology( topics, teams );

				synchronized ( idGenerator )
				{
					if ( id == idGenerator.get( ) )
					{
						currentTopology = topology;
					}

					eventBus.post( topology );
				}

				McpService.printMcpGraph( teams );
				McpService.printMcpTopics( topics );

				return currentTopology;
			}
		} );
	}
}

/*

*/
