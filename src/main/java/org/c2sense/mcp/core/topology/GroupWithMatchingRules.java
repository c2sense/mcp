
package org.c2sense.mcp.core.topology;



import java.util.ArrayList;
import java.util.List;

import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.Rule;





/**
 * Helper class
 *
 */
public class GroupWithMatchingRules
{
	public final Group		group;
	public final String		topicName;
	public List < Rule >	mathingRules;

	public GroupWithMatchingRules( Group group, String topicName )
	{
		this.group = group;
		this.topicName = topicName;
	}

	public void addMatchingRule( Rule rule )
	{
		if ( mathingRules == null )
		{
			mathingRules = new ArrayList <>( );
		}

		mathingRules.add( rule );
	}
}
