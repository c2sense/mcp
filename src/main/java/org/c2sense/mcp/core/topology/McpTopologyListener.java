
package org.c2sense.mcp.core.topology;



public interface McpTopologyListener
{
	public void listenOnTopologyUpdate( McpTopology updatetdTopology );
}
