
package org.c2sense.mcp.core;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.core.topology.McpTopology;
import org.c2sense.mcp.core.topology.McpTopology.GroupRules;
import org.c2sense.mcp.core.topology.McpTopology.TopicGroups;
import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public class MCPBus
{
	volatile McpTopology currentTopology;

	public MCPBus( )
	{

	}

	/**
	 * Accepts a Topic name and the actual Message received on that Topic, returns a list of Groups that have at least
	 * one Rule matching the specified Message.
	 *
	 * @param topicName
	 * @param message
	 * @return
	 */
	public List < GroupWithMatchingRules > message( String topicName, CommonMessageFormat message )
	{
		final TopicGroups topicGroups = currentTopology.rulesTree.get( topicName );

		if ( topicGroups == null )
		{
			// no Rules for this Topic
			return Collections.emptyList( );
		}

		final List < GroupWithMatchingRules > groupsWithMatchingRules = new ArrayList <>( );

		for ( GroupRules groupRules : topicGroups.groups.values( ) )
		{
			// @-
			final Group         group = groupRules.group;
			final List < Rule > rules = groupRules.rules;
			// @+

			GroupWithMatchingRules groupWithMatchingRules = null;

			for ( Rule rule : rules )
			{
				if ( rule.getRuleExecutable( ).apply( message ) )
				{
					if ( groupWithMatchingRules == null )
					{
						groupWithMatchingRules = new GroupWithMatchingRules( group, topicName );
					}

					groupWithMatchingRules.addMatchingRule( rule );
				}
			}

			if ( groupWithMatchingRules != null )
			{
				groupsWithMatchingRules.add( groupWithMatchingRules );
			}
		}

		return groupsWithMatchingRules;
	}
}

/*

*/
