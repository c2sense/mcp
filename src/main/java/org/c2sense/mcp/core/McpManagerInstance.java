
package org.c2sense.mcp.core;



import java.util.concurrent.ExecutionException;

import org.c2sense.mcp.core.kafka.KafkaManager;
import org.c2sense.mcp.core.topology.McpTopology;
import org.c2sense.mcp.core.topology.McpTopologyManager;

import com.google.common.base.Throwables;





public enum McpManagerInstance
{
	instance;

	McpTopologyManager	mcpTopologyManager;
	KafkaManager		kafkaManager;

	private McpManagerInstance( )
	{
		this.mcpTopologyManager = new McpTopologyManager( );

		try
		{
			mcpTopologyManager.reloadTopologyAsync( ).get( );
		}
		catch ( InterruptedException | ExecutionException e )
		{
			Throwables.propagate( e );
		}
	}

	public void init( )
	{
		this.kafkaManager = new KafkaManager( );
	}

	public McpTopology getCurrentTopology( )
	{
		return mcpTopologyManager.getCurrentTopology( );
	}

	public McpTopologyManager getCurrentTopologyManager( )
	{
		return mcpTopologyManager;
	}

	public McpManagerInstance fireTopologyUpdateEvent( )
	{
		mcpTopologyManager.reloadTopologyAsync( );

		return this;
	}
}

/*



















*/
