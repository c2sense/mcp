
package org.c2sense.mcp.core.dispatch.distiller.message;



import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;

import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public class EmailMessage extends HumanReadableMessage
{

	public EmailMessage( GroupWithMatchingRules groupWithMatchingRules, CommonMessageFormat commonMessageFormat, Long messageId )
	{
		super( groupWithMatchingRules, commonMessageFormat, messageId );
	}

	@Override
	public String buildBody( )
	{
		String cr = "\n";
		SimpleDateFormat sdf = new SimpleDateFormat( "EEE, d MMM yyyy HH:mm:ss Z" );
		StringBuilder complexMessage = new StringBuilder( "This email has been generated to inform you that an inbound message satisfying some preconfigured filter critera has been detected. " );
		complexMessage
			.append( cr )
			.append( "Please find the original message following this link: " )
			.append( HOST_SERVER )
			.append( getMessageId( ) )
			.append( cr )
			.append( cr )
			.append( cr )
			.append( "=== Message Details ===" )
			.append( cr )
			.append( cr )
			.append( "Received on : " )
			.append( groupWithMatchingRules.topicName )
			.append( cr )
			.append( "Received at : " )
			.append( sdf.format( new Date( ) ) )
			.append( cr )
			.append( "Severity: " )
			.append( commonMessageFormat.getSeverity( ).toString( ) )
			.append( cr )
			.append( "Description: " )
			.append( commonMessageFormat.getContent( ) )
			.append( cr )
			.append( cr )
			.append( cr )
			.append( "=== Recipient Details ===" )
			.append( cr )
			.append( cr )
			.append( "Recipient: " )
			.append( groupWithMatchingRules.group.name )
			.append( cr )
			.append( cr )
			.append( cr )
			.append( "=== Rule Details ===" )
			.append( cr )
			.append( cr )
			.append( "The following Rules have been triggered:" )
			.append( cr )
			.append( groupWithMatchingRules.group.rules.stream( ).map( rule -> rule.name ).collect( Collectors.joining( "\n * " ) ) );

		return complexMessage.toString( );
	}

	@Override
	public String buildSubject( )
	{
		return "[C2-SENSE] - MCP Alert";
	}
}
