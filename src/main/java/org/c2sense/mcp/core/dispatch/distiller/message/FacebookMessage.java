
package org.c2sense.mcp.core.dispatch.distiller.message;



import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public class FacebookMessage extends HumanReadableMessage
{
	public FacebookMessage( GroupWithMatchingRules groupWithMatchingRules, CommonMessageFormat commonMessageFormat, Long messageId )
	{
		super( groupWithMatchingRules, commonMessageFormat, messageId );
	}

	@Override
	public String buildBody( )
	{
		StringBuilder messageBuilder = new StringBuilder( );
		messageBuilder
			.append( "C2-SENSE Alert. An event has been detected for the group " )
			.append( groupWithMatchingRules.group.name )
			.append( ". " )
			.append( HOST_SERVER )
			.append( getMessageId( ) );

		return messageBuilder.toString( );
	}

	@Override
	public String buildSubject( )
	{
		return "";
	}
}
