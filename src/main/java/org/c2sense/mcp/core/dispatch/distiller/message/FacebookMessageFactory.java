
package org.c2sense.mcp.core.dispatch.distiller.message;



import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public class FacebookMessageFactory implements HumanReadableMessageFactory
{
	@Override
	public HumanReadableMessage createMessage( GroupWithMatchingRules groupWithMatchingRules, CommonMessageFormat commonMessageFormat, Long messageId )
	{
		return new FacebookMessage( groupWithMatchingRules, commonMessageFormat, messageId );
	}
}
