
package org.c2sense.mcp.core.dispatch.distiller.message;



import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public interface HumanReadableMessageFactory
{
	HumanReadableMessage createMessage( GroupWithMatchingRules groupWithMatchingRules, CommonMessageFormat commonMessageFormat, Long messageId );
}
