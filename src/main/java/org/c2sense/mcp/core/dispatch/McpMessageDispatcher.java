
package org.c2sense.mcp.core.dispatch;



import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.c2sense.mcp.McpApplication;
import org.c2sense.mcp.core.dispatch.distiller.McpDistiller;
import org.c2sense.mcp.core.dispatch.distiller.message.HumanReadableMessage;
import org.c2sense.mcp.core.dispatch.distiller.message.SmsMessage;
import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.c2sense.mcp.service.MailService;
import org.hibernate.cfg.NotYetImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;

import com.google.common.base.Throwables;





public class McpMessageDispatcher implements Runnable
{
	protected static final Logger					logger		= LoggerFactory.getLogger( McpMessageDispatcher.class );
	private static String							NO_REPLY	= "no-reply@c2sense.org";

	protected final List < GroupWithMatchingRules >	groupsWithMatchingRules;
	protected final CommonMessageFormat				commonMessageFormat;
	protected final McpDistiller					mcpDistiller;

	private MailService								mailService;

	public McpMessageDispatcher( List < GroupWithMatchingRules > groupdWithMatchingRules, CommonMessageFormat commonMessageFormat )
	{
		// @-
		this.groupsWithMatchingRules = groupdWithMatchingRules;
		this.commonMessageFormat     = commonMessageFormat;
		this.mcpDistiller            = new McpDistiller( commonMessageFormat );
		// @+
	}

	public void dispatchMessage( String [ ] recipients, HumanReadableMessage message )
	{
		if ( message instanceof SmsMessage )
		{
			throw new NotYetImplementedException( "SMS Getway not yet integrated" );
		}
		else
		{
			try
			{
				getMailService( ).send( NO_REPLY, recipients, null, null, NO_REPLY, message.buildSubject( ), message.buildBody( ), false );
			}
			catch ( MailException | MessagingException e )
			{
				Throwables.propagate( e );
			}
		}
	}

	@Override
	public void run( )
	{
		for ( GroupWithMatchingRules groupWithMatchingRules : groupsWithMatchingRules )
		{
			if ( groupWithMatchingRules.group.isSmsNotificationActive( ) )
			{
				dispatchMessage( groupWithMatchingRules.group.users.stream( ).map( user -> user.phoneNumber ).toArray( String [ ]::new ),
					mcpDistiller.distillSMSMessage( groupWithMatchingRules ) );
			}

			if ( groupWithMatchingRules.group.isUserEmailNotificationActive( ) )
			{
				dispatchMessage( groupWithMatchingRules.group.users.stream( ).map( user -> user.email ).toArray( String [ ]::new ),
					mcpDistiller.distillEmailMessage( groupWithMatchingRules ) );
			}

			if ( groupWithMatchingRules.group.isMailingListNotificationActive( ) )
			{
				dispatchMessage( ( String [ ] ) Arrays.asList( groupWithMatchingRules.group.mailingList ).toArray( ),
					mcpDistiller.distillEmailMessage( groupWithMatchingRules ) );
			}

			if ( groupWithMatchingRules.group.isFacebookEmailNotificationActive( ) )
			{
				dispatchMessage( ( String [ ] ) Arrays.asList( groupWithMatchingRules.group.facebookEmail ).toArray( ),
					mcpDistiller.distillFacebookEmailMessage( groupWithMatchingRules ) );
			}

			if ( groupWithMatchingRules.group.isTwitterEmailNotificationActive( ) )
			{
				dispatchMessage( ( String [ ] ) Arrays.asList( groupWithMatchingRules.group.twitterEmail ).toArray( ),
					mcpDistiller.distillTwitterEmailMessage( groupWithMatchingRules ) );
			}
		}
	}

	private MailService getMailService( )
	{
		if ( mailService == null )
		{
			mailService = McpApplication.getApplicationContext( ).getBean( MailService.class );
		}
		return mailService;
	}
}
