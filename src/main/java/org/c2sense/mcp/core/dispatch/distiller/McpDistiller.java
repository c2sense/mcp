
package org.c2sense.mcp.core.dispatch.distiller;



import org.c2sense.mcp.McpApplication;
import org.c2sense.mcp.core.dispatch.distiller.message.EmailMessageFactory;
import org.c2sense.mcp.core.dispatch.distiller.message.FacebookMessageFactory;
import org.c2sense.mcp.core.dispatch.distiller.message.HumanReadableMessage;
import org.c2sense.mcp.core.dispatch.distiller.message.HumanReadableMessageFactory;
import org.c2sense.mcp.core.dispatch.distiller.message.SmsMessageFactory;
import org.c2sense.mcp.core.dispatch.distiller.message.TwitterMessageFactory;
import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.model.Message;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.c2sense.mcp.service.MessageService;





public class McpDistiller
{
	public enum MessageType
	{
		SMS, FACEBOOK, TWITTER, EMAIL
	}

	private final CommonMessageFormat	commonMessageFormat;
	private HumanReadableMessageFactory	messageFactory;
	private Long						messageId;
	private MessageService				messageService;

	public McpDistiller( CommonMessageFormat commonMessageFormat )
	{
		this.commonMessageFormat = commonMessageFormat;
	}

	public HumanReadableMessage distillSMSMessage( GroupWithMatchingRules groupWithMatchingRules )
	{
		return distillMessage( MessageType.SMS, groupWithMatchingRules );
	}

	public HumanReadableMessage distillFacebookEmailMessage( GroupWithMatchingRules groupWithMatchingRules )
	{
		return distillMessage( MessageType.FACEBOOK, groupWithMatchingRules );
	}

	public HumanReadableMessage distillTwitterEmailMessage( GroupWithMatchingRules groupWithMatchingRules )
	{
		return distillMessage( MessageType.TWITTER, groupWithMatchingRules );
	}

	public HumanReadableMessage distillEmailMessage( GroupWithMatchingRules groupWithMatchingRules )
	{
		messageFactory = new EmailMessageFactory( );
		return distillMessage( MessageType.EMAIL, groupWithMatchingRules );
	}

	public HumanReadableMessage distillMessage( MessageType ofType, GroupWithMatchingRules groupWithMatchingRules )
	{
		return getFactory( ofType ).createMessage( groupWithMatchingRules, commonMessageFormat, getMessageId( ) );
	}

	private HumanReadableMessageFactory getFactory( MessageType ofType )
	{
		switch ( ofType )
		{
			case SMS:
				messageFactory = new SmsMessageFactory( );
				break;
			case FACEBOOK:
				messageFactory = new FacebookMessageFactory( );
				break;
			case TWITTER:
				messageFactory = new TwitterMessageFactory( );
				break;
			case EMAIL:
				messageFactory = new EmailMessageFactory( );
				break;
			default:
				messageFactory = new EmailMessageFactory( );
				break;
		}

		return messageFactory;
	}

	private Long getMessageId( )
	{
		if ( messageId == null )
		{
			messageId = getMessageService( ).save( new Message( commonMessageFormat.toXml( ) ) );
		}
		return messageId;
	}

	private MessageService getMessageService( )
	{
		if ( messageService == null )
		{
			messageService = McpApplication.getApplicationContext( ).getBean( MessageService.class );
		}
		return messageService;
	}
}
