
package org.c2sense.mcp.core.dispatch.distiller.message;



import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public abstract class HumanReadableMessage
{
	public static String				HOST_SERVER	= "http://mcp.itest.c2-sense.eu/messages/";
	public String						subject, body;
	public final Long					messageId;
	public final GroupWithMatchingRules	groupWithMatchingRules;
	public final CommonMessageFormat	commonMessageFormat;

	public HumanReadableMessage( GroupWithMatchingRules groupWithMatchingRules, CommonMessageFormat commonMessageFormat, Long messageId )
	{
		this.groupWithMatchingRules = groupWithMatchingRules;
		this.commonMessageFormat = commonMessageFormat;
		this.messageId = messageId;
	}

	public Long getMessageId( )
	{
		return messageId;
	}

	public abstract String buildBody( );

	public abstract String buildSubject( );

}
