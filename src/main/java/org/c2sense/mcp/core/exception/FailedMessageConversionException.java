
package org.c2sense.mcp.core.exception;



public class FailedMessageConversionException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public FailedMessageConversionException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
