
package org.c2sense.mcp.core.exception;



public class UnknownTopicException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public UnknownTopicException( String message )
	{
		super( message );
	}
}
