
package org.c2sense.mcp.core.kafka;



import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.c2sense.mcp.model.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;





/**
 * Wraps a {@link KafkaConsumer}, offers some limited business logic over the message retrieval
 *
 */
public abstract class KafkaAbstractConsumer implements Runnable
{
	protected static final Logger						logger							= LoggerFactory.getLogger( KafkaAbstractConsumer.class );
	protected static final Logger						messageErrorConversionlogger	= LoggerFactory.getLogger( "message_conversion_error" );
	protected static final SimpleDateFormat				sdf								= new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
	protected static final AtomicInteger				idSequence						= new AtomicInteger( 1 );
	protected static final long							MESSAGE_MAX_AGE					= TimeUnit.MILLISECONDS.convert( 12, TimeUnit.HOURS );

	protected final KafkaConsumer < String, String >	consumer;
	protected final int									id;
	protected final String								groupId;
	protected final ThreadPoolExecutor					dispatchingThreadPoolExecutor;

	/**
	 *
	 * @param id
	 * @param groupId
	 */
	public KafkaAbstractConsumer( String groupId, Properties properties, ThreadPoolExecutor dispatchingThreadPoolExecutor )
	{
		if ( groupId == null )
		{
			throw new IllegalArgumentException( "A KafkaListener cannot be build without specifying a valid Kafka `group.id`" );
		}

		// @-
		this.id       = idSequence.getAndIncrement( );
		this.groupId  = groupId;
		this.consumer = new KafkaConsumer < > ( properties );
		this.dispatchingThreadPoolExecutor = dispatchingThreadPoolExecutor;
		// @+
	}

	@Override
	public void run( )
	{
		try
		{
			while ( true )
			{
				logger.debug( "[KafkaListener {}#{}] Ready to poll NEW records", groupId, id );

				final ConsumerRecords < String, String > records = consumer.poll( Long.MAX_VALUE );

				logger.debug( "[KafkaListener {}#{}] Received {} records", groupId, id, records.count( ) );

				for ( ConsumerRecord < String, String > record : records )
				{
					/* TODO: CAPIRE COME FUNZIONA IL TIMESTAMP DA C2SESB!!!
					if ( ! acceptTimestamp( record.timestamp( ) ) )
					{
						logger.debug( "[KafkaListener {}#{}] An incoming message has been discarded due to its expired timestamp: `{}`",
							groupId, id, sdf.format( new Date( record.timestamp( ) ) ) );

						continue;
					}
					*/

					logger.debug( "[KafkaListener {}#{}] New message retrieved from topic `{}`, with key `{}` and timestamp `{}`...",
						groupId, id, record.topic( ), record.key( ), sdf.format( new Date( record.timestamp( ) ) ) );

					// @-
					String topicName = record.topic( );
					String key       = record.key  ( );
					String value     = record.value( );
					// @+

					onMessageReceived( topicName, value );
				}
			}
		}
		catch ( WakeupException ignore )
		{
		}
		finally
		{
			consumer.close( );
		}
	}

	protected abstract void onMessageReceived( String topicName, String rawPayload );

	protected boolean acceptTimestamp( long timestamp )
	{
		return System.currentTimeMillis( ) - MESSAGE_MAX_AGE < timestamp;
	}

	/**
	 *
	 * @param topics
	 */
	public synchronized void subscribe( List < Topic > topics )
	{
		if ( topics == null )
		{
			consumer.subscribe( Collections.emptyList( ) );
		}
		else
		{
			consumer.subscribe( topics.stream( ).map( ( t ) -> t.name ).collect( Collectors.toList( ) ) );
		}
	}

	/**
	 *
	 */
	public void wakeup( )
	{
		consumer.wakeup( );
	}

	// ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ ノ

	public int getId( )
	{
		return id;
	}

	public String getGroupId( )
	{
		return groupId;
	}

	public boolean isBase64SringTest( String payload )
	{
		final String regex = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( payload );

		return matcher.find( );
	}

	public String decodeBase64String( String rawPayload )
	{
		byte [ ] decoded = Base64.getDecoder( ).decode( rawPayload );
		String dencodedString = StringUtils.toEncodedString( decoded, Charsets.UTF_8 );
		return dencodedString;
	}
}

/*



















*/
