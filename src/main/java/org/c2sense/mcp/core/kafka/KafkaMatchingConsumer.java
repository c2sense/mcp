
package org.c2sense.mcp.core.kafka;



import static java.lang.String.format;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ThreadPoolExecutor;

import javax.xml.transform.Transformer;

import org.apache.commons.io.IOUtils;
import org.c2sense.mcp.core.McpManagerInstance;
import org.c2sense.mcp.core.dispatch.McpMessageDispatcher;
import org.c2sense.mcp.core.exception.FailedMessageConversionException;
import org.c2sense.mcp.core.exception.UnknownTopicException;
import org.c2sense.mcp.core.topology.GroupWithMatchingRules;
import org.c2sense.mcp.core.topology.McpTopology;
import org.c2sense.mcp.core.topology.McpTopology.GroupRules;
import org.c2sense.mcp.core.topology.McpTopology.TopicGroups;
import org.c2sense.mcp.core.topology.McpTopologyListener;
import org.c2sense.mcp.core.topology.McpTopologyManager;
import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.model.Topic;
import org.c2sense.mcp.model.Topic.ContentType;
import org.c2sense.mcp.rulengine.CommonCapMessage;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.c2sense.mcp.utils.C2SDIConverter;
import org.hibernate.cfg.NotYetImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXParseException;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.Subscribe;
import com.google.publicalerts.cap.Alert;
import com.google.publicalerts.cap.CapException;
import com.google.publicalerts.cap.CapXmlParser;
import com.google.publicalerts.cap.NotCapException;





public class KafkaMatchingConsumer extends KafkaAbstractConsumer implements McpTopologyListener
{
	protected static final Logger					loggerKafka				= LoggerFactory.getLogger( KafkaMatchingConsumer.class );
	private volatile McpTopology					currentTopology;
	private final ImmutableMap < Object, Object >	contentTypeParsers;

	public KafkaMatchingConsumer					topologyUpdateListener	= this;

	public KafkaMatchingConsumer( String groupId, Properties properties, ThreadPoolExecutor dispatchingThreadPoolExecutor )
	{
		super( groupId, properties, dispatchingThreadPoolExecutor );

		McpTopologyManager currentTopologyManager = McpManagerInstance.instance.getCurrentTopologyManager( );

		this.currentTopology = currentTopologyManager.getCurrentTopology( );

		topologyUpdateListener.subscribe( new ArrayList < Topic >( currentTopology.topics.values( ) ) );
		currentTopologyManager.eventBus.register( topologyUpdateListener );

		this.contentTypeParsers = ImmutableMap.builder( )
			.put( Topic.ContentType.CAP_XML, new CapXmlParser( true ) )
			.build( );
	}

	@Subscribe
	@Override
	public void listenOnTopologyUpdate( McpTopology updatetdTopology )
	{
		this.currentTopology = updatetdTopology;
	}

	@Override
	protected void onMessageReceived( String topicName, String rawPayload )
	{
		try
		{
			loggerKafka.debug( format( "Message received on Topic: %s", topicName ) );

			// 1. message conversion
			CommonMessageFormat message = convert( topicName, rawPayload );

			// 2. matching rules analysis
			List < GroupWithMatchingRules > tryMatch = tryMatch( topicName, message );

			// 3. dispatching
			if ( ! tryMatch.isEmpty( ) )
			{
				dispatchingThreadPoolExecutor.execute( new McpMessageDispatcher( tryMatch, message ) );
			}
			loggerKafka.debug( format( "No rules apply on Topic %s.", topicName ) );
		}
		catch ( UnknownTopicException | FailedMessageConversionException ignore )
		{
		}
	}

	/**
	 *
	 * @param topicName
	 * @param rawPayload
	 * @return
	 */
	private CommonMessageFormat convert( String topicName, String rawPayload )
	{
		Topic topic = currentTopology.topics.get( topicName );
		CommonMessageFormat message = null;

		if ( topic == null )
		{
			loggerKafka.debug( format( "Topic %s, not found in current topology", topicName ) );
			throw new UnknownTopicException( format( "The topic %s has not been found on the current Topology", topicName ) );
		}

		switch ( topic.contentType )
		{
			case CAP_XML:
				try
				{
					loggerKafka.debug( "Content of the message is CAP" );

					if ( isBase64SringTest( rawPayload ) )
					{
						loggerKafka.debug( " Base64 message detected Content of the message is CAP" );

						rawPayload = decodeBase64String( rawPayload );
					}

					Alert parsedAlert = ( ( CapXmlParser ) contentTypeParsers.get( topic.contentType ) ).parseFrom( rawPayload );
					message = new CommonCapMessage( parsedAlert );
				}
				catch ( NotCapException | SAXParseException | CapException e )
				{
					loggerKafka.error( "Not a cap message, try to convert from edxl.de.xml" );

					try
					{
						Transformer transformer = C2SDIConverter.instance.getTransformer( "edxl.de.xml_cap.xml" );
						loggerKafka.error( "Trovato xslt" );
						StringWriter capConverted = C2SDIConverter.instance.convert( IOUtils.toInputStream( rawPayload, Charsets.UTF_8 ), transformer );
						loggerKafka.error( "String writer compilato" );
						Alert parsedAlert = ( ( CapXmlParser ) contentTypeParsers.get( topic.contentType ) ).parseFrom( capConverted.toString( ) );
						loggerKafka.error( "Creato alert message" );
						message = new CommonCapMessage( parsedAlert );
						return message;
					}
					catch ( NotCapException | SAXParseException | CapException e1 )
					{
						// log the standard exception
						loggerKafka.error( e.getMessage( ), e );

						String log = new StringBuilder( "Conversion error: " + e.getMessage( ) + ". the original message follows: \n\n" )
							.append( rawPayload )
							.toString( );

						// log the offending message
						messageErrorConversionlogger.error( log );
					}
					throw new FailedMessageConversionException( "Failed conversion for an incoming message. Plaese  look at the specific log for the original message contentent.", e );
				}
				loggerKafka.error( "Ritorno cap message" );
				return message;

			default:
			{
				throw new NotYetImplementedException( "The unique content type currently supported is: " + ContentType.CAP_XML );
			}
		}
	}

	/**
	 * Accepts a Topic name and the actual Message received on that Topic, returns a list of Groups that have at least
	 * one Rule matching the specified Message.
	 *
	 * @param topicName
	 * @param message
	 * @return
	 */
	private List < GroupWithMatchingRules > tryMatch( String topicName, CommonMessageFormat message )
	{
		final TopicGroups topicGroups = currentTopology.rulesTree.get( topicName );

		loggerKafka.debug( format( "Cerco regole su topic: %s ", topicName ) );

		if ( topicGroups == null )
		{
			// no Rules for this Topic
			loggerKafka.debug( format( "No rules found on topic %s", topicName ) );
			return Collections.emptyList( );
		}

		final List < GroupWithMatchingRules > groupsWithMatchingRules = new ArrayList <>( );

		for ( GroupRules groupRules : topicGroups.groups.values( ) )
		{
			// @-
			final Group         group = groupRules.group;
			final List < Rule > rules = groupRules.rules;
			// @+

			GroupWithMatchingRules groupWithMatchingRules = null;

			for ( Rule rule : rules )
			{
				loggerKafka.debug( format( "Parsing rule %s", rule.name ) );
				loggerKafka.debug( format( "Rule definition %s", rule.getRuleDefinition( ) ) );
				loggerKafka.debug( format( "Message content %s", message.getContent( ) ) );
				loggerKafka.debug( format( "Executable rule %s", rule.getRuleExecutable( ).toString( ) ) );
				if ( rule.getRuleExecutable( ).apply( message ) )
				{
					if ( groupWithMatchingRules == null )
					{
						groupWithMatchingRules = new GroupWithMatchingRules( group, topicName );
						loggerKafka.debug( format( "Group to be alerted %s", group.name ) );
					}
					loggerKafka.debug( format( "Matched rule %s", rule.name ) );
					groupWithMatchingRules.addMatchingRule( rule );
				}
			}
			if ( groupWithMatchingRules != null )
			{
				groupsWithMatchingRules.add( groupWithMatchingRules );
			}
		}
		return groupsWithMatchingRules;
	}
}
