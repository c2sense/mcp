
package org.c2sense.mcp.core.kafka;



import static java.lang.Math.max;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Throwables;





/**
 * Class {@link KafkaManager}
 *
 */
public class KafkaManager
{
	private static final Properties properties;

	static
	{
		properties = new Properties( );

		try
		{
			properties.load( KafkaManager.class.getClassLoader( ).getResourceAsStream( "application.properties" ) );
		}
		catch ( IOException e )
		{
			Throwables.propagate( e );
		}
	}

	final List < KafkaAbstractConsumer >	consumers;
	final ExecutorService					executor;
	final ThreadPoolExecutor				dispatchingThreadPoolExecutor;
	final int								numConsumers;
	final String							groupId;

	public KafkaManager( )
	{
		// @-
		this.numConsumers = max( 8, Integer.valueOf( (String) properties.get( "number.of.consumers" )));
		this.groupId      = properties.getProperty( "group.id" );
		this.consumers	  = new ArrayList < > ( );
		this.executor     = Executors.newFixedThreadPool( numConsumers );
		// @+

		this.dispatchingThreadPoolExecutor = new ThreadPoolExecutor( numConsumers, numConsumers * 6, 1, TimeUnit.MINUTES,
			new ArrayBlockingQueue <>( max( 30, numConsumers * 10 ) ) );

		for ( int i = 0; i < numConsumers; i ++ )
		{
			KafkaAbstractConsumer consumer = new KafkaMatchingConsumer( groupId, properties, this.dispatchingThreadPoolExecutor );

			consumers.add( consumer );
			executor.submit( consumer );
		}

		Runtime.getRuntime( ).addShutdownHook( new Thread( )
		{
			@Override
			public void run( )
			{
				for ( KafkaAbstractConsumer consumer : consumers )
				{
					consumer.wakeup( );
				}

				executor.shutdown( );

				try
				{
					executor.awaitTermination( 30, TimeUnit.SECONDS );
				}
				catch ( InterruptedException ignore )
				{
				}

				try
				{
					dispatchingThreadPoolExecutor.awaitTermination( 60, TimeUnit.SECONDS );
				}
				catch ( InterruptedException e )
				{
				}
			}
		} );
	}
}

/*

*/
