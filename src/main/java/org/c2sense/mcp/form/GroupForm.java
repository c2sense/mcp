
package org.c2sense.mcp.form;



import javax.validation.constraints.Size;

import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.NamedEntity;
import org.c2sense.mcp.model.RequiredFieldCondition;
import org.c2sense.mcp.validation.AvailableName;
import org.c2sense.mcp.validation.AvailableOrOwnName;
import org.c2sense.mcp.validation.FieldRequiredOnCondition;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;





@AvailableOrOwnName( groups = OnUpdate.class, entityClass = Group.class )
@FieldRequiredOnCondition( groups = { OnInsert.class, OnUpdate.class }, entityClass = Group.class )
public class GroupForm implements NamedEntity < Long >, RequiredFieldCondition < String >
{
	@JsonIgnore
	public Long		teamId_;

	@JsonIgnore
	public Long		groupId_;

	@NotEmpty
	@Size( max = 100 )
	@AvailableName( groups = OnInsert.class, onClass = Group.class )
	public String	name;

	@NotEmpty
	@Size( max = 250 )
	public String	description;

	public Boolean	isSmsNotificationActive;
	public Boolean	isMailingListRequired;
	public Boolean	isUserEmailNotificationActive;
	public Boolean	isFacebookEmailRequired;
	public Boolean	isTwitterEmailRequired;

	@Size( max = 250 )
	@Email
	public String	mailingList;

	@Size( max = 250 )
	@Email
	public String	facebookEmail;

	@Size( max = 250 )
	@Email
	public String	twitterEmail;

	@Override
	public Long getId( )
	{
		return groupId_;
	}

	@Override
	public String getName( )
	{
		return name;
	}

	@Override
	public Boolean isFieldRequired( )
	{
		return isMailingListRequired;
	}

	@Override
	public String fieldValue( )
	{
		return mailingList;
	}
}
