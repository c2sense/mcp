
package org.c2sense.mcp.form;



import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.c2sense.mcp.model.NamedEntity;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.rulengine.spi.ExecutableRule;
import org.c2sense.mcp.validation.AvailableName;
import org.c2sense.mcp.validation.AvailableOrOwnName;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;





@AvailableOrOwnName( entityClass = Rule.class, groups = OnUpdate.class )
public class RuleForm implements NamedEntity < Long >
{
	@JsonIgnore
	public Long				teamId_;

	@JsonIgnore
	public Long				groupId_;

	@JsonIgnore
	public Long				ruleId_;

	@NotEmpty
	public Long [ ]			topicIds;

	@NotEmpty
	@Size( max = 100 )
	@AvailableName( onClass = Rule.class, groups = OnInsert.class )
	public String			name;

	@Size( max = 250 )
	public String			description;

	@Valid
	public ExecutableRule	ruleExec;

	@Override
	public Long getId( )
	{
		return ruleId_;
	}

	@Override
	public String getName( )
	{
		return name;
	}
}
