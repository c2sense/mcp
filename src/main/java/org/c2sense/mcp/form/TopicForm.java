
package org.c2sense.mcp.form;



import javax.validation.constraints.Size;

import org.c2sense.mcp.model.NamedEntity;
import org.c2sense.mcp.model.Topic;
import org.c2sense.mcp.validation.AvailableName;
import org.c2sense.mcp.validation.AvailableOrOwnName;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;





@AvailableOrOwnName( groups = OnUpdate.class, entityClass = Topic.class )
public class TopicForm implements NamedEntity < Long >
{

	@JsonIgnore
	public Long		topicId_;

	@NotEmpty
	@Size( max = 100 )
	@AvailableName( groups = OnInsert.class, onClass = Topic.class )
	public String	name;

	@NotEmpty
	@Size( max = 250 )
	public String	description;

	@NotEmpty
	public String	contentType;

	@Override
	public Long getId( )
	{
		return topicId_;
	}

	@Override
	public String getName( )
	{
		return name;
	}

}
