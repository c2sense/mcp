
package org.c2sense.mcp.form;



import org.c2sense.mcp.instances.JSON;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.rulengine.spi.ExecutableRule;





public class RuleJson extends Rule
{
	public ExecutableRule ruleExec;

	public RuleJson( )
	{
	}

	public RuleJson( Rule rule )
	{
		super( rule );

		this.ruleExec = JSON.parse( rule.getRuleDefinition( ), ExecutableRule.class );
	}
}
