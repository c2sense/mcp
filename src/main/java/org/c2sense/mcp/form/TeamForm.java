
package org.c2sense.mcp.form;



import javax.validation.constraints.Size;

import org.c2sense.mcp.model.NamedEntity;
import org.c2sense.mcp.model.Team;
import org.c2sense.mcp.validation.AvailableName;
import org.c2sense.mcp.validation.AvailableOrOwnName;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;





@AvailableOrOwnName( groups = OnUpdate.class, entityClass = Team.class )
public class TeamForm implements NamedEntity < Long >
{
	@JsonIgnore
	public Long		teamId_;

	@NotEmpty
	@Size( max = 100 )
	@AvailableName( groups = OnInsert.class, onClass = Team.class )
	public String	name;

	@Size( max = 250 )
	public String	description;

	@Override
	public Long getId( )
	{
		return teamId_;
	}

	@Override
	public String getName( )
	{
		return name;
	}
}
