
package org.c2sense.mcp.form;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.c2sense.mcp.model.Topic;





public class TopicJson extends Topic
{
	public List < Long >	teams	= new ArrayList <>( );

	public List < Long >	groups	= new ArrayList <>( );

	public List < Long >	rules	= new ArrayList <>( );

	// public List<Team>

	public void setTeams( String teamIds )
	{
		if ( teamIds == null )
		{
			return;
		}

		String [ ] splittedIds = teamIds.split( "," );

		for ( int i = 0; i < splittedIds.length; i ++ )
		{
			teams.add( Long.parseLong( splittedIds[ i ] ) );
		}
	}

	public void setGroups( String groupIds )
	{
		if ( groupIds == null )
		{
			return;
		}

		String [ ] splittedIds = groupIds.split( "," );

		for ( int i = 0; i < splittedIds.length; i ++ )
		{
			groups.add( Long.parseLong( splittedIds[ i ] ) );
		}
	}

	public void setRules( String ruleIds )
	{
		if ( ruleIds == null )
		{
			return;
		}

		String [ ] splittedIds = ruleIds.split( "," );

		for ( int i = 0; i < splittedIds.length; i ++ )
		{
			rules.add( Long.parseLong( splittedIds[ i ] ) );
		}
	}

	public static class RowMapper implements org.springframework.jdbc.core.RowMapper < TopicJson >
	{
		@Override
		public TopicJson mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			TopicJson topic = new TopicJson( );

			topic.id = rs.getLong( "id" );
			topic.name = rs.getString( "name" );

			topic.setTeams( rs.getString( "teams" ) );
			topic.setGroups( rs.getString( "groups" ) );
			topic.setRules( rs.getString( "rules" ) );

			return topic;
		}
	}
}
