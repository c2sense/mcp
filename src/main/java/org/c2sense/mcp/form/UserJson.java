
package org.c2sense.mcp.form;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.c2sense.mcp.model.User;





public class UserJson extends User
{
	public List < Long >	teams		= new ArrayList <>( );

	public List < Long >	teamAdmins	= new ArrayList <>( );

	public List < Long >	groups		= new ArrayList <>( );

	// public List<Team>

	public void setTeams( String teamIds )
	{
		if ( teamIds == null )
		{
			return;
		}

		String [ ] splittedIds = teamIds.split( "," );

		for ( int i = 0; i < splittedIds.length; i ++ )
		{
			teams.add( Long.parseLong( splittedIds[ i ] ) );
		}
	}

	public void setTeamAdmins( String teamIds )
	{
		if ( teamIds == null )
		{
			return;
		}

		String [ ] splittedIds = teamIds.split( "," );

		for ( int i = 0; i < splittedIds.length; i ++ )
		{
			teamAdmins.add( Long.parseLong( splittedIds[ i ] ) );
		}
	}

	public void setGroups( String groupIds )
	{
		if ( groupIds == null )
		{
			return;
		}

		String [ ] splittedIds = groupIds.split( "," );

		for ( int i = 0; i < splittedIds.length; i ++ )
		{
			groups.add( Long.parseLong( splittedIds[ i ] ) );
		}
	}

	public static class RowMapper implements org.springframework.jdbc.core.RowMapper < UserJson >
	{
		@Override
		public UserJson mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			UserJson user = new UserJson( );

			user.id = rs.getLong( "id" );
			user.name = rs.getString( "name" );
			user.email = rs.getString( "email" );
			user.phoneNumber = rs.getString( "phone_number" );

			user.setTeams( rs.getString( "teams" ) );
			user.setTeamAdmins( rs.getString( "team_admins" ) );
			user.setGroups( rs.getString( "groups" ) );

			return user;
		}
	}
}
