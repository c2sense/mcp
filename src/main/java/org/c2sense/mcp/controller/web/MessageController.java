
package org.c2sense.mcp.controller.web;



import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.c2sense.mcp.model.Message;
import org.c2sense.mcp.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import com.google.common.base.Throwables;





@Controller
@RequestMapping( path = "/messages" )
public class MessageController
{
	@Autowired
	private MessageService messageService;

	@RequestMapping( path = "/{messageId}", method = RequestMethod.GET )
	public ModelAndView getMessage( @PathVariable( required = true ) Long messageId )
	{
		ModelMap model = new ModelMap( );
		Message message = messageService.findMessageById( messageId );

		try
		{
			model.addAttribute( "message", message.getIndentedAlert( ) );
		}
		catch ( ClassNotFoundException | InstantiationException | IllegalAccessException | ClassCastException | TransformerException | ParserConfigurationException | SAXException | IOException e )
		{
			Throwables.propagate( e );
		}
		return new ModelAndView( "/messages", model );
	}

}
