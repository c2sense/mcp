
package org.c2sense.mcp.controller.web;



import org.c2sense.mcp.controller.BaseController;
import org.c2sense.mcp.filter.PrincipalDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;





@Controller
@RequestMapping( "/" )
public class LoginController extends BaseController
{
	@RequestMapping( path = "/login" )
	public ModelAndView login( )
	{
		return new ModelAndView( "/login" );
	}

	@ResponseBody
	@RequestMapping( value = "/rest/loggeduser", method = RequestMethod.GET )
	public String printWelcome( )
	{
		PrincipalDetails details = this.getPrincipalDetails( );
		return details.name;
	}
}

/*



















*/
