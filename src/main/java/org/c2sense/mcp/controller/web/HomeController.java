
package org.c2sense.mcp.controller.web;



import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;





@Controller
@RequestMapping( "/" )
public class HomeController
{
	@RequestMapping( method = RequestMethod.GET )
	public ModelAndView getHomeView( )
	{
		ModelMap model = new ModelMap( );
		return new ModelAndView( "/homepage", model );
	}
}

/*



















*/
