
package org.c2sense.mcp.controller;



import static org.c2sense.mcp.filter.PrincipalDetails.ROLE_ADMINISTRATOR;

import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.c2sense.mcp.core.McpManagerInstance;
import org.c2sense.mcp.filter.PrincipalDetails;
import org.c2sense.mcp.filter.PrincipalDetailsFilter;
import org.c2sense.mcp.web.HttpNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;





@Controller
public abstract class BaseController
{
	@Autowired
	HttpServletRequest				request;

	@Autowired
	Validator						validator;

	Optional < PrincipalDetails >	principalDetails;

	public BaseController			authorization	= this;

	public PrincipalDetails getPrincipalDetails( )
	{
		if ( principalDetails == null )
		{
			try
			{
				principalDetails = Optional.of( PrincipalDetailsFilter.fromRequest( request ) );
			}
			catch ( NullPointerException e )
			{
				throw new HttpNotAllowedException( );
			}
		}

		return principalDetails.get( );
	}

	public BaseController ownsTeam( Long teamId )
	{
		final PrincipalDetails principalDetails = getPrincipalDetails( );

		if ( principalDetails.hasRole( ROLE_ADMINISTRATOR ) || principalDetails.hasTeam( teamId ) )
		{
			return this;
		}

		throw new HttpNotAllowedException( );
	}

	public BaseController ownsGroup( Long groupId )
	{
		final PrincipalDetails principalDetails = getPrincipalDetails( );

		if ( principalDetails.hasRole( ROLE_ADMINISTRATOR ) || principalDetails.hasGroup( groupId ) )
		{
			return this;
		}

		throw new HttpNotAllowedException( );
	}

	public void fireTopologyUpdateEvent( )
	{
		McpManagerInstance.instance.fireTopologyUpdateEvent( );
	}

	public < T > void validate( T object, Class < ? >... groups )
	{
		Set < ConstraintViolation < T > > constraintViolations = validator.validate( object, groups );

		if ( ! constraintViolations.isEmpty( ) )
		{
			throw new ConstraintViolationException( constraintViolations );
		}
	}
}
