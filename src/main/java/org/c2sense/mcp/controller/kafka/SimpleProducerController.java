
package org.c2sense.mcp.controller.kafka;



import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;





@RestController
@RequestMapping( path = "/rest/kafka/" )
public class SimpleProducerController
{
	private static KafkaProducer < String, String > producer;

	@PostConstruct
	public void init( )
	{
		Properties properties = new Properties( );
		// @-
		properties.put( "bootstrap.servers", "37.59.48.153:9092" );
		properties.put("acks"              , "all");
		properties.put("retries"           , 0);
		properties.put("batch.size"        , 16384);
		properties.put("linger.ms"         , 1);
		properties.put("buffer.memory"     , 33554432);
		properties.put("key.serializer"    , "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("value.serializer"  , "org.apache.kafka.common.serialization.StringSerializer");
		// @+

		producer = new KafkaProducer <>( properties );
	}

	public void close( )
	{
		producer.close( );
	}

	@RequestMapping( path = "sendCapPlain/{topicName}", method = RequestMethod.GET )
	public void sendCapPlain( @PathVariable( "topicName" ) String topicName )
	{
		try
		{
			Resource resource = new ClassPathResource( "cap/cap_test_old.xml" );
			InputStream resourceInputStream = resource.getInputStream( );
			String payload = IOUtils.toString( resourceInputStream, Charsets.UTF_8 );

			ProducerRecord < String, String > record = new ProducerRecord <>( topicName, UUID.randomUUID( ).toString( ), payload );
			producer.send( record );
		}
		catch ( IOException e )
		{
			Throwables.propagate( e );
		}
	}

	@RequestMapping( path = "sendCap/{topicName}", method = RequestMethod.GET )
	public void sendCap( @PathVariable( "topicName" ) String topicName )
	{
		try
		{
			Resource resource = new ClassPathResource( "cap/cap_test.xml" );
			InputStream resourceInputStream = resource.getInputStream( );
			String payload = IOUtils.toString( resourceInputStream, Charsets.UTF_8 );

			ProducerRecord < String, String > record = new ProducerRecord <>( topicName, UUID.randomUUID( ).toString( ), payload );
			producer.send( record );
		}
		catch ( IOException e )
		{
			Throwables.propagate( e );
		}
	}

	@RequestMapping( path = "sendCapDe/{topicName}", method = RequestMethod.GET )
	public void sendCapDe( @PathVariable( "topicName" ) String topicName )
	{
		try
		{
			Resource resource = new ClassPathResource( "cap/ie_msg" );
			InputStream resourceInputStream = resource.getInputStream( );
			String payload = IOUtils.toString( resourceInputStream, Charsets.UTF_8 );

			ProducerRecord < String, String > record = new ProducerRecord <>( topicName, UUID.randomUUID( ).toString( ), payload );
			producer.send( record );
		}
		catch ( IOException e )
		{
			Throwables.propagate( e );
		}
	}
}
