
package org.c2sense.mcp.controller.rest;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.groups.Default;

import org.c2sense.mcp.controller.BaseController;
import org.c2sense.mcp.form.RuleForm;
import org.c2sense.mcp.form.RuleJson;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.service.RuleService;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;





@RestController
@RequestMapping( path = "/rest/teams/{teamId}/groups/{groupId}/rules" )
public class RuleResource extends BaseController
{
	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private RuleService		ruleService;

	@RequestMapping( method = RequestMethod.GET )
	public List < Rule > getRules( @PathVariable Long teamId, @PathVariable Long groupId )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		return em.createQuery( "SELECT new Rule( r.id, r.name, r.description ) from Rule r where r.groupId = :groupId order by r.name asc", Rule.class ).setParameter( "groupId", groupId ).getResultList( );
	}

	@RequestMapping( method = RequestMethod.GET, path = "{ruleId}" )
	public Rule getRuleByRuleId( @PathVariable Long teamId, @PathVariable Long groupId, @PathVariable Long ruleId )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		Rule rule = em.createQuery( "from Rule r where r.id = :ruleId", Rule.class ).setParameter( "ruleId", ruleId ).getSingleResult( );
		return new RuleJson( rule );
	}

	@RequestMapping( method = RequestMethod.POST )
	public void createRule( @PathVariable Long teamId, @PathVariable Long groupId, @RequestBody RuleForm form )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		form.teamId_ = teamId;
		form.groupId_ = groupId;
		validate( form, Default.class, OnInsert.class );
		ruleService.create( form );

		fireTopologyUpdateEvent( );
	}

	@RequestMapping( method = RequestMethod.PUT, path = "{ruleId}" )
	public void updateRule( @PathVariable Long teamId, @PathVariable Long groupId, @PathVariable Long ruleId, @RequestBody RuleForm form )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		form.teamId_ = teamId;
		form.groupId_ = groupId;
		form.ruleId_ = ruleId;
		validate( form, Default.class, OnUpdate.class );
		ruleService.update( form );

		fireTopologyUpdateEvent( );
	}

	@RequestMapping( method = RequestMethod.DELETE, path = "{ruleId}" )
	public void deleteRule( @PathVariable Long teamId, @PathVariable Long groupId, @PathVariable Long ruleId )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		ruleService.deleteRule( ruleId );

		fireTopologyUpdateEvent( );
	}
}
