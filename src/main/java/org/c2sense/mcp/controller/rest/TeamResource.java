
package org.c2sense.mcp.controller.rest;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.groups.Default;

import org.c2sense.mcp.controller.BaseController;
import org.c2sense.mcp.filter.PrincipalDetails;
import org.c2sense.mcp.filter.PrincipalDetailsFilter;
import org.c2sense.mcp.form.TeamForm;
import org.c2sense.mcp.model.Team;
import org.c2sense.mcp.service.TeamService;
import org.c2sense.mcp.utils.McpUtils;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;





/* @RolesAllowed( { "ADMINISTRATOR", "TEAM_ADMINISTRATOR" } ) */
@RestController
@RequestMapping( "/rest/teams" )
public class TeamResource extends BaseController
{
	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private TeamService		teamService;

	@RequestMapping( method = RequestMethod.GET )
	public List < Team > getTeams( HttpServletRequest req )
	{
		PrincipalDetails principalDetails = PrincipalDetailsFilter.fromRequest( req );
		String hql;

		if ( principalDetails.hasRole( "ADMINISTRATOR" ) )
		{
			hql = "from Team order by name asc";
		}
		else
		{
			hql = "from Team t where t.id in (" + McpUtils.joinList( principalDetails.teamIds, "," ) + ") order by t.name asc";
		}

		return em.createQuery( hql, Team.class ).getResultList( );
	}

	@RequestMapping( path = "{teamId}", method = RequestMethod.GET )
	public Team getTeamDetails( HttpServletRequest req, @PathVariable( required = true ) Long teamId )
	{
		authorization.ownsTeam( teamId );

		return em.find( Team.class, teamId );
	}

	@RequestMapping( method = RequestMethod.POST )
	public void createTeam( HttpServletRequest req, @RequestBody TeamForm form )
	{
		validate( form, Default.class, OnInsert.class );

		teamService.create( form );
		fireTopologyUpdateEvent( );
	}

	@RequestMapping( path = "{teamId}", method = RequestMethod.PUT )
	public void updateTeam( HttpServletRequest req, @PathVariable( required = true ) Long teamId, @RequestBody TeamForm form )
	{
		authorization.ownsTeam( teamId );

		form.teamId_ = teamId;
		validate( form, Default.class, OnUpdate.class );
		teamService.update( teamId, form );
		fireTopologyUpdateEvent( );
	}

	@RequestMapping( path = "{teamId}", method = RequestMethod.DELETE )
	public void deleteTeam( HttpServletRequest req, @PathVariable( required = true ) Long teamId )
	{
		authorization.ownsTeam( teamId );

		teamService.deleteTeam( teamId );
		fireTopologyUpdateEvent( );
	}
}

/*

*/
