
package org.c2sense.mcp.controller.rest;



import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.c2sense.mcp.controller.BaseController;
import org.c2sense.mcp.form.TeamUserForm;
import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.User;
import org.c2sense.mcp.service.GroupService;
import org.c2sense.mcp.service.TeamService;
import org.c2sense.mcp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;





@RestController
@RequestMapping( "/rest/user" )
public class UserResource extends BaseController
{
	@Autowired
	private UserService		userService;

	@Autowired
	private TeamService		teamService;

	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private GroupService	groupService;

	@RequestMapping( path = "/list" )
	public ImmutableMap < Object, Object > getTeamsAndUsers( )
	{
		return ImmutableMap.builder( )
			.put( "teams", teamService.findAllTeams( ) )
			.put( "users", userService.findAllUsers( ) )
			.build( );
	}

	@RequestMapping( path = "/list/{teamId}" )
	public Map < Object, Object > getTeamGroupsAndUsersByTeamId( @PathVariable( required = true ) Long teamId )
	{
		return ImmutableMap.builder( )
			.put( "groups", em.createQuery( "from Group g where g.teamId = :teamId order by g.name asc", Group.class ).setParameter( "teamId", teamId ).getResultList( ) )
			.put( "users", userService.findAllUsersByTeam( teamId ) )
			.build( );
	}

	@RequestMapping( path = "/list/group/{groupId}", method = RequestMethod.GET )
	public List < User > getGroupUsers( @PathVariable( required = true ) Long groupId )
	{
		return userService.findAllUsersByGroupId( groupId );
	}

	@RequestMapping( path = "/add", method = RequestMethod.POST )
	public void addUsersToTeam( @RequestBody TeamUserForm userForm )
	{
		if ( userForm.teamId != null && userForm.teamId != 0 )
		{
			if ( userForm.groupId != null && userForm.groupId != 0 )
			{
				groupService.addUsersToGroup( userForm.userIds, userForm.teamId, userForm.groupId );
			}
			else
			{
				teamService.addUsersToTeam( userForm.userIds, userForm.teamId );
			}
			fireTopologyUpdateEvent( );
		}
	}

	@RequestMapping( path = "/addAdmin", method = RequestMethod.POST )
	public void addUsersToAdminTeam( @RequestBody TeamUserForm userForm )
	{
		if ( userForm.teamId != null && userForm.teamId != 0 )
		{

			teamService.addUsersToAdminTeam( userForm.userIds, userForm.teamId );

			fireTopologyUpdateEvent( );
		}
	}

	@RequestMapping( path = "/remove", method = RequestMethod.POST )
	public void removeUsersFromTeamOrGroup( @RequestBody TeamUserForm userForm )
	{
		// TODO rivedere questo metodo.
		if ( userForm.teamId != null )
		{
			teamService.removeUsersFromTeam( userForm.userIds, userForm.teamId );
			fireTopologyUpdateEvent( );
		}
		else if ( userForm.groupId != null )
		{
			groupService.removeUsersFromGroup( userForm.userIds, userForm.groupId );
			fireTopologyUpdateEvent( );
		}
	}

	@RequestMapping( path = "/removeAdmin", method = RequestMethod.POST )
	public void removeUsersFromTeamAdmin( @RequestBody TeamUserForm userForm )
	{
		// TODO rivedere questo metodo.
		if ( userForm.teamId != null )
		{
			teamService.removeUsersFromTeamAdmin( userForm.userIds, userForm.teamId );
			fireTopologyUpdateEvent( );
		}
	}
}

/*



















*/
