
package org.c2sense.mcp.controller.rest;



import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.groups.Default;

import org.c2sense.mcp.controller.BaseController;
import org.c2sense.mcp.form.TeamTopicForm;
import org.c2sense.mcp.form.TopicForm;
import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.model.Topic;
import org.c2sense.mcp.service.GroupService;
import org.c2sense.mcp.service.TeamService;
import org.c2sense.mcp.service.TopicService;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;





/* @RolesAllowed( { "ADMINISTRATOR", "TEAM_ADMINISTRATOR" } ) */
@RestController
@RequestMapping( "/rest/topics" )
public class TopicResource extends BaseController
{
	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private TopicService	topicService;

	@Autowired
	private TeamService		teamService;

	@Autowired
	private GroupService	groupService;

	@RequestMapping( method = RequestMethod.GET )
	public ImmutableMap < Object, Object > getTopicsAndTeams( HttpServletRequest req )
	{
		return ImmutableMap.builder( )
			.put( "topics", topicService.findAllTopics( ) )
			.put( "teams", teamService.findAllTeams( ) )
			.build( );
	}

	@RequestMapping( path = "/teams/{teamId}", method = RequestMethod.GET )
	public Map < Object, Object > getTeamTopicsAndGroupsTeamId( @PathVariable Long teamId )
	{
		return ImmutableMap.builder( )
			.put( "topics", topicService.findAllTopicsByTeam( teamId ) )
			.put( "groups", em.createQuery( "from Group g where g.teamId = :teamId order by g.name asc", Group.class ).setParameter( "teamId", teamId ).getResultList( ) )
			.build( );
	}

	@RequestMapping( path = "/teams/{teamId}/groups/{groupId}", method = RequestMethod.GET )
	public Map < Object, Object > getGroupTopicsAndRules( @PathVariable Long teamId, @PathVariable Long groupId )
	{
		return ImmutableMap.builder( )
			.put( "topics", topicService.findAllTopicsByGroupId( teamId, groupId ) )
			.put( "rules", em.createQuery( "from Rule r where r.groupId = :groupId order by r.name asc", Rule.class ).setParameter( "groupId", groupId ).getResultList( ) )
			.build( );
	}

	@RequestMapping( path = "/teams/{teamId}/groups/{groupId}/rules/{ruleId}", method = RequestMethod.GET )
	public Map < Object, Object > getGroupTopics( @PathVariable Long teamId, @PathVariable Long groupId, @PathVariable Long ruleId )
	{
		return ImmutableMap.builder( )
			.put( "topics", topicService.findAllTopicsByGroupId( teamId, groupId ) )
			.build( );
	}

	@RequestMapping( path = "{topicId}", method = RequestMethod.GET )
	public Topic getTopicDetails( HttpServletRequest req, @PathVariable Long topicId )
	{
		return em.find( Topic.class, topicId );
	}

	@RequestMapping( method = RequestMethod.POST )
	public void createTopic( HttpServletRequest req, @RequestBody TopicForm form )
	{
		validate( form, Default.class, OnInsert.class );
		topicService.create( form );
		fireTopologyUpdateEvent( );
	}

	@RequestMapping( path = "{topicId}", method = RequestMethod.PUT )
	public void updateTopic( HttpServletRequest req, @PathVariable Long topicId, @RequestBody TopicForm form )
	{
		form.topicId_ = topicId;
		validate( form, Default.class, OnUpdate.class );
		topicService.update( topicId, form );
		fireTopologyUpdateEvent( );
	}

	@RequestMapping( path = "{topicId}", method = RequestMethod.DELETE )
	public void deleteTopic( HttpServletRequest req, @PathVariable Long topicId )
	{
		topicService.deleteTopic( topicId );
		fireTopologyUpdateEvent( );
	}

	@RequestMapping( method = RequestMethod.POST, value = "/add" )
	public void addTopicsToTeamOrGroup( @RequestBody TeamTopicForm teamTopicForm )
	{
		if ( teamTopicForm.teamId != null )
		{
			authorization.ownsTeam( teamTopicForm.teamId );

			if ( teamTopicForm.groupId != null )
			{
				authorization.ownsGroup( teamTopicForm.groupId );

				groupService.addTopicsToGroup( teamTopicForm.topicIds, teamTopicForm.teamId, teamTopicForm.groupId );
			}
			else
			{
				teamService.addTopicsToTeam( teamTopicForm.topicIds, teamTopicForm.teamId );
			}
			fireTopologyUpdateEvent( );
		}
	}

	@RequestMapping( method = RequestMethod.POST, value = "/remove" )
	public void removeTopicsFromTeamOrGroup( @RequestBody TeamTopicForm teamTopicForm )
	{
		if ( teamTopicForm.teamId != null )
		{
			authorization.ownsTeam( teamTopicForm.teamId );

			teamService.removeTopicsFromTeam( teamTopicForm.topicIds, teamTopicForm.teamId );
			fireTopologyUpdateEvent( );
		}
		else if ( teamTopicForm.groupId != null )
		{
			authorization.ownsGroup( teamTopicForm.groupId );

			groupService.removeTopicsFromGroup( teamTopicForm.topicIds, teamTopicForm.groupId );
			fireTopologyUpdateEvent( );
		}
	}
}

// @-
/*

*/
// @+
