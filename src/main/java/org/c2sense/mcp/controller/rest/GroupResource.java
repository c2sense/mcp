
package org.c2sense.mcp.controller.rest;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.groups.Default;

import org.c2sense.mcp.controller.BaseController;
import org.c2sense.mcp.form.GroupForm;
import org.c2sense.mcp.model.Group;
import org.c2sense.mcp.service.GroupService;
import org.c2sense.mcp.validation.groups.OnInsert;
import org.c2sense.mcp.validation.groups.OnUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;





/* @RolesAllowed( { "ADMINISTRATOR", "TEAM_ADMINISTRATOR" } ) */
@RestController
@RequestMapping( path = "/rest/teams/{teamId}/groups" )
public class GroupResource extends BaseController
{
	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private GroupService	groupService;

	@RequestMapping( method = RequestMethod.GET )
	public List < Group > getTeamGroups( @PathVariable( required = true ) Long teamId )
	{
		authorization.ownsTeam( teamId );

		return em.createQuery( "from Group g where g.teamId = :teamId order by g.name asc", Group.class )
			.setParameter( "teamId", teamId )
			.getResultList( );
	}

	@RequestMapping( method = RequestMethod.GET, path = "/{groupId}" )
	public Group getGroup( @PathVariable( required = true ) Long teamId, @PathVariable( required = true ) Long groupId )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		return em.createQuery( "from Group g where g.teamId = :teamId and g.id = :groupId", Group.class )
			.setParameter( "teamId", teamId )
			.setParameter( "groupId", groupId )
			.getSingleResult( );
	}

	@RequestMapping( method = RequestMethod.POST )
	public void createGroup( @PathVariable( required = true ) Long teamId, @RequestBody GroupForm form )
	{
		authorization.ownsTeam( teamId );

		form.teamId_ = teamId;
		validate( form, Default.class, OnInsert.class );
		groupService.create( form );

		fireTopologyUpdateEvent( );
	}

	@RequestMapping( method = RequestMethod.PUT, path = "/{groupId}" )
	public void updateGroup( @PathVariable( required = true ) Long teamId, @PathVariable( required = true ) Long groupId, @RequestBody GroupForm form )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		form.teamId_ = teamId;
		form.groupId_ = groupId;
		validate( form, Default.class, OnUpdate.class );

		groupService.update( form );

		fireTopologyUpdateEvent( );
	}

	@RequestMapping( method = RequestMethod.DELETE, path = "/{groupId}" )
	public void deleteGroup( @PathVariable( required = true ) Long teamId, @PathVariable( required = true ) Long groupId )
	{
		authorization.ownsTeam( teamId ).ownsGroup( groupId );

		groupService.deleteGroup( teamId, groupId );

		fireTopologyUpdateEvent( );
	}
}

/*



















*/
