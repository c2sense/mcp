
package org.c2sense.mcp.controller;



import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.c2sense.mcp.model.ConstraintViolationErrorInfo;
import org.c2sense.mcp.model.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;





@ControllerAdvice
@RestController
public class GlobalExceptionHandler
{
	@ResponseStatus( HttpStatus.UNPROCESSABLE_ENTITY )
	@ExceptionHandler( value = ConstraintViolationException.class )
	public @ResponseBody ErrorInfo handleConstraintViolationException( HttpServletRequest req, ConstraintViolationException ex )
	{
		ConstraintViolationErrorInfo constraintViolationErrorInfo = new ConstraintViolationErrorInfo( req.getRequestURL( ).toString( ), ex );

		return constraintViolationErrorInfo;
	}

}
