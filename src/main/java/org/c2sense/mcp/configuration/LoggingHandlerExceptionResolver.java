
package org.c2sense.mcp.configuration;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;





public class LoggingHandlerExceptionResolver implements HandlerExceptionResolver, Ordered
{
	@Override
	public int getOrder( )
	{
		return Integer.MIN_VALUE; // we're first in line, yay!
	}

	@Override
	public ModelAndView resolveException( HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex )
	{
		ex.printStackTrace( ); // again, you can do better than this ;)
		return null; // trigger other HandlerExceptionResolver's
	}
}
