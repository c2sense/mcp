
package org.c2sense.mcp.configuration.security;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;





@XmlRootElement( name = "credential" )
@XmlAccessorType( XmlAccessType.FIELD )
public class Credential
{
	public String	username;
	public String	password;
	public String	role;
}
