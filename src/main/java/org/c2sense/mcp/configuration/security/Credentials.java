
package org.c2sense.mcp.configuration.security;



import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;





@XmlRootElement( name = "credentials" )
@XmlAccessorType( XmlAccessType.FIELD )
public class Credentials
{
	@XmlElement( name = "credential" )
	List < Credential > credentials;
}
