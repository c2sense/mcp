
package org.c2sense.mcp.configuration.security;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;





@Configuration
@EnableWebSecurity
public class WebApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter
{
	@Autowired
	private McpUserPasswordAuthProvider authProvider;

	public WebApplicationSecurityConfiguration( )
	{
		super( );
	}

	@Override
	protected void configure( final HttpSecurity http ) throws Exception
	{
		// @-

		http
		.csrf().disable()
		.authorizeRequests( )
			.antMatchers( "/login", "/resources/**", "/messages/**" )
				.permitAll( )
		.anyRequest( )
			.authenticated( )
		.and( )
			.formLogin( )
				.loginPage( "/login" )
		.and( )
			.logout( )
				.logoutUrl( "/logout" )
				.deleteCookies( "JSESSIONID" )
				.permitAll( );

		/*
		http
			.csrf( )
				.disable( )
			.authorizeRequests( )
				.antMatchers( "/", "/index", "/login" )
					.permitAll( );
				.antMatchers( "/admin/**" )
					.hasAuthority( "ADMINISTRATOR" )
			.anyRequest( )
				.authenticated( )
			.and( )
			.formLogin( )
				.loginPage( "/login" )
			.and( )
			.logout( )
				.logoutUrl( "/logout" )
				.deleteCookies( "JSESSIONID" )
				.permitAll( );
				*/
		// @+
	}

	@Override
	protected void configure( AuthenticationManagerBuilder auth ) throws Exception
	{
		auth.authenticationProvider( authProvider );
		// @-
		/*
		auth
			.inMemoryAuthentication( )
			.withUser( "user@c2-sense.org" ).password( "password" ).roles( "ADMINISTRATOR" );
		*/
		// @+
	}
}
