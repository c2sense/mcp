
//

package org.c2sense.mcp.configuration.security;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.c2sense.mcp.McpApplication;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;





@Component
public class McpUserPasswordAuthProvider implements AuthenticationProvider
{

	public McpUserPasswordAuthProvider( )
	{
		super( );
	}

	@Override
	public Authentication authenticate( Authentication authentication ) throws AuthenticationException
	{
		final String name = authentication.getName( );
		final String password = authentication.getCredentials( ).toString( );

		JAXBContext jaxbContext;
		try
		{
			Resource credentialsResource = McpApplication.getApplicationContext( ).getResource( "classpath:credentials.xml" );

			jaxbContext = JAXBContext.newInstance( Credentials.class );
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller( );
			Credentials credentials = ( Credentials ) jaxbUnmarshaller.unmarshal( credentialsResource.getFile( ) );

			for ( Credential cred : credentials.credentials )
			{
				System.out.println( cred.username );
				System.out.println( cred.password );

				if ( name.equals( cred.username ) && password.equals( cred.password ) )
				{
					final List < GrantedAuthority > grantedAuths = new ArrayList <>( );
					grantedAuths.add( new SimpleGrantedAuthority( "ADMINISTRATOR" ) );
					final UserDetails principal = new User( name, password, grantedAuths );
					final Authentication auth = new UsernamePasswordAuthenticationToken( principal, password, grantedAuths );
					return auth;
				}
			}
			throw new BadCredentialsException( "Bad Credentials" );
		}
		catch ( JAXBException e )
		{
			return null;
		}
		catch ( IOException e )
		{
			return null;
		}
	}

	@Override
	public boolean supports( Class < ? > authentication )
	{
		return authentication.equals( UsernamePasswordAuthenticationToken.class );
	}

}
