
package org.c2sense.mcp.configuration;



import java.util.List;
import java.util.Properties;

import org.c2sense.mcp.annotation.PrincipalDetailInterceptor;
import org.c2sense.mcp.filter.PrincipalDetailsFilter;
import org.c2sense.mcp.instances.JSON;
import org.c2sense.mcp.instances.McpValidation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.fasterxml.jackson.databind.ObjectMapper;





@EnableWebMvc
@Configuration
public class WebApplicationConfig extends WebMvcConfigurerAdapter
{

	@Value( "${mail.protocol}" )
	private String	protocol;
	@Value( "${mail.host}" )
	private String	host;
	@Value( "${mail.port}" )
	private int		port;
	@Value( "${mail.smtp.auth}" )
	private boolean	auth;
	@Value( "${mail.smtp.starttls.enable}" )
	private boolean	starttls;
	@Value( "${mail.username}" )
	private String	username;
	@Value( "${mail.password}" )
	private String	password;

	public WebApplicationConfig( )
	{
		super( );
	}

	@Bean
	public FilterRegistrationBean principalDetailsFilterRegistration( PrincipalDetailsFilter principalDetailsFilter )
	{
		FilterRegistrationBean registrationBean = new FilterRegistrationBean( );
		registrationBean.setFilter( principalDetailsFilter );
		registrationBean.addUrlPatterns( "/*" );

		return registrationBean;
	}

	@Bean
	public InterceptorRegistration principalDetailsInterceptorRegistration( PrincipalDetailInterceptor principalDetailInterceptor )
	{
		return new InterceptorRegistration( principalDetailInterceptor );
	}

	// @-
	/*
	@Override
	public void configureHandlerExceptionResolvers( List < HandlerExceptionResolver > exceptionResolvers )
	{
		super.configureHandlerExceptionResolvers( exceptionResolvers );

		exceptionResolvers.add( new LoggingHandlerExceptionResolver( ) );
		exceptionResolvers.add( new ExceptionHandlerExceptionResolver( ) );
		exceptionResolvers.add( new ResponseStatusExceptionResolver( ) );
	}
	*/
	// @+

	@Bean
	public javax.validation.Validator localValidatorFactoryBean( )
	{
		return McpValidation.instance.getValidator( );
	}

	@Override
	public void configureMessageConverters( List < HttpMessageConverter < ? > > converters )
	{
		converters.add( jacksonMessageConverter( ) );
		super.configureMessageConverters( converters );
	}

	public MappingJackson2HttpMessageConverter jacksonMessageConverter( )
	{
		MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter( );
		messageConverter.setObjectMapper( objectMapper( ) );

		return messageConverter;
	}

	@Bean
	public JavaMailSender createMailSender( )
	{
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl( );
		Properties mailProperties = new Properties( );
		mailProperties.put( "mail.smtp.auth", this.auth );
		mailProperties.put( "mail.smtp.starttls.enable", this.starttls );
		mailSender.setJavaMailProperties( mailProperties );
		mailSender.setHost( this.host );
		mailSender.setPort( this.port );
		mailSender.setProtocol( this.protocol );
		mailSender.setUsername( this.username );
		mailSender.setPassword( this.password );
		return mailSender;
	}

	@Bean
	public ObjectMapper objectMapper( )
	{
		return JSON.getObjectMapper( );
	}

	@Bean
	public ViewResolver getViewResolver( )
	{
		InternalResourceViewResolver resolver = new InternalResourceViewResolver( );
		resolver.setViewClass( JstlView.class );
		resolver.setPrefix( "/WEB-INF/views" );
		resolver.setSuffix( ".jsp" );
		return resolver;
	}

	@Override
	public void configureDefaultServletHandling( DefaultServletHandlerConfigurer configurer )
	{
		configurer.enable( );
	}
}

/*
*/
