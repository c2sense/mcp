
package org.c2sense.mcp.rulengine;



import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.locationtech.spatial4j.context.jts.JtsSpatialContext;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.publicalerts.cap.Alert;
import com.google.publicalerts.cap.Area;
import com.google.publicalerts.cap.CapXmlBuilder;
import com.google.publicalerts.cap.Circle;
import com.google.publicalerts.cap.Info;
import com.google.publicalerts.cap.Info.Severity;
import com.google.publicalerts.cap.Point;
import com.google.publicalerts.cap.Polygon;
import com.vividsolutions.jts.geom.Geometry;

import io.jeo.geom.Geom;
import io.jeo.geom.GeomBuilder;





public class CommonCapMessage implements CommonMessageFormat
{
	private final Alert			alert;
	private List < Geometry >	geometries;
	private CapXmlBuilder		capXmlBuilder	= null;

	public CommonCapMessage( Alert alert )
	{
		this.alert = alert;
	}

	@Override
	public String toXml( )
	{
		if ( capXmlBuilder == null )
		{
			capXmlBuilder = new CapXmlBuilder( );
		}
		return StringEscapeUtils.escapeXml11( capXmlBuilder.toXml( alert ) ).replaceAll( "\\r\\n|\\r|\\n", " " );
	}

	@Override
	public Severity getSeverity( )
	{
		Severity severity = Severity.UNKNOWN_SEVERITY;

		for ( Info info : this.alert.getInfoList( ) )
		{
			// info esco solo se trovo extreme o se arrivo alla fine

			if ( info.getSeverity( ).compareTo( severity ) > 0 )
			{
				severity = info.getSeverity( );
			}

			if ( severity.equals( Severity.EXTREME ) )
			{
				break;
			}
		}

		return severity;
	}

	@Override
	public String getContent( )
	{
		StringBuilder content = new StringBuilder( );

		for ( Info info : this.alert.getInfoList( ) )
		{
			System.out.println( info.getHeadline( ) + "\n" );
			System.out.println( info.getDescription( ) );
			System.out.println( info.getInstruction( ) );

			content.append( "\n" ).append( "\n" )
				.append( info.getHeadline( ) )
				.append( "\n" )
				.append( info.getDescription( ) )
				.append( "\n" )
				.append( info.getInstruction( ) )
				.append( "\n" );
		}

		return content.toString( );
	}

	@Override
	public List < Area > getAreas( )
	{
		Builder < Area > builder = ImmutableList.< Area > builder( );

		for ( Info info : this.alert.getInfoList( ) )
		{
			builder.addAll( info.getAreaList( ) );
		}

		return builder.build( );
	}

	@Override
	public List < Area > getAreasByInfoIndex( int index )
	{
		Builder < Area > builder = ImmutableList.< Area > builder( );

		Info info = this.alert.getInfo( index );
		builder.addAll( info.getAreaList( ) );

		return builder.build( );
	}

	@Override
	public boolean contains( String searchText )
	{
		return getContent( ).indexOf( searchText ) > 0;
	}

	@Override
	public List < Geometry > getGeometries( )
	{
		if ( geometries == null )
		{
			Builder < Geometry > geomertyListBuilder = ImmutableList.< Geometry > builder( );

			for ( Area area : this.getAreas( ) )
			{
				List < Polygon > poligons = area.getPolygonList( );
				for ( Polygon polygon : poligons )
				{
					GeomBuilder build = Geom.build( );
					for ( Point point : polygon.getPointList( ) )
					{
						build.point( point.getLatitude( ), point.getLongitude( ) );
					}

					geomertyListBuilder.add( build.toPolygon( ) );
				}

				List < Circle > circles = area.getCircleList( );
				for ( Circle circle : circles )
				{
					double circleLat = circle.getPoint( ).getLatitude( );
					double circleLon = circle.getPoint( ).getLongitude( );

					org.locationtech.spatial4j.shape.Circle circleShape = JtsSpatialContext.GEO.getShapeFactory( ).circle( circleLat, circleLon, circle.getRadius( ) );
					Geometry circleGeometry = JtsSpatialContext.GEO.getShapeFactory( ).getGeometryFrom( circleShape );

					geomertyListBuilder.add( circleGeometry );
				}
			}
			geometries = geomertyListBuilder.build( );
		}

		return geometries;

	}
}
