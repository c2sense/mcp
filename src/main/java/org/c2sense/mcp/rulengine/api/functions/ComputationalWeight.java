
package org.c2sense.mcp.rulengine.api.functions;



/**
 * Describes the approximate computational weight of a {@link Function}. {@link Function} executors <i>should</i>, but
 * are not forced to, try to execute {@link Function}s with low computational weight first.
 */
public enum ComputationalWeight
{

	LOW( 0 ), MEDIUM( 1 ), HIGH( 2 );

	int weight;

	ComputationalWeight( int weight )
	{
		this.weight = weight;
	}

	public int compare( ComputationalWeight other )
	{
		return Integer.compare( this.weight, other.weight );
	}
}
