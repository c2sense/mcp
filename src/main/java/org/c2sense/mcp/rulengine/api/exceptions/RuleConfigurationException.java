package org.c2sense.mcp.rulengine.api.exceptions;

import org.c2sense.mcp.rulengine.api.functions.Function;

import static java.lang.String.format;

/**
 * An exception thrown when there is a problem with a {@link Function} instantiation / configuration.
 */
public class RuleConfigurationException extends IllegalArgumentException {

    private static final long serialVersionUID = -3474750694511262963L;

    public RuleConfigurationException() {
        super();
    }

    public RuleConfigurationException(Throwable cause) {
        super(cause);
    }

    public RuleConfigurationException(String message, Object... messageArgs) {
        super(format(message, messageArgs));
    }

    public RuleConfigurationException(Throwable cause, String message, Object... messageArgs) {
        super(format(message, messageArgs), cause);
    }
}
