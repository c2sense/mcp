
package org.c2sense.mcp.rulengine.api.functions;



import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.c2sense.mcp.rulengine.api.exceptions.FunctionConfigurationException;

import com.fasterxml.jackson.annotation.JsonTypeInfo;





/**
 * A {@link Function} object, once configured with the most appropriate {@link Configuration}, is capable to evaluate an
 * incoming {@link CommonMessageFormat} message and return a boolean value indicating if it satisfied a set of
 * conditions or not. Each actual implementation of a {@link Function} will provide its own set of conditions.
 *
 * @param <T>
 *            The type of {@link Configuration} this {@link Function} is based upon
 */
@JsonTypeInfo( use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, visible = true )
public interface Function < T extends Configuration >
{
	/**
	 * Returns the estimated computational weight of this function.
	 *
	 * @return The estimated computational weight of this function
	 */
	public ComputationalWeight getComputationalWeight( );

	/**
	 * Returns the {@link Configuration} instance that this {@link Function} is using
	 *
	 * @return the {@link Configuration} instance that this {@link Function} is using
	 */
	@NotNull
	@Valid
	public T getConfiguration( );

	/**
	 * Sets the actual {@link Configuration} instance that this {@link Function} is going to use during its life cycle.
	 * Actual implementations of this interface shall throw a {@link FunctionConfigurationException} if the
	 * configuration is set more than once.
	 *
	 * @param configuration
	 *            The actual {@link Configuration} instance that this {@link Function} is going to use during its life
	 *            cycle
	 */
	public void setConfiguration( T configuration );

	/**
	 * Executes this function using the specified message as argument.
	 *
	 * @param message
	 *            The message to evaluate
	 * @return {@code true} if the execution succeeds (i.e. the given message satisfies the requested criteria),
	 *         {@code false} otherwise
	 */
	public boolean apply( CommonMessageFormat message );
}
