
package org.c2sense.mcp.rulengine.api;



import java.util.List;

import com.google.publicalerts.cap.Area;
import com.google.publicalerts.cap.Info.Severity;
import com.vividsolutions.jts.geom.Geometry;





public interface CommonMessageFormat
{
	public Severity getSeverity( );

	public String getContent( );

	public List < Area > getAreasByInfoIndex( int index );

	public List < Area > getAreas( );

	public List < Geometry > getGeometries( );

	public boolean contains( String searchText );

	public String toXml( );
}
