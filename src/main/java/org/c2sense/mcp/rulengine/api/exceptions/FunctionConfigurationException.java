package org.c2sense.mcp.rulengine.api.exceptions;

import org.c2sense.mcp.rulengine.api.functions.Function;

import static java.lang.String.format;

/**
 * An exception thrown when there is a problem with a {@link Function} instantiation / configuration.
 */
public class FunctionConfigurationException extends IllegalArgumentException {

    private static final long serialVersionUID = -3474750694511262963L;

    public FunctionConfigurationException() {
        super();
    }

    public FunctionConfigurationException(Throwable cause) {
        super(cause);
    }

    public FunctionConfigurationException(String message, Object... messageArgs) {
        super(format(message, messageArgs));
    }

    public FunctionConfigurationException(Throwable cause, String message, Object... messageArgs) {
        super(format(message, messageArgs), cause);
    }
}
