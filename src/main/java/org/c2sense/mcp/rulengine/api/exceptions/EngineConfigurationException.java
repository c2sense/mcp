package org.c2sense.mcp.rulengine.api.exceptions;

import org.c2sense.mcp.rulengine.spi.RuleEngine;

import static java.lang.String.format;

/**
 * An exception thrown when there is a problem with a {@link RuleEngine} instantiation / configuration.
 */
public class EngineConfigurationException extends IllegalArgumentException {

    private static final long serialVersionUID = -3474750694511262963L;

    public EngineConfigurationException() {
        super();
    }

    public EngineConfigurationException(Throwable cause) {
        super(cause);
    }

    public EngineConfigurationException(String message, Object... messageArgs) {
        super(format(message, messageArgs));
    }

    public EngineConfigurationException(Throwable cause, String message, Object... messageArgs) {
        super(format(message, messageArgs), cause);
    }
}
