package org.c2sense.mcp.rulengine.api.functions;

/**
 * Class {@link Configuration}.
 */
public interface Configuration {
    public void validate();
}
