
package org.c2sense.mcp.rulengine;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.c2sense.mcp.model.Rule;
import org.c2sense.mcp.rulengine.spi.ExecutableRule;
import org.c2sense.mcp.rulengine.spi.RuleEngine;
import org.springframework.stereotype.Service;





@Service
@Deprecated
public class RuleManager
{
	private static RuleEngine				ruleEngine;

	// @Autowired
	// private RuleServiceImpl ruleService;

	private Map < String, List < Rule > >	rulesMap	= new HashMap <>( 1 );

	public RuleManager( )
	{
		if ( ruleEngine == null )
		{
			RuleEngine.withDefaultConfiguration( );
			ruleEngine = RuleEngine.getInstance( );
		}
	}

	public Map < String, List < Rule > > getRulesMap( )
	{
		if ( this.rulesMap.isEmpty( ) )
		{
			loadRulesMap( );
		}

		return this.rulesMap;
	}

	public String marshalRules( ExecutableRule rule )
	{
		return ruleEngine.writeValueAsString( rule );
	}

	public ExecutableRule unmashalRules( String ruleJSON )
	{
		return ruleEngine.readValue( ruleJSON, ExecutableRule.class );
	}

	/**
	 * If the topology is updated the map must be reloaded; each service has the responsibility to call this method
	 *
	 * @throws TopicNotFoundException
	 * @throws RuleNotFoundException
	 */
	public void invalidateRulesMap( )
	{

		synchronized ( this.rulesMap )
		{
			loadRulesMap( );
		}
		this.rulesMap.clear( );
	}

	private void loadRulesMap( )
	{

		List < Rule > list;
		// List < Rule > rules = this.ruleService.findAll( );
		// for ( Rule rule : rules )
		// {
		// for ( Topic topic : rule.getTopics( ) )
		// {
		// if ( ! this.rulesMap.containsKey( topic.getName( ) ) )
		// {
		// logger.info( "--- new list topic: " + topic.getName( ) );
		// list = new ArrayList <>( 1 );
		// this.rulesMap.put( topic.getName( ), list );
		// }
		// else
		// {
		// logger.info( "--- add element: " + topic.getName( ) );
		// list = this.rulesMap.get( topic.getName( ) );
		// }
		// list.add( rule );
		// }
		// }
	}
}
