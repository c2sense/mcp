
package org.c2sense.mcp.rulengine.spi.functions;



import org.c2sense.mcp.rulengine.api.functions.Configuration;
import org.c2sense.mcp.rulengine.spi.RuleEngine;





public abstract class AbstractFunctionConfiguration implements Configuration
{
	@Override
	public void validate( )
	{
		RuleEngine.getInstance( ).validate( this );
	}
}
