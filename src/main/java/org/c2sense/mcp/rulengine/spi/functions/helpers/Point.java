package org.c2sense.mcp.rulengine.spi.functions.helpers;

import javax.validation.constraints.NotNull;

public class Point {

    @NotNull
    public Double longitude;
    @NotNull
    public Double latitude;

    public Point() {
    }

    public Point(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Point{" +
                "longitude=" + this.longitude +
                ", latitude=" + this.latitude +
                '}';
    }
}
