
package org.c2sense.mcp.rulengine.spi.functions;



import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.publicalerts.cap.Info.Severity;





public class SeverityFunctionConfiguration extends AbstractFunctionConfiguration
{
	@JsonProperty
	@NotNull
	private Severity severity;

	public Severity getSeverity( )
	{
		return this.severity;
	}

	public void setSeverity( Severity severity )
	{
		this.severity = severity;
	}
}
