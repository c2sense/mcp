
package org.c2sense.mcp.rulengine.spi.functions;



import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;





public class ContentFunctionConfiguration extends AbstractFunctionConfiguration
{

	@JsonProperty
	@NotBlank
	private String expression;

	public String getExpression( )
	{
		return this.expression;
	}

	public void setExpression( String expression )
	{
		this.expression = expression;
	}

	@Override
	public String toString( )
	{
		return "ContentFunctionConfiguration{" +
			"expression='" + this.expression + '\'' +
			'}';
	}
}
