
package org.c2sense.mcp.rulengine.spi.functions;



import java.lang.reflect.ParameterizedType;

import org.c2sense.mcp.rulengine.api.exceptions.FunctionConfigurationException;
import org.c2sense.mcp.rulengine.api.functions.ComputationalWeight;
import org.c2sense.mcp.rulengine.api.functions.Configuration;
import org.c2sense.mcp.rulengine.api.functions.Function;

import com.fasterxml.jackson.annotation.JsonProperty;





/**
 * Class {@link AbstractFunction}
 *
 * @param <T>
 *            The type of {@link Configuration} this {@link Function} is based upon
 */
public abstract class AbstractFunction < T extends Configuration > implements Function < T >
{
	/**
	 * The actual configuration object this {@link Function} is going to use
	 */
	protected T configuration;

	@Override
	public ComputationalWeight getComputationalWeight( )
	{
		return ComputationalWeight.LOW;
	}

	@Override
	@JsonProperty
	public T getConfiguration( )
	{
		return this.configuration;
	}

	@Override
	@JsonProperty
	public void setConfiguration( T configuration )
	{
		if ( configuration == null )
		{
			throw new FunctionConfigurationException( "A function configuration cannot be null" );
		}

		if ( ! configuration.getClass( ).isAssignableFrom( getFunctionConfigurationType( ) ) )
		{
			throw new FunctionConfigurationException( "The function %s cannot be configured by a %s configuration object", this.getClass( ), configuration.getClass( ) );
		}

		configuration.validate( );

		this.configuration = configuration;
	}

	@SuppressWarnings( "unchecked" )
	protected final Class < T > getFunctionConfigurationType( )
	{
		return ( Class < T > ) ( ( ParameterizedType ) this.getClass( ).getGenericSuperclass( ) ).getActualTypeArguments( )[ 0 ];
	}
}
