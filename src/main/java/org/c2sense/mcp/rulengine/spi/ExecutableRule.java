
package org.c2sense.mcp.rulengine.spi;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.validation.Valid;

import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.c2sense.mcp.rulengine.api.functions.Function;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;





public final class ExecutableRule
{
	@NotEmpty
	@Valid
	public List < Function < ? > > functions = new ArrayList <>( );

	@JsonProperty
	public List < Function < ? > > getFunctions( )
	{
		return this.functions;
	}

	@JsonProperty
	public void setFunctions( List < Function < ? > > functions )
	{
		Collections.sort( functions, new Comparator < Function < ? > >( )
		{
			@Override
			public int compare( Function < ? > f1, Function < ? > f2 )
			{
				return f1.getComputationalWeight( ).compare( f2.getComputationalWeight( ) );
			}
		} );

		// @-
		// Si può fare anche con le lambda?
		// Collections.sort( functions, ( f1, f2 )-> f1.getComputationalWeight( ).compare( f2.getComputationalWeight()));
		// @+

		this.functions = functions;
	}

	public boolean apply( CommonMessageFormat message )
	{
		for ( Function < ? > function : this.functions )
		{
			if ( ! function.apply( message ) )
			{
				return false;
			}
		}

		return true;
	}

	@Override
	public String toString( )
	{
		return "Rule{" +
			"functions=" + this.functions +
			'}';
	}
}
