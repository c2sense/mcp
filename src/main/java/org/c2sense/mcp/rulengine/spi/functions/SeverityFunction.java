
package org.c2sense.mcp.rulengine.spi.functions;



import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public class SeverityFunction extends AbstractFunction < SeverityFunctionConfiguration >
{
	@Override
	public boolean apply( CommonMessageFormat message )
	{
		return message.getSeverity( ).getNumber( ) <= this.configuration.getSeverity( ).getNumber( );
	}
}
