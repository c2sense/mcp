package org.c2sense.mcp.rulengine.spi;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.Validator;

public interface RuleEngineConfiguration {

    public ObjectMapper geObjectMapper();

    public Validator getValidator();
}
