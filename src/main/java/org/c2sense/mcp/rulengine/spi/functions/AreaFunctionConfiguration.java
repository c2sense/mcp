
package org.c2sense.mcp.rulengine.spi.functions;



import java.util.List;

import javax.validation.Valid;

import org.c2sense.mcp.rulengine.spi.functions.helpers.Polygon;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;





public class AreaFunctionConfiguration extends AbstractFunctionConfiguration
{

	@JsonProperty
	@NotEmpty
	@Valid
	public List < Polygon > polygons;

	@Override
	public String toString( )
	{
		return "AreaFunctionConfiguration";
	}
}
