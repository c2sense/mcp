package org.c2sense.mcp.rulengine.spi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.c2sense.mcp.rulengine.api.exceptions.EngineConfigurationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.Set;

public class RuleEngine {

    private static RuleEngine instance;

    private final ObjectMapper objectMapper;
    private final Validator validator;

    private RuleEngine(RuleEngineConfiguration configuration) {
        this.objectMapper = configuration.geObjectMapper();
        this.validator = configuration.getValidator();
    }

    public static RuleEngine getInstance() {
        if (instance == null) {
            throw new EngineConfigurationException("A RuleEngine instance must be configured before it is possible to use it");
        }

        return instance;
    }

    public static void withDefaultConfiguration() {
        withCustomConfiguration(new RuleEngineDefaultConfiguration());
    }

    public static void withCustomConfiguration(RuleEngineConfiguration configuration) {
        if (configuration == null) {
            throw new EngineConfigurationException("A RuleEngine object cannot be instantiated without an actual configuration");
        }

        if (instance == null) {
            synchronized (configuration) {
                if (instance == null) {
                    instance = new RuleEngine(configuration);

                    return;
                }
            }
        }

        throw new EngineConfigurationException("A RuleEngine object cannot be instantiated twice");
    }

    public ObjectMapper getObjectMapper() {
        return this.objectMapper;
    }

    public Validator getValidator() {
        return this.validator;
    }

    /**
     * Syntactic sugar, with the sole purpose to remove checked exception catching from
     * {@link ObjectMapper#writeValueAsString(Object)} invocations.
     *
     * @param value
     * @return
     */
    public String writeValueAsString(Object value) {
        try {
            return this.objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Syntactic sugar, with the sole purpose to remove checked exception catching from
     * {@link ObjectMapper#readValue(String, Class) invocations.
     *
     * @param value
     * @return
     */
    public <T> T readValue(String content, Class<T> valueType) {
        try {
            return this.getObjectMapper().readValue(content, valueType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Syntactic sugar, around {@link Validator#validate(Object, Class...)}
     *
     * @param object
     * @param groups
     * @return
     */
    public <T> Set<ConstraintViolation<T>> validate(T object, Class<?>... groups) {
        return this.validator.validate(object, groups);
    }
}
