
package org.c2sense.mcp.rulengine.spi.functions;



import java.util.List;

import org.c2sense.mcp.rulengine.api.CommonMessageFormat;
import org.c2sense.mcp.rulengine.api.functions.ComputationalWeight;

import com.google.common.collect.ImmutableCollection.Builder;
import com.google.common.collect.ImmutableList;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;

import io.jeo.geom.Geom;
import io.jeo.geom.GeomBuilder;





public class AreaFunction extends AbstractFunction < AreaFunctionConfiguration >
{

	@Override
	public ComputationalWeight getComputationalWeight( )
	{
		return ComputationalWeight.HIGH;
	}

	@Override
	public boolean apply( CommonMessageFormat message )
	{
		GeometryCollection configuredPolygons = convertConfiguredPoygons( this.configuration.polygons );

		for ( Geometry geometry : message.getGeometries( ) )
		{
			for ( int i = 0; i < configuredPolygons.getNumGeometries( ); i ++ )
			{
				if ( configuredPolygons.getGeometryN( i ).intersects( geometry ) )
				{
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public String toString( )
	{
		return "AreaFunction{" +
			"configuration=" + this.configuration +
			'}';
	}

	private GeometryCollection convertConfiguredPoygons( List < org.c2sense.mcp.rulengine.spi.functions.helpers.Polygon > polygons )
	{
		Builder < com.vividsolutions.jts.geom.Polygon > geometriesBuilder = ImmutableList.< com.vividsolutions.jts.geom.Polygon > builder( );

		for ( org.c2sense.mcp.rulengine.spi.functions.helpers.Polygon polygon : polygons )
		{
			GeomBuilder build = Geom.build( );
			for ( org.c2sense.mcp.rulengine.spi.functions.helpers.Point point : polygon.points )
			{
				build.point( point.getLatitude( ), point.getLongitude( ) );
			}

			geometriesBuilder.add( build.toPolygon( ) );
		}

		return new GeometryCollection( geometriesBuilder.build( ).toArray( new com.vividsolutions.jts.geom.Polygon [ ] { } ), new GeometryFactory( ) );
	}
}
