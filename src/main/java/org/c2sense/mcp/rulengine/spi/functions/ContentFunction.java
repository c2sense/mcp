package org.c2sense.mcp.rulengine.spi.functions;

import org.c2sense.mcp.rulengine.api.CommonMessageFormat;

public class ContentFunction extends AbstractFunction<ContentFunctionConfiguration> {

    @Override
    public boolean apply(CommonMessageFormat message) {
        return message.getContent() != null && message.getContent().toLowerCase().contains(this.configuration.getExpression());
    }

    @Override
    public String toString() {
        return "ContentFunction{" +
                "configuration=" + this.configuration +
                '}';
    }
}
