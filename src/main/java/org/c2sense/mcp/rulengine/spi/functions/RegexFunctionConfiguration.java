
package org.c2sense.mcp.rulengine.spi.functions;



import org.c2sense.mcp.validation.RegularExpression;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;





public class RegexFunctionConfiguration extends AbstractFunctionConfiguration
{
	@JsonProperty
	@NotBlank
	@RegularExpression
	private String regEx;

	public String getRegEx( )
	{
		return regEx;
	}

	public void setRegEx( String regEx )
	{
		this.regEx = regEx;
	}
}
