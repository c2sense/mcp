
package org.c2sense.mcp.rulengine.spi;



import javax.validation.Validator;

import org.c2sense.mcp.instances.JSON;
import org.c2sense.mcp.instances.McpValidation;

import com.fasterxml.jackson.databind.ObjectMapper;





public class RuleEngineDefaultConfiguration implements RuleEngineConfiguration
{

	@Override
	public ObjectMapper geObjectMapper( )
	{

		return JSON.getObjectMapper( );
	}

	@Override
	public Validator getValidator( )
	{
		return McpValidation.instance.getValidator( );
	}
}
