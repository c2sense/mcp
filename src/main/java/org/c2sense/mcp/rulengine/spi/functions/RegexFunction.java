
package org.c2sense.mcp.rulengine.spi.functions;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.c2sense.mcp.rulengine.api.CommonMessageFormat;





public class RegexFunction extends AbstractFunction < RegexFunctionConfiguration >
{

	private Pattern	pattern	= null;
	private Matcher	matcher	= null;

	@Override
	public boolean apply( CommonMessageFormat message )
	{
		pattern = Pattern.compile( this.configuration.getRegEx( ) );
		matcher = pattern.matcher( message.getContent( ) );

		while ( matcher.find( ) )
		{
			for ( int i = 0; i <= matcher.groupCount( ); i ++ )
			{
				if ( matcher.group( i ) != null )
				{
					return true;
				}
			}
		}

		return false;
	}
}
