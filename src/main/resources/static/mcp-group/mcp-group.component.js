'use strict';

angular.
module('mcpGroup').
component('mcpGroup', {
    templateUrl: 'resources/mcp-group/mcp-group.template.html',
    controller: ['$http', '$routeParams', 'Group', function McpGroupController($http, $routeParams, Group) {
        var self = this;
        console.log('mcp-group');
        self.groups = Group.query();

        self.addGroup = function () {
            self.groups.push({name: 'gruppo 3'});
        };
    }]
});