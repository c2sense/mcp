'use strict';

angular.
module('mcpRule').
component('mcpRule', {
    templateUrl: 'resources/mcp-rule/mcp-rule.template.html',
    controller: ['$http', '$routeParams', 'Rule', function McpRuleController($http, $routeParams, Rule) {
        var self = this;
        console.log('mcp-rule');

        self.retrieveRules = function(){
            Rule.query().then(function(data){
                self.rules = data;
            }, function(){
                Rule.showError("Some error occurred.");
            });
        };
        self.retrieveRules();

        /*
         self.addRule = function () {
         "#!/rules/{{rule.id}}"
         };
         */

        self.configureModal = function(ruleId, action){
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedRuleId = ruleId;
        };

        self.activateCommand = function(){
            console.log(self.savedRuleId + " " + self.actionToDo);
            if(self.actionToDo === 'delete'){
                $(".mcp-modal-sm").modal("hide");
                Rule.delete(self.savedRuleId).then(function(data){
                    self.rules = self.rules.filter(function(obj){ return obj.id !== self.savedRuleId; });
                    Rule.showSuccess("Rule successfully deleted.");
                },function(error){
                    Rule.showError(error.ex);
                });
            }
        };
    }]
});