/**
 * Created by Andrea.Sbarra on 30/08/2016.
 */
'use strict';

angular.
module('core.alert').
factory('alertService', ['$rootScope',
    function ($rootScope) {
	 var alertService = {};

	    // create an array of alerts available globally
	    $rootScope.alerts = [];

	    alertService.add = function(type, msg) {
	      $rootScope.alerts.push({'type': type, 'msg': msg});
	    };

	    alertService.closeAlert = function(index) {
	      $rootScope.alerts.splice(index, 1);
	    };

	    return alertService;
    }
]);
