/**
 * Created by Andrea.Sbarra on 30/08/2016.
 */
'use strict';

angular.
module('core.user').
factory('User', ['$http', '$q',
    function ($http, $q) {
        var user;
        var service = {};

        service.query = function (teamId) {
            var _url = 'rest/user/list' ;
            if(!!teamId){
                _url = _url + '/' +  teamId;
            }
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: _url
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.queryGroupUsers = function (groupId) {
            var _url = 'rest/user/list/group/'+groupId ;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: _url
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.get = function (userId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'users/' + userId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.save = function (user) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'users/save',
                data: user
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }
        
        service.saveUserForm = function (userForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'rest/user/add',
                data: userForm
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.saveTeamAdminForm = function (teamAdminForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'rest/user/addAdmin',
                data: teamAdminForm
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }
        
        service.removeUserForm = function (userForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'rest/user/remove',
                data: userForm
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.removeTeamAdminForm = function (userForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'rest/user/removeAdmin',
                data: userForm
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }
        
        service.delete = function (userId) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: 'users/delete/'+userId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.showSuccess = function (message) {
            $("#generalModalTitle").text("Success");
            $("#generalModalText").text(message);
            $('#generalModal').modal('show');
        }

        service.showError = function (message) {
            $("#generalModalTitle").text("Error");
            $("#generalModalText").text(message);
            $('#generalModal').modal('show');
        }

        return service;
    }
]);
