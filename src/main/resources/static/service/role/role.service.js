'use strict';

angular.
module('core.role').
factory('Role', ['$http', '$q',
    function ($http, $q) {
        var service = {};

        service.query = function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'roles/list'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.get = function (roleId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'roles/' + roleId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.save = function (role) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'roles/save',
                data: role
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.delete = function (roleId) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: 'roles/delete/'+roleId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.showSuccess = function (message) {
            $("#generalModalTitle").text("Success");
            $("#generalModalText").text(message);
            $('#generalModal').modal('show');
        }

        service.showError = function (message) {
            $("#generalModalTitle").text("Error");
            $("#generalModalText").text(message);
            $('#generalModal').modal('show');
        }

        return service;
    }
]);
