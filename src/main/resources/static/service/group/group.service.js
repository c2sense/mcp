'use strict';

angular.
module('core.group').
factory('Group', ['$http', '$q',
    function ($http, $q) {
        var service = {};
        var basePath = "/rest/teams/{teamId}/groups";

        function buildBasePathWithTeam(teamId){
            return basePath.replace("{teamId}", teamId);
        }

        function buildBasePathWithTeamAndGroup(teamId, groupId){
            if(!groupId)
            {
                return buildBasePathWithTeam(teamId);
            }
            return buildBasePathWithTeam(teamId) + "/" + groupId;
        }

        service.get = function (teamId, groupId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: buildBasePathWithTeamAndGroup(teamId, groupId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.create = function (teamId, groupForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: buildBasePathWithTeam(teamId),
                data: groupForm
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.update = function (teamId, groupId, groupForm) {
            var deferred = $q.defer();
            $http({
                method: 'PUT',
                url: buildBasePathWithTeamAndGroup(teamId, groupId),
                data: groupForm
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.delete = function (teamId, groupId) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: buildBasePathWithTeamAndGroup(teamId, groupId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        return service;
    }
]);
