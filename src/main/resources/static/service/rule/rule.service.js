'use strict';

angular.
module('core.rule').
factory('Rule', ['$http', '$q',
    function ($http, $q) {
        var service = {};
        var basePath = "/rest/teams/{teamId}/groups/{groupId}/rules";

        function buildBasePathWithTeam(teamId){
            return basePath.replace("{teamId}", teamId);
        }

        function buildBasePathWithTeamAndGroup(teamId, groupId){
            if(!groupId)
            {
                return buildBasePathWithTeam(teamId);
            }
            return buildBasePathWithTeam(teamId).replace("{groupId}", groupId);
        }

        function buildBasePathWithTeamAndGroupRule(teamId, groupId, ruleId){
            if(!ruleId)
            {
                return buildBasePathWithTeamAndGroup(teamId, groupId);
            }
            return buildBasePathWithTeamAndGroup(teamId, groupId) + "/" + ruleId;
        }

        service.query = function (teamId, groupId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: buildBasePathWithTeamAndGroup(teamId, groupId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.get = function (teamId, groupId, ruleId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: buildBasePathWithTeamAndGroup(teamId, groupId) + '/' + ruleId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.create = function (teamId, groupId, rule) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: buildBasePathWithTeamAndGroup(teamId, groupId),
                data: rule
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }
        
        service.save = function (teamId, groupId, ruleId, rule) {
            var deferred = $q.defer();
            $http({
                method: 'PUT',
                url: buildBasePathWithTeamAndGroupRule(teamId, groupId, ruleId),
                data: rule
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.delete = function (teamId, groupId, ruleId) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: buildBasePathWithTeamAndGroup(teamId, groupId) + '/' +  ruleId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        return service;
    }
]);
