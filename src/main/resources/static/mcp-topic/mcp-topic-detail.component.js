'use strict';

angular.
module('mcpTopicDetail').
component('mcpTopicDetail', {
    controller: ['$http', '$routeParams', 'Topic', 'alertService', function McpTopicDetailController($http, $routeParams, Topic, alertService) {
        var self = this;

        self.submitted = false;
        self.topic = {};
        self.errors = [];

        self.contentTypes = [{
            id: "CAP_XML",
            name: "Cap"
        }, {
            id: "CMF_JSON",
            name: "Cmf"
        }];

        if ($routeParams.topicId !== 'create') {
            Topic.get($routeParams.topicId).then(function(data) {
                self.topic = data;
            }, function() {
                alertService.add("warning", "Some error occurred");
            });
        }

        self.save = function() {
            self.errors = [];
            Topic.save(self.topic).then(function(response) {
                self.topic.id = response.id;
                alertService.add("success", "Topic was successfully saved.");
            }, function(response) {
                console.log(response);
                self.errors = response.errors;
                alertService.add("warning", "Some error occured.");
            });
        };

    }],
    templateUrl: 'resources/mcp-topic/mcp-topic-detail.template.html'
});
