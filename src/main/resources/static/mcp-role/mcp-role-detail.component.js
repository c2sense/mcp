'use strict';

angular.
module('mcpRoleDetail').
component('mcpRoleDetail', {
    controller: ['$http', '$routeParams', 'Role', 'Group', function McpRoleDetailController($http, $routeParams, Role, Group) {
        var self = this;

        self.submitted = false;
        self.role = {};

        if ($routeParams.roleId !== 'create') {
            Role.get($routeParams.roleId).then(function (data) {
                self.role = data;
            }, function () {
                Role.showError("Some error occurred");
            });
        }

        self.save = function (isValid) {
            self.submitted = true;
            if (isValid) {
                Role.save(self.role).then(function (response) {
                    self.role.id = response.id;
                    Role.showSuccess("Role successfully saved.");
                }, function (response) {
                    Role.showError(response.ex);
                });
            }else{
                Role.showError("Please fix error before submit the form.");
            }
        };

        self.configureModal = function (groupId, action) {
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedGroupId = groupId;
        };

        self.activateCommand = function () {
            console.log(self.savedGroupId + " " + self.actionToDo);
            if (self.actionToDo === 'delete') {
                $(".mcp-modal-sm").modal("hide");
                Group.delete(self.role.id, self.savedGroupId).then(function (data) {
                    self.role.rolegroup = self.role.rolegroup.filter(function (obj) {
                        return obj.id !== self.savedGroupId;
                    });
                    Role.showSuccess("Group successfully deleted.");

                }, function (error) {
                    Role.showError(error.ex);
                });
            }
        };

    }],
    templateUrl: 'resources/mcp-role/mcp-role-detail.template.html'
});