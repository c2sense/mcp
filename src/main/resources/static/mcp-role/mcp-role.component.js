'use strict';

angular.
module('mcpRole').
component('mcpRole', {
    templateUrl: 'resources/mcp-role/mcp-role.template.html',
    controller: ['$http', '$routeParams', 'Role', function McpRoleController($http, $routeParams, Role) {
        var self = this;
        console.log('mcp-role');

        self.retrieveRoles = function(){
            Role.query().then(function(data){
                self.roles = data;
            }, function(){
                Role.showError("Some error occurred.");
            });
        };
        self.retrieveRoles();

        /*
         self.addRole = function () {
         "#!/roles/{{role.id}}"
         };
         */

        self.configureModal = function(roleId, action){
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedRoleId = roleId;
        };

        self.activateCommand = function(){
            console.log(self.savedRoleId + " " + self.actionToDo);
            if(self.actionToDo === 'delete'){
                $(".mcp-modal-sm").modal("hide");
                Role.delete(self.savedRoleId).then(function(data){
                    self.roles = self.roles.filter(function(obj){ return obj.id !== self.savedRoleId; });
                    Role.showSuccess("Role successfully deleted.");

                },function(error){
                    Role.showError(error.ex);
                });
            }
        };
    }]
});