-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mcp
-- ------------------------------------------------------
-- Server version	5.6.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `group_`
--

DROP TABLE IF EXISTS `group_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_notification` smallint(1) DEFAULT NULL,
  `user_email_notification` smallint(1) DEFAULT NULL,
  `mailing_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `52f56816-6a0d-49bf-b5a2-4c94f8bdd77d_idx` (`team_id`),
  KEY `c279c468-4e19-45fe-a688-ec643e92e9a6_idx` (`id`,`team_id`),
  CONSTRAINT `52f56816-6a0d-49bf-b5a2-4c94f8bdd77d` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_`
--

LOCK TABLES `group_` WRITE;
/*!40000 ALTER TABLE `group_` DISABLE KEYS */;
INSERT INTO `group_` VALUES (5,4,'Volunteers','Volunteers group for Civil Protection of Regione Puglia',1,1,'','facebook@volunteer.com','');
/*!40000 ALTER TABLE `group_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_topic`
--

DROP TABLE IF EXISTS `group_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_topic` (
  `team_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`group_id`,`topic_id`),
  KEY `d184a6dd-032b-4b1f-9004-d5ab8a94f6ba_idx` (`team_id`,`topic_id`),
  KEY `3fd2d98d-0101-49f0-abb3-266e50f8f2c5_idx` (`group_id`,`topic_id`),
  CONSTRAINT `bbc555da-0035-4305-ac99-bced36f21b31` FOREIGN KEY (`team_id`, `group_id`) REFERENCES `group_` (`team_id`, `id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `d184a6dd-032b-4b1f-9004-d5ab8a94f6ba` FOREIGN KEY (`team_id`, `topic_id`) REFERENCES `team_topic` (`team_id`, `topic_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_topic`
--

LOCK TABLES `group_topic` WRITE;
/*!40000 ALTER TABLE `group_topic` DISABLE KEYS */;
INSERT INTO `group_topic` VALUES (4,5,8);
/*!40000 ALTER TABLE `group_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_user`
--

DROP TABLE IF EXISTS `group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_user` (
  `group_id` bigint(20) NOT NULL,
  `team_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`group_id`,`team_id`,`user_id`),
  KEY `a2a7f191-e18d-4509-ab94-ceacbbd32f96_idx` (`team_id`,`user_id`),
  KEY `c279c468-4e19-45fe-a688-ec643e92e9a6_idx` (`group_id`,`team_id`),
  CONSTRAINT `074c9607-8359-4426-b50d-12fb675e1a05` FOREIGN KEY (`team_id`, `user_id`) REFERENCES `team_user` (`team_id`, `user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `c279c468-4e19-45fe-a688-ec643e92e9a6` FOREIGN KEY (`group_id`, `team_id`) REFERENCES `group_` (`id`, `team_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_user`
--

LOCK TABLES `group_user` WRITE;
/*!40000 ALTER TABLE `group_user` DISABLE KEYS */;
INSERT INTO `group_user` VALUES (5,4,2),(5,4,3);
/*!40000 ALTER TABLE `group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule`
--

DROP TABLE IF EXISTS `rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_definition` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `da4fe9e3-6da6-4472-bb58-6973d4b9e82a_idx` (`group_id`),
  KEY `6802d146-19fb-409c-85fb-5e4ef0b68c08_idx` (`id`,`group_id`),
  CONSTRAINT `da4fe9e3-6da6-4472-bb58-6973d4b9e82a` FOREIGN KEY (`group_id`) REFERENCES `group_` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule`
--

LOCK TABLES `rule` WRITE;
/*!40000 ALTER TABLE `rule` DISABLE KEYS */;
INSERT INTO `rule` VALUES (5,5,'Emergency on Bari Area',NULL,'{\"functions\":[{\"@class\":\"org.c2sense.mcp.rulengine.spi.functions.ContentFunction\",\"configuration\":{\"expression\":\"emergency\"}},{\"@class\":\"org.c2sense.mcp.rulengine.spi.functions.AreaFunction\",\"configuration\":{\"polygons\":[{\"points\":[{\"longitude\":16.815261840820312,\"latitude\":41.13936457644675},{\"longitude\":16.938858032226562,\"latitude\":41.10419094457646},{\"longitude\":16.882553100585938,\"latitude\":41.08142149109681},{\"longitude\":16.809768676757812,\"latitude\":41.102121320364915}]}]}}]}');
/*!40000 ALTER TABLE `rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule_topic`
--

DROP TABLE IF EXISTS `rule_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule_topic` (
  `rule_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  PRIMARY KEY (`rule_id`,`group_id`,`topic_id`),
  KEY `3fd2d98d-0101-49f0-abb3-266e50f8f2c5_idx` (`group_id`,`topic_id`),
  KEY `79e6dcdd-f29e-4a38-ba3f-a01d5ae7b033_idx` (`group_id`,`topic_id`),
  CONSTRAINT `3fd2d98d-0101-49f0-abb3-266e50f8f2c5` FOREIGN KEY (`group_id`, `topic_id`) REFERENCES `group_topic` (`group_id`, `topic_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `6802d146-19fb-409c-85fb-5e4ef0b68c08` FOREIGN KEY (`rule_id`, `group_id`) REFERENCES `rule` (`id`, `group_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule_topic`
--

LOCK TABLES `rule_topic` WRITE;
/*!40000 ALTER TABLE `rule_topic` DISABLE KEYS */;
INSERT INTO `rule_topic` VALUES (5,5,8);
/*!40000 ALTER TABLE `rule_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (4,'Civil Protection','Civil protection team of Regione Puglia');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_admin`
--

DROP TABLE IF EXISTS `team_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_admin` (
  `team_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_admin`
--

LOCK TABLES `team_admin` WRITE;
/*!40000 ALTER TABLE `team_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_topic`
--

DROP TABLE IF EXISTS `team_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_topic` (
  `team_id` bigint(20) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`topic_id`),
  KEY `9a674901-dd20-42d8-ad07-6dd12e2f0e32_idx` (`topic_id`),
  CONSTRAINT `9a674901-dd20-42d8-ad07-6dd12e2f0e32` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `bc23ea49-d46c-43f3-a31f-242360ccbffd` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_topic`
--

LOCK TABLES `team_topic` WRITE;
/*!40000 ALTER TABLE `team_topic` DISABLE KEYS */;
INSERT INTO `team_topic` VALUES (4,8);
/*!40000 ALTER TABLE `team_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_user`
--

DROP TABLE IF EXISTS `team_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_user` (
  `team_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`user_id`),
  KEY `6499b5a0-573c-4121-bdb5-72ab46db30d6_idx` (`user_id`),
  CONSTRAINT `0ed16a65-1f6a-48d0-9d1f-b7b310b41797` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `6499b5a0-573c-4121-bdb5-72ab46db30d6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_user`
--

LOCK TABLES `team_user` WRITE;
/*!40000 ALTER TABLE `team_user` DISABLE KEYS */;
INSERT INTO `team_user` VALUES (4,1),(4,2),(4,3),(4,4);
/*!40000 ALTER TABLE `team_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` VALUES (8,'CAP','Cap topic','CAP_XML');
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `spt_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'spt_id','user 1','+39123','email_1@email.com'),(2,'spt_id','user 2','+39123','email_2@email.com'),(3,'spt_id','user 3','+39123','email_3@email.com'),(4,'spt_id','user 4','+39123','email_4@email.com'),(5,'spt_id','user 5','+39123','email_5@email.com'),(6,'spt_id','user 6','+39123','email_6@email.com'),(7,'spt_id','user 7','+39123','email_7@email.com'),(8,'spt_id','user 8','+39123','email_8@email.com'),(9,'spt_id','user 9','+39123','email_9@email.com'),(10,'spt_id','user 10','+39123','email1_0@email.com'),(11,'spt_id','user 11','+39123','email1_1@email.com'),(12,'spt_id','user 12','+39123','email1_2@email.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-16 17:37:07
