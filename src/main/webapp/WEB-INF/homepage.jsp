<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <c:set var="contextPath" value="${pageContext.request.contextPath}" />
        <!DOCTYPE html>
        <html class="no-js" lang="" ng-app="mcpClient">

        <head>
            <meta charset="utf-8">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <title></title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="_csrf" content="${_csrf.token}" />
            <meta name="_csrf_header" content="${_csrf.headerName}" />
            <link rel="apple-touch-icon" href="apple-touch-icon.png">
            <!-- Place favicon.ico in the root directory -->
            <link rel="stylesheet" href="${contextPath}/static/css/normalize.css">
            <link rel="stylesheet" href="${contextPath}/static/css/main.css">
            <link rel="stylesheet" href="${contextPath}/static/css/mcp.css">
            <link rel="stylesheet" href="${contextPath}/static/css/ionicons.min.css">
            <link rel="stylesheet" href="${contextPath}/static/bower_components/bootstrap/dist/css/bootstrap.css">
            <script src="${contextPath}/static/js/vendor/modernizr-2.8.3.min.js"></script>
        </head>

        <body>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="#!/">MCP</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  ng-controller="NavController">
                        <ul class="nav navbar-nav">
                            <li ng-class="{ active: isActive('/home')}"><a href="#!/home"><i class="glyphicon glyphicon-home"></i></a></li>
                            <li ng-class="{ active: isActive('/teams')}"><a href="#!/teams">Teams</a></li>
                            <li ng-class="{ active: isActive('/users')}"><a href="#!/users">Users</a></li>
                            <!--<li ng-class="{ active: isActive('/roles')}"><a href="#!/roles">Roles</a></li>-->
                            <li ng-class="{ active: isActive('/topics')}"><a href="#!/topics">Topics</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="" class="dropdown-toggle" data-toggle="dropdown">Signed in as <strong>{{loggedUser}}</strong><b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="" ng-click="logout()"><span class="fa fa-sign-out"></span><i class="glyphicon glyphicon-off"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
                        <p class="navbar-text navbar-right"></p>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div id="generalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="generalModalTitle"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p id="generalModalText"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-{{alert.type}} alert-dismissible ng-cloack" role="alert" ng-repeat="alert in alerts" ng-controller="RootCtrl">
                    <button type="button" class="close" data-dismiss="alert" ng-click="closeAlert($index)"><span>&times;</span></button> {{ alert.msg }}
                </div>
            </div>
            <div class="container" style="min-height: 600px;">
                <div ng-view></div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="background: #e8e8e8; margin-top: 60px; min-height: 100px; padding-top:20px;">
                        <img src="${contextPath}/static/img/logo_c2sense.png" />
                        <span class="pull-right" style="margin-top:20px;color:#777">&copy; Lutech s.p.a.</span>
                    </div>
                </div>
            </div>

            <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
            <script>
                window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
            </script>
            <script src="${contextPath}/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="${contextPath}/static/js/plugins.js"></script>
            <script src="${contextPath}/static/js/main.js"></script>
            <script src="${contextPath}/static/bower_components/angular/angular.js"></script>
            <script src="${contextPath}/static/bower_components/angular-route/angular-route.js"></script>
            <script src="${contextPath}/static/bower_components/angular-resource/angular-resource.js"></script>
            <script src="${contextPath}/static/js/angular-ui-bootstrap-2.1.4.js"></script>
            <script src="${contextPath}/static/js/ui-bootstrap-tpls-2.1.4.min.js"></script>
            <script src="${contextPath}/static/js/angular-animate.js"></script>
            <script src="${contextPath}/static/js/angular-touch.js"></script>
            <script src="${contextPath}/static/js/app.module.js"></script>
            <script src="${contextPath}/static/js/app.config.js"></script>
            <script src="${contextPath}/static/service/core.module.js"></script>
            <script src="${contextPath}/static/service/group/group.module.js"></script>
            <script src="${contextPath}/static/service/group/group.service.js"></script>
            <script src="${contextPath}/static/service/team/team.module.js"></script>
            <script src="${contextPath}/static/service/team/team.service.js"></script>
            <script src="${contextPath}/static/service/role/role.module.js"></script>
            <script src="${contextPath}/static/service/role/role.service.js"></script>
            <script src="${contextPath}/static/service/rule/rule.module.js"></script>
            <script src="${contextPath}/static/service/rule/rule.service.js"></script>
            <script src="${contextPath}/static/service/user/user.module.js"></script>
            <script src="${contextPath}/static/service/user/user.service.js"></script>
            <script src="${contextPath}/static/service/topic/topic.module.js"></script>
            <script src="${contextPath}/static/service/topic/topic.service.js"></script>
            <script src="${contextPath}/static/service/alert/alert.module.js"></script>
            <script src="${contextPath}/static/service/alert/alert.service.js"></script>
            <script src="${contextPath}/static/mcp-home/mcp-home.module.js"></script>
            <script src="${contextPath}/static/mcp-home/mcp-home.component.js"></script>
            <script src="${contextPath}/static/mcp-group/mcp-group-detail.module.js"></script>
            <script src="${contextPath}/static/mcp-group/mcp-group-detail.component.js"></script>
            <script src="${contextPath}/static/mcp-group/mcp-group-create.module.js"></script>
            <script src="${contextPath}/static/mcp-group/mcp-group-create.component.js"></script>
            <script src="${contextPath}/static/mcp-role/mcp-role.module.js"></script>
            <script src="${contextPath}/static/mcp-role/mcp-role.component.js"></script>
            <script src="${contextPath}/static/mcp-role/mcp-role-detail.module.js"></script>
            <script src="${contextPath}/static/mcp-role/mcp-role-detail.component.js"></script>
            <script src="${contextPath}/static/mcp-team/mcp-team.module.js"></script>
            <script src="${contextPath}/static/mcp-team/mcp-team.component.js"></script>
            <script src="${contextPath}/static/mcp-team/mcp-team-detail.module.js"></script>
            <script src="${contextPath}/static/mcp-team/mcp-team-detail.component.js"></script>
            <script src="${contextPath}/static/mcp-team/mcp-team-create.module.js"></script>
            <script src="${contextPath}/static/mcp-team/mcp-team-create.component.js"></script>
            <script src="${contextPath}/static/mcp-rule/mcp-rule.module.js"></script>
            <script src="${contextPath}/static/mcp-rule/mcp-rule.component.js"></script>
            <script src="${contextPath}/static/mcp-rule/mcp-rule-detail.module.js"></script>
            <script src="${contextPath}/static/mcp-rule/mcp-rule-detail.component.js"></script>
            <script src="${contextPath}/static/mcp-rule/mcp-rule-create.module.js"></script>
            <script src="${contextPath}/static/mcp-rule/mcp-rule-create.component.js"></script>
            <script src="${contextPath}/static/mcp-user/mcp-user.module.js"></script>
            <script src="${contextPath}/static/mcp-user/mcp-user.component.js"></script>
            <script src="${contextPath}/static/mcp-topic/mcp-topic.module.js"></script>
            <script src="${contextPath}/static/mcp-topic/mcp-topic.component.js"></script>
            <script src="${contextPath}/static/mcp-topic/mcp-topic-detail.module.js"></script>
            <script src="${contextPath}/static/mcp-topic/mcp-topic-detail.component.js"></script>
            <script src="${contextPath}/static/js/main.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApOUws_3Qec_M6p3baXXkVG85lsuBXmAQ&libraries=drawing"></script>
        </body>

        </html>