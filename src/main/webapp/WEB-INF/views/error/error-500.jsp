<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en" xml:lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="robots" content="noindex, nofollow">

    <title>404 Not Found</title>

    <link rel="icon" type="image/png" href="favicon.png">

    <style type="text/css" media="screen">
        html, body{ margin: 0; padding: 0; border: 0; font-size: 100%; vertical-align: baseline; }
        body{ font: 100%/160% Georgia, Constantia, "Lucida Bright", LucidaBright, "DejaVu Serif", "Bitstream Vera Serif", "Liberation Serif", Times, Serif; text-align: center; color: #2A2925; background: #E1E0DC; padding-top: 10%; }
        img{ float: right; width: 25%; min-width: 150px; max-width: 300px; height: auto; margin-top: -5%; }
        p{ margin: 0; padding: 0 2% 2% 2%; line-height: 1em; font-size: 1.8em; }
        .error{ font-size: 4em; }
        .funny{ font-style: italic; }
        a, a:link, a:visited{ color: #fff; background: #2A2925; text-decoration: none; padding: .2em .4em; line-height: 2em; -webkit-border-radius: .5em; -moz-border-radius: .5em; border-radius: .5em; }
        a:hover{ background: #281d30; }
    </style>
</head>
<body>

<p class="error">Page error</p><p class="funny">Sorry for the incovenience!</p>
<c:if test="${not empty errCode}">
    <h1>${errCode} : System Errors</h1>
</c:if>

<c:if test="${empty errCode}">
    <h1>System Errors</h1>
</c:if>

<c:if test="${not empty errMsg}">
    <h2>${errMsg}</h2>
</c:if>

<p><a href="/mcp/home.html" title="Back">&lt;======mcp/</a></p>

</body></html>