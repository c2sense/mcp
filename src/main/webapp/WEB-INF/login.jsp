<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html class="no-js" lang="">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->
<link rel="stylesheet" href="${contextPath}/static/css/normalize.css">
<link rel="stylesheet" href="${contextPath}/static/css/main.css">
<link rel="stylesheet" href="${contextPath}/static/css/mcp.css">
<link rel="stylesheet" href="${contextPath}/static/css/ionicons.min.css">
<link rel="stylesheet" href="${contextPath}/static/bower_components/bootstrap/dist/css/bootstrap.css">
</head>

<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#!/">MCP</a>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container" style="min-height: 600px;">
		<h1>Login</h1>

		<form action="/login" method="post">
			<fieldset>
				<legend>Please Login</legend>
				<!-- use param.error assuming FormLoginConfigurer#failureUrl contains the query parameter error -->
				<c:if test="${param.error != null}">
					<div style="color: red;">
						Failed to login.
						<c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
	                  Reason: <c:out
								value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
						</c:if>
					</div>
				</c:if>
				<!-- the configured LogoutConfigurer#logoutSuccessUrl is /login?logout and contains the query param logout -->
				<c:if test="${param.logout != null}">
					<div>You have been logged out.</div>
				</c:if>
				<p>
					<label for="username">Username</label> <input type="text"
						id="username" name="username" />
				</p>
				<p>
					<label for="password">Password</label> <input type="password"
						id="password" name="password" />
				</p>

				<!-- if using RememberMeConfigurer make sure remember-me matches RememberMeConfigurer#rememberMeParameter -->
				<!--
			<p>
				<label for="remember-me">Remember Me?</label>
				<input type="checkbox" id="remember-me" name="remember-me" />
			</p>
			-->
				<div>
					<button type="submit" class="btn">Log in</button>
				</div>
			</fieldset>
		</form>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12"
				style="background: #e8e8e8; margin-top: 60px; min-height: 100px; padding-top: 20px;">
				<img src="${contextPath}/static/img/logo_c2sense.png" /> <span
					class="pull-right" style="margin-top: 20px; color: #777">&copy;
					Lutech s.p.a.</span>
			</div>
		</div>
	</div>
</body>
</html>



