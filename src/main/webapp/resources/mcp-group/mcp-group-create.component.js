'use strict';

angular.
module('mcpGroupCreate').
component('mcpGroupCreate', {
    templateUrl: 'resources/mcp-group/mcp-group-create.template.html',
    controller: ['$http', '$routeParams', 'Team', 'Group', 'alertService', function McpGroupCreateController($http, $routeParams, Team, Group, alertService) {
        var self = this;

        self.submitted = false;
        self.teamId = $routeParams.teamId;
        self.groupId = $routeParams.groupId;
        self.team = {};
        self.group = {
            name: "",
            description: "",
            isSmsNotificationActive: false,
            isUserEmailNotificationActive: false,
            isMailingListRequired: false,
            isFacebookEmailRequired: false,
            isTwitterEmailRequired: false,
            mailingList: "",
            facebookEmail: "",
            twitterEmail: ""
        };
        self.errors = [];

        self.manageMailField = function(checkThis, mailField) {
            if (!checkThis) {
                self.group[mailField] = '';
            }
        }

        Team.get(self.teamId).then(function(data) {
            self.team = data;
        }, function() {
            alertService.add("warning", "Some error occurred");
        });

        self.save = function() {
            self.errors = [];
            Group.create(self.teamId, self.group).then(function(response) {
                alertService.add("success", "Group successfully created.");
            }, function(response) {
                self.errors = response.errors;
                alertService.add("warning", "Some error occurred ");
            });
        };

        self.emptyMailingList = function() {
            if (!self.group.isMailingListRequired) {
                self.group.mailingList = '';
            }
        }
    }]
});
