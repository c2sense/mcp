'use strict';

angular.
module('mcpGroupDetail').
component('mcpGroupDetail', {
    templateUrl: 'resources/mcp-group/mcp-group-detail.template.html',
    controller: ['$http', '$routeParams', 'Team', 'Group', 'Rule', 'alertService', function McpGroupDetailController($http, $routeParams, Team, Group, Rule, alertService) {
        var self = this;
        self.submitted = false;
        self.teamId = $routeParams.teamId || null;
        self.groupId = $routeParams.groupId || null;
        self.team = {};
        self.group = {
            name: "",
            description: "",
            isSmsNotificationActive: false,
            isUserEmailNotificationActive: false,
            isMailingListRequired: false,
            isFacebookEmailRequired: false,
            isTwitterEmailRequired: false,
            mailingList: "",
            facebookEmail: "",
            twitterEmail: ""
        };
        self.rules = [];
        self.errors = [];

        self.findRules = function() {
            Rule.query(self.teamId, self.groupId).then(function(data) {
                self.rules = data;
            }, function() {
                alertService.add("warning", "Some error occurred");
            });
        }

        self.isTeamIdDefined = function() {
            return !!self.teamId && self.teamId !== 'create';
        }

        self.isGroupIdDefined = function() {
            return !!self.groupId && self.groupId !== 'create';
        }

        self.manageMailField = function(checkThis, mailField) {
            if (!checkThis) {
                self.group[mailField] = '';
            }
        }

        Team.get(self.teamId).then(function(data) {
            self.team = data;
        }, function() {
            alertService.add("warning", "Some error occurred");
        });

        Group.get(self.teamId, self.groupId).then(function(data) {
            self.group = data;
            self.group.isSmsNotificationActive = self.group.smsNotification;
            self.group.isUserEmailNotificationActive = self.group.userEmailNotification;
            self.group.isMailingListRequired = !!self.group.mailingList;
            self.group.isFacebookEmailRequired = !!self.group.facebookEmail;
            self.group.isTwitterEmailRequired = !!self.group.twitterEmail;
        }, function() {
            alertService.add("warning", "Some error occurred");
        });

        self.save = function() {
            self.errors = [];
            Group.update(self.teamId, self.groupId, self.group).then(function(response) {
                alertService.add("success", "Groupd successfully updated.");
            }, function(response) {
                self.errors = response.errors;
                alertService.add("warning", "Some error occurred");
            });
        };

        self.configureModal = function(ruleId, action) {
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedRuleId = ruleId;
        };

        self.activateCommand = function() {
            if (self.actionToDo === 'delete') {
                $(".mcp-modal-sm").modal("hide");
                Rule.delete(self.teamId, self.groupId, self.savedRuleId).then(function(data) {
                    self.rules = self.rules.filter(function(obj) {
                        return obj.id !== self.savedRuleId;
                    });
                    alertService.add("success", "Rule successfully deleted.");
                }, function(error) {
                    alertService.add("warning", "Some error occurred");
                });
            }
        };
    }]
});