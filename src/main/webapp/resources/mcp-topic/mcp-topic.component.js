'use strict';

var mcpTopicModule = angular.
module('mcpTopic').
component('mcpTopic', {
    templateUrl: 'resources/mcp-topic/mcp-topic.template.html',
    controller: ['$http', '$routeParams', '$uibModal', 'Topic', 'alertService', function McpTopicController($http, $routeParams, $uibModal, Topic, alertService) {
        var self = this;

        self.teamId = $routeParams.teamId || null;
        self.groupId = $routeParams.groupId || null;
        self.teams = [];
        self.groups = [];
        self.selectedTopics = [];
        self.selectedGroups = [];
        self.filteredTopics = [];

        self.isTeamIdDefined = function() {
            return !!self.teamId && self.teamId !== 'create';
        }

        self.isGroupIdDefined = function() {
            return !!self.groupId && self.groupId !== 'create';
        }

        self.printTitle = function() {
            if (self.isTeamIdDefined()) {
                if (self.isGroupIdDefined()) {
                    return 'Rule';
                }
                return 'Group';
            }
            return 'Team';
        }

        self.retrieveTopics = function() {

            // Se il teamId non c'è o c'è e non è create.
            if (!self.teamId || (!!self.teamId && self.teamId !== 'create')) {
                if(!!self.groupId)
                {
                    Topic.query(self.teamId, self.groupId).then(function(data) {
                        self.topics = data.topics;
                        self.teams = data.teams;
                        self.groups = data.groups;
                        self.rules = data.rules;
                        self.topics.map(function(topic) {
                            self.rulesTemp = self.rules.filter(function(rule) {
                                return topic.rules.indexOf(rule.id) > -1
                            });
                            topic.rules = self.rulesTemp;
                        });
                        console.log(self.topics);
                    }, function() {
                        
                    });
                    
                }else
                {
                    Topic.query(self.teamId).then(function(data) {
                        self.topics = data.topics;
                        self.teams = data.teams;
                        self.groups = data.groups;
                        if (self.isTeamIdDefined()) {
                            self.topics.map(function(topic) {
                                self.groupsTemp = self.groups.filter(function(group) {
                                    return topic.groups.indexOf(group.id) > -1
                                });
                                topic.groups = self.groupsTemp;
                            });
                        } else {
                            self.topics.map(function(topic) {
                                self.teamsTemp = self.teams.filter(function(team) {
                                    return topic.teams.indexOf(team.id) > -1
                                });
                                topic.teams = self.teamsTemp;
                            });
                        }
                    }, function() {
                        
                    });
                }
                
            }
        };

        self.retrieveTopics();

        self.selectTopics = function(list) {
            self.filteredTopics.map(function(topic) {
                var idx = list.indexOf(topic);
                if (idx > -1 && !self.master) {
                    list.splice(idx, 1);
                }
                if (idx < 0 && self.master) {
                    list.push(topic);
                }
            });
        }

        self.toggle = function(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            } else {
                list.push(item);
            }
        };

        self.exists = function(item, list) {
            return list.indexOf(item) > -1;
        };

        self.configureModal = function(topicId, action) {
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedTopicId = topicId;
        };

        self.activateCommand = function() {
            console.log(self.savedTopicId + " " + self.actionToDo);
            if (self.actionToDo === 'delete') {
                $(".mcp-modal-sm").modal("hide");
                Topic.delete(self.savedTopicId).then(function(data) {
                    self.topics = self.topics.filter(function(obj) {
                        return obj.id !== self.savedTopicId; });
                    alertService.add("success", "Topic was successfully deleted.");
                }, function(error) {
                    
                });
            }
        };


        self.prepareTeamAdd = function() {
            self.configureModal = 'team';
            self.action = 'saveTopicTeam';

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'resources/mcp-topic/topicOwnershipModal.html',
                controller: 'TopicOwnershipModalCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    args: function() {
                        return {
                            action: self.action,
                            configureModal: self.configureModal,
                            selectedTopics: self.selectedTopics,
                            teams: self.teams
                        };
                    }
                }
            });
        }

        self.prepareTeamRemove = function() {
            self.configureModal = 'team';
            self.action = 'removeTopicTeam';

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'resources/mcp-topic/topicOwnershipModal.html',
                controller: 'TopicOwnershipModalCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    args: function() {
                        return {
                            action: self.action,
                            configureModal: self.configureModal,
                            selectedTopics: self.selectedTopics,
                            teams: self.teams
                        };
                    }
                }
            });
        }

        self.prepareGroupAdd = function() {
            self.selectedGroup = [];
            self.configureModal = 'group';
            self.action = 'saveTopicGroup';

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'resources/mcp-topic/topicOwnershipModal.html',
                controller: 'TopicOwnershipModalCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    args: function() {
                        return {
                            action: self.action,
                            configureModal: self.configureModal,
                            selectedTopics: self.selectedTopics,
                            groups: self.groups,
                            teamId: self.teamId
                        };
                    }
                }
            });
        }

        self.prepareGroupRemove = function() {
            self.selectedGroup = [];
            self.configureModal = 'group';
            self.action = 'removeTopicGroup';

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'resources/mcp-topic/topicOwnershipModal.html',
                controller: 'TopicOwnershipModalCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    args: function() {
                        return {
                            action: self.action,
                            configureModal: self.configureModal,
                            selectedTopics: self.selectedTopics,
                            groups: self.groups
                        };
                    }
                }
            });

        }
    }]
});


mcpTopicModule.controller('TopicOwnershipModalCtrl', ['$uibModalInstance', 'args', 'Topic', 'alertService', function($uibModalInstance, args, Topic, alertService) {
    var self = this;

    self.action = args.action;
    self.configureModal = args.configureModal;
    self.selectedTopics = args.selectedTopics;
    self.teams = args.teams;
    self.groups = args.groups;
    self.teamId = args.teamId;

    self.saveTopicTeam = function() {
        if (self.selectedTeam && self.selectedTopics.length > 0) {
            var teamTopicForm = {
                teamId: self.selectedTeam.id,
                topicIds: self.selectedTopics.map(function(topic) {
                    return topic.id
                })
            };
            Topic.saveTopicForm(teamTopicForm)
                .then(function(data) {
                    self.selectedTopics.map(function(topic) {
                        topic.teams.push(self.selectedTeam);
                        var teamsTemp = topic.teams.filter(function(item, pos, removeDuplicate) {
                            return removeDuplicate.indexOf(item) == pos;
                        });
                        topic.teams = teamsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Topics were succesfully added to team");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a topic and a team");
        }
    }

    self.removeTopicTeam = function() {
        if (self.selectedTeam && self.selectedTopics.length > 0) {
            var teamTopicForm = {
                teamId: self.selectedTeam.id,
                topicIds: self.selectedTopics.map(function(topic) {
                    return topic.id
                })
            };
            Topic.removeTopicForm(teamTopicForm)
                .then(function(data) {
                    self.selectedTopics.map(function(topic) {

                        var teamsTemp = topic.teams.filter(function(item, pos) {
                            return item.id !== self.selectedTeam.id;
                        });
                        topic.teams = teamsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Topics were succesfully removed from team");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a topic and a team");
        }
    }

    self.saveTopicGroup = function() {
        if (self.selectedGroup && self.selectedTopics.length > 0) {
            var groupTopicForm = {
                groupId: self.selectedGroup.id,
                teamId: self.teamId,
                topicIds: self.selectedTopics.map(function(topic) {
                    return topic.id
                })
            };
            Topic.saveTopicForm(groupTopicForm)
                .then(function(data) {
                    self.selectedTopics.map(function(topic) {
                        topic.groups.push(self.selectedGroup);
                        var groupsTemp = topic.groups.filter(function(item, pos, removeDuplicate) {
                            return removeDuplicate.indexOf(item) == pos;
                        });
                        topic.groups = groupsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Topics were succesfully added to group");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a topic and a group");
        }
    }

    self.removeTopicGroup = function() {
        if (self.selectedGroup && self.selectedTopics.length > 0) {
            var groupTopicForm = {
                groupId: self.selectedGroup.id,
                topicIds: self.selectedTopics.map(function(topic) {
                    return topic.id
                })
            };
            Topic.removeTopicForm(groupTopicForm)
                .then(function(data) {
                    self.selectedTopics.map(function(topic) {
                        var groupsTemp = topic.groups.filter(function(item, pos) {
                            return item.id !== self.selectedGroup.id;
                        });
                        topic.groups = groupsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Topics were succesfully removed from group");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a topic and a group");
        }
    }

    self.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    self.activateAction = function() {
        switch (args.action) {
            case 'saveTopicTeam':
                self.saveTopicTeam();
                break;
            case 'removeTopicTeam':
                self.removeTopicTeam();
                break;
            case 'saveTopicGroup':
                self.saveTopicGroup();
                break;
            case 'removeTopicGroup':
                self.removeTopicGroup();
                break;
            default:
                alert("[Action] - Some error occurred");
        }
    }
}]);
