'use strict';

angular.
module('mcpTeamDetail').
component('mcpTeamDetail', {
    controller: ['$http', '$routeParams', 'Team', 'Group', 'alertService', function McpTeamDetailController($http, $routeParams, Team, Group, alertService) {
        var self = this;
        self.submitted = false;
        self.team = {};
        self.teamId = $routeParams.teamId || null;
        self.errors = [];

        self.isTeamIdDefined = function() {
            return !!self.teamId && self.teamId !== 'create';
        }

        self.activeTab = !self.isTeamIdDefined() ? 3 : 0;

        if (self.teamId !== 'create') {
            Team.get(self.teamId).then(function(data) {
                self.team = data;
            }, function() {
                Team.showError("Some error occurred");
            });
        }

        self.save = function() {
            self.errors = [];
            Team.save(self.team).then(function(response) {
                self.team.id = response.id;
                alertService.add("success", "Team successfully saved.");
            }, function(response) {
                self.errors = response.errors;
                alertService.add("warning", "Some error occurred.");
            });
        };

        self.configureModal = function(groupId, action) {
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedGroupId = groupId;
        };

        self.activateCommand = function() {
            if (self.actionToDo === 'delete') {
                $(".mcp-modal-sm").modal("hide");
                Group.delete(self.team.id, self.savedGroupId).then(function(data) {
                    self.groups = self.groups.filter(function(obj) {
                        return obj.id !== self.savedGroupId;
                    });
                    alertService.add("success", "Group successfully deleted.");
                }, function(error) {
                    alertService.add("warning", "Some error occurred.");
                });
            }
        };

        //Spostare questo nel Group service.
        self.findGroups = function() {
            if (self.teamId !== 'create') {
                Team.findGroups(self.teamId).then(function(data) {
                    self.groups = data;
                }, function() {
                    Team.showError("Some error occurred.");
                });
            }
        };
    }],
    templateUrl: 'resources/mcp-team/mcp-team-detail.template.html'
});
