'use strict';

angular.
module('mcpTeamCreate').
component('mcpTeamCreate', {
    controller: ['$http', '$routeParams', 'Team', 'Group', 'alertService', function McpTeamCreateController($http, $routeParams, Team, Group, alertService) {
        var self = this;
        self.team = {
            name: "",
            description: ""
        };
        self.errors = [];

        self.save = function() {
            self.errors = [];
            Team.save(self.team).then(function(response) {
                self.team.id = response.id;
                alertService.add("success", "Team successfully saved.");
            }, function(response) {
                self.errors = response.errors;
                alertService.add("warning", "Some error occurred.");
            });
        };
    }],
    templateUrl: 'resources/mcp-team/mcp-team-create.template.html'
});
