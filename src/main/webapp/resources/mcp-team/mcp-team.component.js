'use strict';

angular.
module('mcpTeam').
component('mcpTeam', {
    templateUrl: 'resources/mcp-team/mcp-team.template.html',
    controller: ['$http', '$routeParams', 'Team', 'alertService', function McpTeamController($http, $routeParams, Team, alertService) {
        var self = this;

        self.retrieveTeams = function(){
            Team.query().then(function(data){
                self.teams = data;
            }, function(){
                Team.showError("Some error occurred.");
            });
        };

        self.retrieveTeams();

        self.configureModal = function(teamId, action){
            self.modaltitle = 'Are you sure?';
            self.modalmessage = 'This action can not be rolled back.';
            self.modalbutton = 'Delete';
            self.actionToDo = action;
            self.savedTeamId = teamId;
        };

        self.activateCommand = function(){
            if(self.actionToDo === 'delete'){
                $(".mcp-modal-sm").modal("hide");
                Team.delete(self.savedTeamId).then(function(data){
                    self.teams = self.teams.filter(function(obj){ return obj.id !== self.savedTeamId; });
                    alertService.add("success", "Team successfully deleted.");
                },function(error){
                    alertService.add("warning", "Some error occurred.");
                });
            }
        };
    }]
});