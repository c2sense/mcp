'use strict';

angular.
module('core.topic').
factory('Topic', ['$http', '$q',
    function($http, $q) {
        var topic;
        var service = {};
        var basePath = 'rest/topics/';

        service.query = function(teamId, groupId) {
            if(!groupId)
            {
                var _url = basePath;
                if (!!teamId) {
                    _url = _url + 'teams/' + teamId;
                }
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: _url
                }).success(function(data) {
                    deferred.resolve(data);
                }).error(function(data) {
                    deferred.reject(data)
                })
                return deferred.promise;
            }else
            {
                var _url = basePath;
                var deferred = $q.defer();
                if (!!teamId && !!groupId) {
                    _url = _url + 'teams/' + teamId + '/groups/' + groupId;
                }else{
                    deferred.reject("Some id is missing");
                    return deferred;
                }
                $http({
                    method: 'GET',
                    url: _url
                }).success(function(data) {
                    deferred.resolve(data);
                }).error(function(data) {
                    deferred.reject(data)
                })
                return deferred.promise;
            }
        }
        
        service.findTopicsForRule = function(teamId, groupId, ruleId)
        {
        	var _url = basePath;
            var deferred = $q.defer();
            if (!!teamId && !!groupId && !!ruleId) {
                _url = _url + 'teams/' + teamId + '/groups/' + groupId + '/rules/' + ruleId;
            }else{
                deferred.reject("Some id is missing");
                return deferred;
            }
            $http({
                method: 'GET',
                url: _url
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data)
            })
            return deferred.promise;	
        }

        service.get = function(topicId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: basePath + topicId
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.save = function(topic) {
            var deferred = $q.defer();
            if (!!topic.id && topic.id !== 'create') {
                $http({
                    method: 'PUT',
                    url: basePath + topic.id,
                    data: topic
                }).success(function(data) {
                    deferred.resolve(data);
                }).error(function(data) {
                    deferred.reject(data)
                });
            } else {
                $http({
                    method: 'POST',
                    url: basePath,
                    data: topic
                }).success(function(data) {
                    deferred.resolve(data);
                }).error(function(data) {
                    deferred.reject(data)
                });
            }
            return deferred.promise;
        }

        service.delete = function(topicId) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: basePath + topicId
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.saveTopicForm = function(topicForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: basePath + 'add',
                data: topicForm
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.removeTopicForm = function(topicForm) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: basePath + 'remove',
                data: topicForm
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        return service;
    }
]);
