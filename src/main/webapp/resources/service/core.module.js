'use strict';

angular.module('core', ['core.team', 'core.role', 'core.group', 'core.rule', 'core.user', 'core.topic', 'core.alert']);