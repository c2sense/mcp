'use strict';

angular.
module('core.team').
factory('Team', ['$http', '$q',
    function ($http, $q) {
        var team;
        var service = {};
        var basePath = 'rest/teams/';

        service.query = function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: basePath
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.findGroups = function (teamId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: basePath + teamId + '/groups'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.get = function (teamId) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: basePath + teamId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.save = function (team) {
            var deferred = $q.defer();
            if(!team.id){
                $http({
                    method: 'POST',
                    url: basePath,
                    data: team
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data)
                })
                return deferred.promise;
            }else{
                $http({
                    method: 'PUT',
                    url: basePath + team.id,
                    data: team
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data)
                })
                return deferred.promise;
            }
        }

        service.delete = function (teamId) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: basePath + teamId
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject(data)
            })
            return deferred.promise;
        }

        service.showSuccess = function (message) {
            $("#generalModalTitle").text("Success");
            $("#generalModalText").text(message);
            $('#generalModal').modal('show');
        }

        service.showError = function (message) {
            $("#generalModalTitle").text("Error");
            $("#generalModalText").text(message);
            $('#generalModal').modal('show');
        }

        return service;
    }
]);
