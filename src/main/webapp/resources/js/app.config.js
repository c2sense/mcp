'use strict';

angular.
module('mcpClient').
config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.
        when('/', {
            template: '<mcp-home></mcp-home>'
        }).
        when('/home', {
            template: '<mcp-home></mcp-home>'
        }).
        when('/teams', {
            template: '<mcp-team></mcp-team>'
        }).
        when('/teams/create', {
            template: '<mcp-team-create></mcp-team-create>'
        }).
        when('/teams/:teamId', {
            template: '<mcp-team-detail></mcp-team-detail>'
        }).
        when('/teams/:teamId/groups/create', {
            template: '<mcp-group-create></mcp-group-create>'
        }).
        when('/teams/:teamId/groups/:groupId', {
            template: '<mcp-group-detail></mcp-group-detail>'
        }).
        when('/teams/:teamId/groups/:groupId/rules/create', {
            template: '<mcp-rule-create></mcp-rule-create>'
        }).
        when('/teams/:teamId/groups/:groupId/rules/:ruleId', {
            template: '<mcp-rule-detail></mcp-rule-detail>'
        }).
        when('/users', {
            template: '<mcp-user></mcp-user>'
        }).
        when('/users/:userId', {
            template: '<mcp-user-details></mcp-user-details>'
        }).
        when('/roles', {
            template: '<mcp-role></mcp-role>'
        }).
        when('/roles/:roleId', {
            template: '<mcp-role-detail></mcp-role-detail>'
        }).
        when('/topics', {
            template: '<mcp-topic></mcp-topic>'
        }).
        when('/topics/:topicId', {
            template: '<mcp-topic-detail></mcp-topic-detail>'
        }).
        otherwise('/');
    }
]);
