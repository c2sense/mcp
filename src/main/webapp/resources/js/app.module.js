'use strict';

var mcpClient = angular.module('mcpClient', [
    'ngRoute',
    'mcpHome',
    'mcpGroupDetail',
    'mcpGroupCreate',
    'mcpRole',
    'mcpRoleDetail',
    'mcpTeam',
    'mcpTeamDetail',
    'mcpTeamCreate',
    'mcpRule',
    'mcpRuleDetail',
    'mcpRuleCreate',
    'mcpTopic',
    'mcpTopicDetail',
    'mcpUser',
    'core'
]);

mcpClient.controller('NavController', function NavController($scope, $location, $http) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
    
    $scope.findLoggedUser = function(){
    	$http.get("/rest/loggeduser").then(function(response){
    		$scope.loggedUser = response.data;
    	}, function(){
    		return "Not logged";
    	});
    }
    
    $scope.findLoggedUser();
    
    $scope.logout = function () {
        $http.get("/logout")
        .then(function(response) {
			console.log("logout");
		}, function() {
			console.log("failed logout");
		});
    };
});

mcpClient.controller('RootCtrl', RootCtrl);

function RootCtrl($rootScope, $location, alertService) {
  $rootScope.changeView = function(view) {
    $location.path(view);
  }

  // root binding for alertService
  $rootScope.closeAlert = alertService.closeAlert; 
}

RootCtrl.$inject = ['$scope', '$location', 'alertService'];