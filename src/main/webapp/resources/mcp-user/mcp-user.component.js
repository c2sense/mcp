/**
 * Created by Andrea.Sbarra on 30/08/2016.
 */
'use strict';

var mcpUserModule = angular
    .module('mcpUser')
    .component('mcpUser', {
        templateUrl: 'resources/mcp-user/mcp-user.template.html',
        controller: ['$http', '$routeParams', '$uibModal', 'User', 'Team', 'alertService', function McpUserController($http, $routeParams, $uibModal, User, Team, alertService) {

            var self = this;

            self.teamId = $routeParams.teamId || null;
            self.groupId = $routeParams.groupId || null;
            self.users = [];
            self.teams = [];
            self.groups = [];
            self.teamTemp = [];
            self.teamUserForm = {};
            self.selectedUsers = [];
            self.selectedGroups = [];
            self.filteredUsers = [];
            self.master = false;
            self.action = '';
            self.configureModal = '';

            self.isTeamIdDefined = function(){
                return !!self.teamId && self.teamId !== 'create' ;
            }

            self.isGroupIdDefined = function(){
                return !!self.groupId && self.groupId !== 'create' ;
            }

            self.printTitle = function(){
                if(self.isTeamIdDefined()){
                    return 'Group';
                }
                return 'Team';
            }

            self.retrieveUsers = function() {
                //Se il teamId non c'è o c'è e non è create.
                if(!self.teamId || (!!self.teamId && self.teamId!=='create'))
                {
                    if(!!self.groupId)
                    {
                        User.queryGroupUsers(self.groupId).then(function(data) 
                        {
                            self.users = data;
                            console.log(self.isGroupIdDefined());
                        }, function() {
                            alertService.add("warning", "Some error occurred.");
                        });
                    }else
                    {
                        User.query(self.teamId).then(function(data) {
                            self.users = data.users;
                            self.teams = data.teams;
                            self.groups = data.groups;
                            if(self.isTeamIdDefined()){
                                self.users.map(function(user) {
                                    self.groupsTemp = self.groups.filter(function(group) {
                                        return user.groups.indexOf(group.id) > -1
                                    });
                                    user.groups = self.groupsTemp;
                                });
                            }else{
                                self.users.map(function(user) {
                                    self.teamsTemp = self.teams.filter(function(team) {
                                        return user.teams.indexOf(team.id) > -1
                                    });
                                    user.teams = self.teamsTemp;
                                    self.teamAdminsTemp = self.teams.filter(function(team) {
                                        return user.teamAdmins.indexOf(team.id) > -1
                                    });
                                    user.teamAdmins = self.teamAdminsTemp;
                                });
                            }
                        }, function() {
                            alertService.add("warning", "Some error occurred.");
                        });
                    }
                }
            };

            self.retrieveUsers();

            self.selectUsers = function(list) {
                self.filteredUsers.map(function(user) {
                    var idx = list.indexOf(user);
                    if (idx > -1 && !self.master) {
                        list.splice(idx, 1);
                    }
                    if (idx < 0 && self.master) {
                        list.push(user);
                    }
                });
            }

            self.toggle = function(item, list) {
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                } else {
                    list.push(item);
                }
            };

            self.exists = function(item, list) {
                return list.indexOf(item) > -1;
            };

            self.prepareTeamAdd = function() {
                self.configureModal = 'team';
                self.action = 'saveUserTeam';

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'resources/mcp-user/userOwnershipModal.html',
                    controller: 'UserOwnershipModalCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        args: function() {
                            return {
                                action: self.action,
                                configureModal: self.configureModal,
                                selectedUsers: self.selectedUsers,
                                teams: self.teams
                            };
                        }
                    }
                });
            }

            self.prepareTeamRemove = function() {
                self.configureModal = 'team';
                self.action = 'removeUserTeam';

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'resources/mcp-user/userOwnershipModal.html',
                    controller: 'UserOwnershipModalCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        args: function() {
                            return {
                                action: self.action,
                                configureModal: self.configureModal,
                                selectedUsers: self.selectedUsers,
                                teams: self.teams
                            };
                        }
                    }
                });
            }
            
            self.prepareTeamAdminAdd = function() {
                self.configureModal = 'team';
                self.action = 'saveTeamAdmin';

                if(self.selectedUsers.length != 1){
                	alertService.add("warning", "Please select one user.");
                	return;
                }
                
                if(self.selectedUsers[0].teams.length < 1){
                	alertService.add("warning", "Please add at least one team to the user.");
                	return;
                }
                
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'resources/mcp-user/userOwnershipModal.html',
                    controller: 'UserOwnershipModalCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        args: function() {
                            return {
                                action: self.action,
                                configureModal: self.configureModal,
                                selectedUsers: self.selectedUsers,
                                teams: self.selectedUsers[0].teams
                            };
                        }
                    }
                });
            }
            
            self.prepareTeamAdminRemove = function() {
                self.configureModal = 'team';
                self.action = 'removeTeamAdmin';
                
                if(self.selectedUsers.length != 1){
                	alertService.add("warning", "Please select one user.");
                	return;
                }
                
                if(self.selectedUsers[0].teamAdmins.length < 1){
                	alertService.add("warning", "No team to admin.");
                	return;
                }

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'resources/mcp-user/userOwnershipModal.html',
                    controller: 'UserOwnershipModalCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        args: function() {
                            return {
                                action: self.action,
                                configureModal: self.configureModal,
                                selectedUsers: self.selectedUsers,
                                teams: self.selectedUsers[0].teams
                            };
                        }
                    }
                });
            }
            

            self.prepareGroupAdd = function() {
                self.selectedGroup = [];
                self.configureModal = 'group';
                self.action = 'saveUserGroup';

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'resources/mcp-user/userOwnershipModal.html',
                    controller: 'UserOwnershipModalCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        args: function() {
                            return {
                                action: self.action,
                                configureModal: self.configureModal,
                                selectedUsers: self.selectedUsers,
                                groups: self.groups,
                                teamId: self.teamId
                            };
                        }
                    }
                });
            }

            self.prepareGroupRemove = function() {
                self.selectedGroup = [];
                self.configureModal = 'group';
                self.action = 'removeUserGroup';

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'resources/mcp-user/userOwnershipModal.html',
                    controller: 'UserOwnershipModalCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        args: function() {
                            return {
                                action: self.action,
                                configureModal: self.configureModal,
                                selectedUsers: self.selectedUsers,
                                groups: self.groups
                            };
                        }
                    }
                });

            }
        }]
    });


mcpUserModule.controller('UserOwnershipModalCtrl', ['$uibModalInstance', 'args', 'User', 'alertService', function($uibModalInstance, args, User, alertService) {
    var self = this;

    self.action = args.action;
    self.configureModal = args.configureModal;
    self.selectedUsers = args.selectedUsers;
    self.teams = args.teams;
    self.groups = args.groups;
    self.teamId = args.teamId;

    self.saveUserTeam = function() {
        if (self.selectedTeam && self.selectedUsers.length > 0) {
            var teamUserForm = {
                teamId: self.selectedTeam.id,
                userIds: self.selectedUsers.map(function(user) {
                    return user.id
                })
            };
            User.saveUserForm(teamUserForm)
                .then(function(data) {
                    self.selectedUsers.map(function(user) {
                        user.teams.push(self.selectedTeam);
                        var teamsTemp = user.teams.filter(function(item, pos, removeDuplicate) {
                            return removeDuplicate.indexOf(item) == pos;
                        });
                        user.teams = teamsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Users were succesfully added to team");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a user and a team");
        }
    }

    self.removeUserTeam = function() {
        if (self.selectedTeam && self.selectedUsers.length > 0) {
            var teamUserForm = {
                teamId: self.selectedTeam.id,
                userIds: self.selectedUsers.map(function(user) {
                    return user.id
                })
            };
            User.removeUserForm(teamUserForm)
                .then(function(data) {
                    self.selectedUsers.map(function(user) {

                        var teamsTemp = user.teams.filter(function(item, pos) {
                            return item.id !== self.selectedTeam.id;
                        });
                        user.teams = teamsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Users were succesfully removed from team");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a user and a team");
        }
    }
    
    self.saveTeamAdmin = function() {
        if (self.selectedTeam && self.selectedUsers.length > 0) {
            var teamAdminForm = {
                teamId: self.selectedTeam.id,
                userIds: self.selectedUsers.map(function(user) {
                    return user.id
                })
            };
            User.saveTeamAdminForm(teamAdminForm)
                .then(function(data) {
                    self.selectedUsers.map(function(user) {
                        user.teamAdmins.push(self.selectedTeam);
                        var teamsTemp = user.teamAdmins.filter(function(item, pos, removeDuplicate) {
                            return removeDuplicate.indexOf(item) == pos;
                        });
                        user.teamAdmins = teamsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Users were succesfully added to team admins");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a user and a team");
        }
    }
    
    self.removeTeamAdmin = function() {
        if (self.selectedTeam && self.selectedUsers.length > 0) {
            var teamUserForm = {
                teamId: self.selectedTeam.id,
                userIds: self.selectedUsers.map(function(user) {
                    return user.id
                })
            };
            User.removeTeamAdminForm(teamUserForm)
                .then(function(data) {
                    self.selectedUsers.map(function(user) {

                        var teamsTemp = user.teamAdmins.filter(function(item, pos) {
                            return item.id !== self.selectedTeam.id;
                        });
                        user.teamAdmins = teamsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Users were succesfully removed from team");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a user and a team");
        }
    }

    self.saveUserGroup = function() {
        if (self.selectedGroup && self.selectedUsers.length > 0) {
            var groupUserForm = {
                groupId: self.selectedGroup.id,
                teamId: self.teamId,
                userIds: self.selectedUsers.map(function(user) {
                    return user.id
                })
            };
            User.saveUserForm(groupUserForm)
                .then(function(data) {
                    self.selectedUsers.map(function(user) {
                        user.groups.push(self.selectedGroup);
                        var groupsTemp = user.groups.filter(function(item, pos, removeDuplicate) {
                            return removeDuplicate.indexOf(item) == pos;
                        });
                        user.groups = groupsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Users were succesfully added to group");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a user and a group");
        }
    }

    self.removeUserGroup = function() {
        if (self.selectedGroup && self.selectedUsers.length > 0) {
            var groupUserForm = {
                groupId: self.selectedGroup.id,
                userIds: self.selectedUsers.map(function(user) {
                    return user.id
                })
            };
            User.removeUserForm(groupUserForm)
                .then(function(data) {
                    self.selectedUsers.map(function(user) {
                        var groupsTemp = user.groups.filter(function(item, pos) {
                            return item.id !== self.selectedGroup.id;
                        });
                        user.groups = groupsTemp.sort();
                    });
                    self.cancel();
                    alertService.add("success", "Users were succesfully removed from group");
                }, function() {
                    self.cancel();
                    alertService.add("warning", "Something went wrong, please try again");
                });
        } else {
            alert("Select at least a user and a group");
        }
    }

    self.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    self.activateAction = function() {
        switch (args.action) {
            case 'saveUserTeam':
                self.saveUserTeam();
                break;
            case 'removeUserTeam':
                self.removeUserTeam();
                break;
            case 'saveTeamAdmin':
                self.saveTeamAdmin();
                break;
            case 'removeTeamAdmin':
                self.removeTeamAdmin();
                break;
            case 'saveUserGroup':
                self.saveUserGroup();
                break;
            case 'removeUserGroup':
                self.removeUserGroup();
                break;
            default:
                alert("[Action] - Some error occurred");
        }
    }
}]);
