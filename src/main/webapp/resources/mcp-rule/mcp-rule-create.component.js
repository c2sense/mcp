'use strict';

angular.module('mcpRuleCreate').component('mcpRuleCreate', {
    controller: ['$http', '$routeParams', 'Team', 'Group', 'Rule', 'Topic', 'alertService', function McpRuleCreateController($http, $routeParams, Team, Group, Rule, Topic, alertService) {
        var self = this;

        self.areaFunctionAdded = false;

        self.teamId = $routeParams.teamId;
        self.groupId = $routeParams.groupId;
        self.team = {};
        self.group = {};

        self.topics = [];
        self.polygons = [];

        self.rule = {};
        self.rule.topicIds = [];
        self.rule.ruleExec = {};
        self.rule.ruleExec.functions = [];

        self.errors = [];

        var Polygon = function() {
            this.points = [];
        }

        Polygon.prototype.buildPolygon = function(polygon) {
            var _points = [];
            polygon.getPath().forEach(function(_a, number) {
                var point = new Point(_a);
                _points.push(point);
            });
            this.points = _points;
        }

        Polygon.prototype.toString = function() {
            var toString = "";
            this.points.map(function(point) {
                toString = toString + point.toString();
            });
            return toString;
        }

        var Point = function(latLng) {
            this.longitude = latLng.lng();
            this.latitude = latLng.lat();
        }

        Point.prototype.toString = function() {
            return "" + this.latitude + " " + this.longitude;
        }

        self.buildAreaFunction = function() {
            if (!self.areaFunction) {
                self.areaFunction = {};
                self.areaFunction["@class"] = 'org.c2sense.mcp.rulengine.spi.functions.AreaFunction';
                self.areaFunction.configuration = {};
            }
            self.areaFunction.configuration.polygons = [];
            self.polygons.map(function(googlePolygon) {
                var mcpPolygon = new Polygon();
                mcpPolygon.buildPolygon(googlePolygon);
                self.areaFunction.configuration.polygons.push(mcpPolygon);
            });
            var index = self.rule.ruleExec.functions.indexOf(self.areaFunction);
            if (index > -1) {
                self.rule.ruleExec.functions.splice(index, 1);
            }
            self.rule.ruleExec.functions.push(self.areaFunction);
        };

        Team.get(self.teamId).then(function(data) {
            self.team = data;
        }, function() {
            alertService.add("warning", "Some error occurred");
        });

        Group.get(self.teamId, self.groupId).then(function(data) {
            self.group = data;
        }, function() {
            alertService.add("warning", "Some error occurred");
        });

        Topic.query(self.teamId, self.groupId).then(function(data) {
            self.topics = data.topics;
        }, function() {
            alertService.add("warning", "Some error occurred");
        });

        self.deleteFunction = function(index) {
            self.rule.ruleExec.functions.splice(index, 1);
        }

        self.addSeverityFunction = function() {
            var severityFunction = {};
            severityFunction["@class"] = 'org.c2sense.mcp.rulengine.spi.functions.SeverityFunction';
            severityFunction.configuration = {};
            self.rule.ruleExec.functions.push(severityFunction);
        }

        self.addContentFunction = function() {
            var contentFunction = {};
            contentFunction["@class"] = 'org.c2sense.mcp.rulengine.spi.functions.ContentFunction';
            contentFunction.configuration = {};
            contentFunction.configuration.expression = "";
            self.rule.ruleExec.functions.push(contentFunction);
        }

        self.addRegExFunction = function() {
            var regExFunction = {};
            regExFunction["@class"] = 'org.c2sense.mcp.rulengine.spi.functions.RegexFunction';
            regExFunction.configuration = {};
            regExFunction.configuration.expression = "";
            self.rule.ruleExec.functions.push(regExFunction);
        }

        self.addAreaFunction = function() {
            self.areaFunctionAdded = true;
            self.initMap();
        }

        self.deleteAreaFunction = function() {
            self.areaFunctionAdded = false;
            self.polygons.map(function(polygon) {
                polygon.setMap(null);
            });
            self.polygons = [];
        }

        self.create = function() {
            if (self.areaFunctionAdded) {
                self.buildAreaFunction();
            }
            self.errors = [];
            console.log(self.rule);
            Rule.create(self.teamId, self.groupId, self.rule).then(function(response) {
                alertService.add("success", "Rule successfully saved.");
            }, function(response) {
                self.errors = response.errors;
                alertService.add("warning", "Some errors occurred");
            });
        };

        self.initMap = function() {
            if (!self.map) {
                self.map = new google.maps.Map(document.getElementById('map'), {
                    center: {
                        lat: 41.25,
                        lng: 16.25
                    },
                    zoom: 8
                });

                self.drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.MARKER,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.TOP_CENTER,
                        drawingModes: ['polygon' /*, 'circle', 'rectangle' */ ]
                    },
                    markerOptions: {
                        icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
                    },
                    circleOptions: {
                        fillColor: '#ffff00',
                        fillOpacity: 1,
                        strokeWeight: 5,
                        clickable: false,
                        editable: true,
                        zIndex: 1
                    },
                    polygonOptions: {
                        strokeColor: '#ffd700',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#ffd700',
                        fillOpacity: 0.35,
                        clickable: true,
                        editable: true,
                        draggable: true,
                        zIndex: 1
                    }
                });
                self.drawingManager.setMap(self.map);
                google.maps.event.addListener(self.drawingManager, 'polygoncomplete', function(polygon) {
                    self.toggle(self.polygons, polygon);
                    google.maps.event.addListener(polygon, 'rightclick', function(event) {
                        polygon.setMap(null);
                        self.toggle(self.polygons, polygon);
                    });
                });

                google.maps.event.addListener(self.drawingManager, 'markercomplete', function(marker) {
                    google.maps.event.addListener(marker, 'rightclick', function(event) {
                        marker.setMap(null);
                    });
                });
            }
        };
        self.toggle = function(list, item) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            } else {
                list.push(item);
            }
        };
    }],
    templateUrl: 'resources/mcp-rule/mcp-rule-create.template.html'
});