# MCP

## Overall description

The Messaging Communication Platform MCP tool will undergo some major developments that will better accommodate what has been discussed during the meeting in Wien (May 2016). The starting point are the Description of Work [1.2.6, Research challenges and progress beyond the state-of-the-art in knowledge level interoperability] statements:
Messaging and Communication Tools: C2-SENSE will support the individuals that make up the emergency team and the interactions between them during the group decision making process. The tool will also support team membership, roles and responsibilities. Publish-subscribe interfaces will be supported so that each user can register his interest in a particular type of message by specifying subscriptions containing rules such as type of incident, geospatial area affected, sender role or severity. Whenever a new message matching the subscription arrives, the user will get a notification by appropriate gateways to delivery mechanisms (e.g. SMS, email, SOAP) which can be selected according to roles. By specifying roles instead of specific email addresses, the messages will reach the appropriate persons regardless of their actual identities.

The changes will be mainly focused on extending the filtering capabilities of the tool – currently relying on simple topic subscriptions – introducing the possibility to take filtering decisions on messages through the inspection of their actual payload. Only specific message types will be made available for content filtering, and the set of applicable filtering functions will cover the most relevant message fields, but not all of them (nor it will be supported a generic query language over them).
This application connect Spring Security with Google OpenIdConnect.
Application is build using Spring Boot and Java 8.

Configure file application.yml with client id and secret obtained with [Google Developers Console](https://console.developers.google.com/).

##License##

Licensed under the [Apache License, Version 2.0][1]

[1]: http://www.apache.org/licenses/LICENSE-2.0